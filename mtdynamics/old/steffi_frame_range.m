function range = steffi_frame_range( filename );

comma = strfind(filename, ',');

if ( isempty( comma ) )
    error('found no comma in the name %s', filename);
end

if ( length( comma ) > 1 )
    error('found two commas in the name %s', filename);
end



minus = strfind(filename, '-');

if ( isempty( minus ) )
    error('found no minus sign in the name %s', filename);
end

if ( length( minus ) > 1 )
    error('found two minus signs in the name %s', filename);
end


if ( comma >= minus )
    error('comma should be before the minus');
end

strlen = length( filename );
part1 = filename(comma+1:minus-1);
part2 = filename(minus+1:strlen);

first = sscanf( part1, '%i' );
last = sscanf( part2, '%i' );
range = [first, last];

