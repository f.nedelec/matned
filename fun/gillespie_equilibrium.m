function [t, n] = gillespie_equilibrium(par, tmax)

% A stochastic simulation of reaction kinetics.
%
% This implements an equilibrium of two reactions:
% A -> B, with rate par.AB
% B -> A, with rate par.BA
%
% The argument `par` should be a structure providing:
% - 2 reaction rates: par.AB, par.BA
% - 2 initial number of molecules: par.A, par.B
%
% The output is a vector of time points `t`, and a matrix `n` with two 
% columns containing the quantity of A and B, at every time point.
%
% Francois Nedelec, 2012--2015

%% offer default parameters

if isempty(par)
    par.A = 0;
    par.B = 100;
    par.AB = 2;
    par.BA = 1;
end

%% initialization

A = par.A;
B = par.B;
time = 0;

t(1) = 0;
n(1, 1:2) = [ A, B ];

%% simulation

while 1

    % select next reaction stochastically:
    [dt, ix] = gillespie_choice([A*par.AB, B*par.BA]);
    
    % update time
    time = time + dt;
    
    if ( time > tmax )
        break
    end
    
    % update population:
    switch(ix)
        case 1
            A = A - 1;
            B = B + 1;
        case 2
            A = A + 1;
            B = B - 1;
        otherwise
            error('unknown reaction')
    end
        
    % record time and current state
    t(end+1) = time;
    n(end+1, 1:2) = [ A, B ];

end


end