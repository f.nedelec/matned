function ellipse_fun(w, a)

m = 35;
h = -5:0.001:m;
p=zeros(length(h), 3);

p(:,1) = w(1) ./ ( 1 + h/a(1) );
p(:,2) = w(2) ./ ( 1 + h/a(2) );
p(:,3) = w(3) ./ ( 1 + h/a(3) );

pa=zeros(size(p));

pa(:,1) = p(:,1)/a(1);
pa(:,2) = p(:,2)/a(2);
pa(:,3) = p(:,3)/a(3);

d = sum(pa.^2,2) - 1.0;

figure(1);
clf;
plot(h, d);
hold on;
plot([-5 m], [0 0], 'k:');
ylim([-1 1000]);

figure(2);
clf;
plot3(p(:,1), p(:,2), p(:,3));

end
