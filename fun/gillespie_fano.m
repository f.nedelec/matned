function gillespie_fano

par.sM = 1;
par.sP = 10;
par.dM = 0.1;
par.dP = 1;
par.G = 1;
par.M = 0;
par.P = 0;

rates = 0.1:0.1:10;
fano = zeros(size(rates));

for i = 1:length(rates)
    par.sP = rates(i);
    par.M = par.G * par.sM / par.dM;
    par.P = par.M * par.sP / par.dP;
    [t, n] = central_dogma(par, 100);
    [m, v] = gillespie_mean(t,n);
    fano(i) = sqrt(v(3)) / m(3);
end

plot(rates, fano);
xlabel('rate');
ylabel('fano');
