function [t, n] = central_dogma(par, tmax)

% function [t, n] = central_dogma(par)
%
% A stochastic simulation of basic gene expression:
%
% G -> G + M, rate sM
% M -> M + P, rate sP
% M -> 0, rate dM
% P -> 0, rate dP
%
% The argument `par` should be a structure providing:
% - 4 reaction rates: par.sM, par.sP, par.dM, par.dP
% - 3 initial abundance: par.G, par.M, par.P
%
% The output is a vector of time points `t`, and a matrix `n` containing in
% each column the number of molecules of (G, M, P), at every time point.
%
% F. Nedelec, April 2015

%% default parameters

if isempty(par)
    par.sM = 1;
    par.sP = 10;
    par.dM = 0.1;
    par.dP = 1;
    par.G = 1;
    par.M = 0;
    par.P = 0;
end

%% initialize population

G = par.G;
M = par.M;
P = par.P;
time = 0;

t(1) = 0;
n(1, 1:3) = [ G, M, P ];

%% simulate

while 1

    % calculate rates for the 4 possible reactions:
    rates = [ par.sM * G, par.dM * M, par.sP * M, par.dP * P ];
    
    % select next reaction stochastically:
    [dt, ix] = gillespie_choice(rates);
        
    % update time
    time = time + dt;

    if ( time > tmax )
        break
    end
    
    % update population depending on reaction:
    switch(ix)
        case 1
            M = M + 1;
        case 2
            M = M - 1;
        case 3
            P = P + 1;
        case 4
            P = P - 1;
        otherwise
            error('unknown reaction')
    end
    
    % record time and current state
    t(end+1) = time;
    n(end+1, 1:3) = [ G, M, P ];
end


end