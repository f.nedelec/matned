function im2 = filterrank( im, n )
% dilate the picture
% i.e. take the maximum value of the neighbooring pixels closer than n

if ( isfield(im,'data') ) im = im.data; end
if ( nargin < 2 ) n = 1; end

mask = maskcircle1(2*n+1);
[ mdx, mdy ] = find( mask );
dd   = n+1;

im2 = im;

for indx = 1:length(mdx)
   
   dx = mdx(indx) - dd;
   dy = mdy(indx) - dd;
   
   lx = max(1, 1 - dx );
   ux = min(size(im,1), size(im,1) - dx);
   ly = max(1, 1 - dy);
   uy = min(size(im,2), size(im,2) - dy);
      
   im2(lx:ux, ly:uy) = max( im2(lx:ux, ly:uy),...
      im( lx+dx:ux+dx, ly+dy:uy+dy ) );
   
end


return;