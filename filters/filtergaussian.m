function imb = filtergaussian( im, v )

%  imb = filtergaussian( im, v )
%
% convolution with a guassian filter of variance v
% using padding of the image, to reduce border effects


if ( nargin < 1 ) v = 1; end

if ( isfield(im,'data') ) im = im.data; end


m  = floor( sqrt( 2.5*v ) ) + 1;
mm = maskgrad1( m );
mm = exp( -mm.^2 ./ v );
mm = mm ./ sum(sum( mm ));


pad  = sum( im(1,:) + im(size(im,1),:) ) + sum( im(:,1) + im(:,size(im,2)) );
pad  = pad / ( 2*( size(im,1) + size(im,2) ) );

%add a border:
imb = image_crop( im, [ 1-m, 1-m, size(im,1)+m, size(im,2)+m ], pad );

imb = filter2( mm, imb );
%im2 = convs2( im, mm, 1 );

imb = image_crop( imb, [ 1+m, 1+m, size(im,1)+m, size(im,2)+m ]);

return;
