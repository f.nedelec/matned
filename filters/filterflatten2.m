function flat = filterflatten2( im )


h = imhist( im );
b = histmin( h )
%b = 0.8 * b
%im = ( im - b );
c = fitellipse( im );
y = imellipse([1 1 size(im)], c , 1);
flat = im ./ y;

