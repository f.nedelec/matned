function im2 = filteisolated( im, n )

% ismax = filtereraseisolated( im, n )
%
% erase local maximums of the picture

if ( isfield(im,'data') ) im = im.data; end
if ( nargin < 2 ) n = 1; end

mask = filterlocmax( im ) >= 7;
im2 = im .* ( 1-mask );

return;