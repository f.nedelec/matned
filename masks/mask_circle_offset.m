function mask=mask_circle_offset(c, r, s)
% function mask = mask_circle_offset(c, r, s)
%
% make a circle of radius r, centered around c=[cx,cy] in a
% rectangle of size s=[sx,sy]


cmask = mask_circle(2*r);
mask  = mask_translate(cmask, c-r-1, s);

return;
