function mask = mask_circle(n, p)

% mask = mask_circle(n)
% mask = mask_circle(n, p)
%
% Return a n*n matrix with ones inside a circle, and zero outside
% the circle of radius n/2 is centered in the matrix.
% In the second form (with argument p) the circular mask of size n,
% has a central disc of diameter p removed
%
% F. Nedelec, Feb. 2008

if nargin < 1
    error('the radius of the mask must be specified as first argument');
end

if rem(n,1) > 0, 
   n = round(n);
   fprintf('n is not an integer and has been rounded to %i',n)
end

if rem(n,2) == 0
    m = n / 2;
    xx  = ( 0.5 + (0:m-1) ) .^2;
else
    m = (n+1) / 2;
    xx  = (0:m-1) .^2;
end

%create a quarter of the mask
dist = ones(m,1) * xx + xx' * ones(1,m);

if nargin == 1
    quart = ( dist <= 0.25*n*n );
else
    quart = ( dist <= 0.25*n*n ) & ( dist > 0.25*p*p );
end

mask = zeros(n,n);

if rem(n,2) == 0
    % n = 2*m
    mask( m+1:n,  m+1:n ) = quart;
    mask( m:-1:1, m+1:n ) = quart;
    mask( m:-1:1, m:-1:1) = quart;
    mask( m+1:n,  m:-1:1) = quart;
else
    % n = 2*m-1
    mask( m:n,    m:n )   = quart;
    mask( m:-1:1, m:n )   = quart;
    mask( m:-1:1, m:-1:1) = quart;
    mask( m:n,    m:-1:1) = quart;
end

return