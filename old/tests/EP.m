function avg = EP

L = 50;
dt = 0.1;
a = dt * 0.05;
d = dt * 0.1;
h = dt * 1;
r  = a / ( a + d );

figure;
plot( [ 1 L ], [ r r ], 'k-' );
hold on;
axis( [1 L 0 1 ] );

for sigma = 0:1
    
de = d + h * ( 1 - sigma );

s = zeros(L, 1);
avg = zeros(L, 1);

steps = 2000000;

for i = 1 : steps 

    attach  = ( rand(L,1) < a ) .* ( 1 - s );
    detach  = ( rand(L,1) < d ) .*  s;
    detach( L ) = ( rand < de ) * s( L );
    s = s + attach - detach;
    move    = ( rand(L-1,1) < h ) .* s(1:L-1) .* ( 1 - s( 2:L ) );
    s = s - [ move ; 0 ] + [ 0 ; move ];
        
    avg = avg + s;
    
end

avg = avg / steps;
if ( sigma == 0 )
    plot( avg, 'kd' );
else
    plot( avg, 'b^' );
end
drawnow;

end