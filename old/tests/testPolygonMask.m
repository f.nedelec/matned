function p = test


for i=1:200
    
    points = 500 * rand(2,2);
   
    points(:,1) = points(:,1) - min( points(:,1) );
    points(:,2) = points(:,2) - min( points(:,2) );
    
    [ m, r ] = maskpolygon(  points );
    %showim( m );
    %drawnow;
    p(i, 1 ) = { points };
    
end