function testbarycenter2(  )

r  = 20;
mc = maskgrad1( r );

for d=1:5*r;
   
   im = immax(mc, mc, [d, 0], 1 );
   
   [cen, mom, rot, sv] = barycenter2( im );
   
   m(d,1:3) = [ rot(2:3) sv ];
   
end

figure(1);
plot( m(:,1)./m(:,2),'k','LineWidth', 3 );

figure(2);
plot( m(:,1)./m(:,3),'b','LineWidth', 3 );
