function testcenterfind(im)

show1(im);

for level=0.5:0.05:0.95
   
   center=centerfind(im,level);
   plot(center(2), center(1),'b+','MarkerSize',20);
   disp( sprintf('level %.2f : %.2f %.2f', level, center(1), center(2)) );
   
end