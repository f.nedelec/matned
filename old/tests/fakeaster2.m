function [im, L] = fakeaster2( length, nbmts )

noise  = 10;
level  = 10;
spread = 1;

im = noise * randn( length );
im = im - min(min(im));

cx = length * ( 0.25 + 0.5 * rand );
cy = length * ( 0.25 + 0.5 * rand );

for mt = 1:nbmts

    %if rem( i, 3 )
        L( mt ) = 0.1 * length * exp( rand );
        %else
    %    L( mt ) = 0.3  * size;
    %end
    
    angle = rand * 2 * pi;
    ca = cos( angle );
    sa = sin( angle );
    
    ab = 0 : 0.025 : L( mt );
    
    mtx = round( cx + ca * ab + spread * randn(size(ab)) );
    mty = round( cy + sa * ab + spread * randn(size(ab)) );
   
    inside = ( mtx > 0 ) & ( mtx < length ) & ( mty > 0 ) & ( mty < length );
        
    for i = 1:size(mtx,2)
        if inside( i )
            im( mtx(i), mty(i) ) = im( mtx(i), mty(i) ) + level * rand;
        end
    end
    
end

disp(sprintf('MTs mean length = %.2f', mean(L)));
return;

for i = 1 : 100*nbmts
    x = round( cx + spread * randn );
    y = round( cy + spread * randn );
    im( x, y ) = im( x, y ) + level * rand;
end
