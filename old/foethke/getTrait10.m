function T = getTrait10(debug)
%F.nedelec, May 2007
% trait 10: variance of bundles without a nucleus

if nargin < 1
    debug = 0;
end

T = -1;

if ~isdir('run0003')
    return
else
    cd('run0003');
end

bun = load('pombe.sta');
pam = getParameters('config.cym');
cell_length = 2 * ( pam.boxsize(1) + pam.boxsize(2) );


bsize = cell_length / 60;
edges = -cell_length:bsize:+cell_length;
nbins = size(edges, 2) - 1;
xmean = ( edges(1:nbins) + edges(2:nbins+1) ) / 2;


%sel   = find( bun(:,1) > 1000 );
cen_pos = bun(:, [3,6,9,12]);
cen_pos = reshape(cen_pos, numel(cen_pos), 1);

his   = histc(cen_pos, edges)';
his   = his(1:nbins);

%calculate the mean and variance
[ w, m, v ] = characteristics(xmean, his);


if debug == 1
    figure;
    plot(xmean, his / w);
    hold on;
end

%convolve with and overlap length of 1.5 um:

%Loiodice et al. Mol Biol Cell 16, 1756 (2005) give L_overlap = 1.4 +/- 0.5
%We use the upper limit, because this is what has the strongest effect on
%the convolution. (this is equivalent to having a variability in L-overlap)
overlap = 1.9;

ohis=zeros(1, nbins);
ixl = 1;
while xmean(ixl) < -overlap/2
    ixl = ixl + 1;
end
ixu = nbins;
while xmean(ixu) > overlap/2
    ixu = ixu - 1;
end

ohis(ixl:ixu) = 1 / (ixu - ixl+1);

chis = conv(his, ohis);
cxmean = 2*xmean(1):bsize:2*xmean(numel(xmean));

%calculate the mean and variance
[ cw, cm, cv ] = characteristics(cxmean, chis);
%[ cw, cm, cv ] = characteristics(xmean, ohis);


if debug == 1
    plot(xmean, ohis, 'k');
    plot(cxmean, chis / cw, 'r');
    hold on;
    fprintf('variance %f, after convolution %f\n', v, cv);
end


T = cv;

cd('..');


end


function [w, m, v] = characteristics(x,y)
%calculate the mean and variance
w = sum( y );
m = sum( x .* y ) / w;
v = sum( x .* x .* y ) / w - m*m;
end

