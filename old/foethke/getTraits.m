function getTraits(fid, dir)

% function getTraits(fid, dir)
%
% calculate the 10 traits for the simulation stored in the directory 'dir'

if nargin < 1
    fid = 1;
end

if nargin < 2
    dir = '.';
else
    wd = pwd;
    cd(dir);
    fprintf(fid, '%s', dir);
end


T = getTraits127();
fprintf(fid, '  %7.3f', T);

T = getTraits89();
fprintf(fid, '  %7.3f', T);

T = getTrait10();
fprintf(fid, '  %7.3f', T);

fprintf(fid, '\n');


if exist('wd', 'var')
    cd(wd);
end