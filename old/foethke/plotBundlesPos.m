function s=plotBundles(s)
%to analyse the bundle positions in S.pombe simulations
%F.nedelec, Nov 2005

if ( nargin < 1 )
    s = load('pombe.sta');
end

col = ['g', 'b', 'r', 'm'];

figure('Position', [10 500 1250 600]);
%plot(s(:,1), s(:,2),'k-', 'Linewidth', 2);
hold on;
plot(s(:,1), s(:,2)+1,'k:');
plot(s(:,1), s(:,2)-1,'k:');

%plot the bundles center position:
plot(s(:,1), s(:, 3), [col(1), ':'], 'Linewidth', 1);
plot(s(:,1), s(:, 6), [col(2), ':'], 'Linewidth', 1);
plot(s(:,1), s(:, 9), [col(3), ':'], 'Linewidth', 1);
plot(s(:,1), s(:,12), [col(4), ':'], 'Linewidth', 1);

%plot the bundles tip positions:
plot(s(:,1), s(:, 4), col(1), 'Linewidth', 1);
plot(s(:,1), s(:, 7), col(2), 'Linewidth', 1);
plot(s(:,1), s(:,10), col(3), 'Linewidth', 1);
plot(s(:,1), s(:,13), col(4), 'Linewidth', 1);

plot(s(:,1), s(:, 5), col(1), 'Linewidth', 1);
plot(s(:,1), s(:, 8), col(2), 'Linewidth', 1);
plot(s(:,1), s(:,11), col(3), 'Linewidth', 1);
plot(s(:,1), s(:,14), col(4), 'Linewidth', 1);


set(gca, 'Position', [0.05 0.1 0.9 0.8]);
axis([0 10000 -6 6]);

fprintf('mean, variance nucleus = %f  %f\n', mean(s(:,2)), var(s(:,2)));
fprintf('mean, variance bundles:');

indx=[3,6,9,12];
fprintf(' %f %f', mean(s(:,indx)),  var(s(:,indx)));
fprintf('\n');
