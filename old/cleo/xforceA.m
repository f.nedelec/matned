function xforce(x, quad)
% 3 = right, 2 = top, 1 = bottom, 0 = left

left  = min(x(:,1));
right = max(x(:,1));
step  = 1;

%the angle
a = atan2( x(:,4), x(:,3) );

hold off;
mov = 0;
for d=left+step:step:right
    
    i  = find( ( x(:,1) > d-step ) & ( x(:,1) <= d ) );
    xs = x(i,:);
    as = a(i,:);
    
    %---------- all
    r = find( xs(:,2) > 0 );
    l = find( xs(:,2) < 0 );
    %plot(d,-sum( xs(r,6) ),'r.');
    %plot(d,-sum( xs(r,6) ),'g.');
    

    plot(as(r), -xs(r,6), 'r.');
    text(-2,-60,sprintf('x=%.2f',d));
    axis([-pi pi -200 200]);
    
    mov = mov + 1;
    F(mov) = getframe;
end

for d=right:-step:left-step
    
    i  = find( ( x(:,1) > d-step ) & ( x(:,1) <= d ) );
    xs = x(i,:);
    as = a(i,:);
    
    %---------- all
    r = find( xs(:,2) > 0 );
    l = find( xs(:,2) < 0 );
    %plot(d,-sum( xs(r,6) ),'r.');
    %plot(d,-sum( xs(r,6) ),'g.');
    

    plot(as(l), -xs(l,6), 'g.');
    text(-2,-60,sprintf('x=%.2f',d));
    axis([-pi pi -200 200]);
    
    mov = mov + 1;
    F(mov) = getframe;
        
end


movie(F,50);