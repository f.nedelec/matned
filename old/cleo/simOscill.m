function simOscill

figure; hold on;
x = 0;
v = 1;

for i=0:1000
    f = force(x,v);
    v = 0.1*f;
    x = x + 0.1*v;

    plot(x,v,'.');
end


function f=force(x,v);

if (v>0)
    f =  1 - x;
else 
    f = -1 - x;
end
    
return;
