function reg = spindle( filename, im )

% function spindle( filename, im )
% Last updated August 17, 2003

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = tiffread_buf( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

if ( isfield(im, 'data') ) 
    pixels = double( im.data ); 
end

%==================find the background level:

%rect       = ROImouserect;
%blackarea  = crop( pixels, rect );
%base1      = mean( mean( blackarea ));
base1      = min(min( pixels ));
base2      = imbase( pixels );
%disp( sprintf('base = %i,   %i', round(base1), round(base2) ) );

%==================   select a rectangular ROI:

showim( pixels );
drawnow;
%showim( pixels );

%roi   = ROImouserect;
poly = ROImousepoly;
[mask, roi] = maskpolygon( poly );
%showim( mask );
close;

%==================   extract the image from the ROI:
%subim  = crop( pixels, roi ) .* mask;

%base1s      = min(min( subim ));
%base2s      = imbase( subim );

subim  = ( crop( pixels, roi ) - base2 ) .* mask;
subim  = subim .* ( subim > 0 );

%==================   fit an ellipse to the sub-image

[cen, mom, rot, sv] = barycenter2( subim );

degres = - rot(1) * 180.0 / pi;
imrot = imrotate( subim, degres, 'bilinear' );

showim( imrot );
[cen, mom, rot, sv] = barycenter2( imrot, 10 );

disp(sprintf('angle = %.3f\n', rot(1)));
disp(sprintf('centre = %.3f %.3f\n', cen(1), cen(2)));
size(imrot)
size(cen)
bbox = [ cen-50, cen+50 ];
reg = crop( imrot, bbox, 1 );

showim(reg);
return;

%==================   find the center of the aster, as the brightest spot:

immin = min( min( subim ));
immax = max( max( subim ));

%==================   make a summary figure:

figid=figure('Name', ['from file ', im.filename], 'Position', [ 100, 100, 800, 400 ] );
set(gca, 'Position', [0.05 0.05 0.45 0.95]);
showim( subim, 'nofigure', 'axisticks', 'trueratio' );


%==================   find the mean pixel value inside

ellmask  = maskellipse( [ rot(1), 2*rot(2:3) ] );
ellsize  = size( ellmask );
botleft  = round( cen - ellsize / 2 );
topright = botleft + ellsize - [1 1];

size(subim)
size(ellmask)
subsubim = ellmask .* crop( subim, [ botleft, topright ] );
%showim( subsubim );
total_intensity = sum( sum( subsubim ) );
total_surface   = sum( sum( ellmask ) );
mean_intensity  = total_intensity / total_surface;
total_intensity = 10^-5 * total_intensity;

figure(figid);
axes( 'Position', [ 0.51 0.13 0.4 0.7 ] );
title( im.filename, 'FontSize', 14, 'Interpreter', 'none' );

mess1 = sprintf('mean  intensity %5.0f a.u.\n',  mean_intensity );
mess2 = sprintf('total intensity %5.0f 10^5 a.u.\n', total_intensity);
mess3 = sprintf('length %7.0f pixels\n', 4 * rot( 2 ) );
mess4 = sprintf('width  %7.0f pixels\n', 4 * rot( 3 ) );
mess5 = sprintf('surface %6.0f pixels^2\n', total_surface);

text( 0 , 0.5, [mess1, mess2, mess3, mess4], 'FontSize', 14, 'FontName', 'fixedwidth');
axis off;

%=============prepare for printing:

set(gcf,'PaperType','A4');
set(gcf,'PaperOrientation','portrait');
set(gcf,'PaperPositionMode','manual');
set(gcf,'PaperUnits','centimeters');
set(gcf,'PaperPosition',[2 12 17 8.5]);
