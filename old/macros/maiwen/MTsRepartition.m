function Nb = MTsRepartition (im);


prof = sum(im,1);
size(prof,2);
maximum = max(prof);
a = 0;
for cnt = 1 : size(prof,2)
    if (( prof(cnt)-maximum/2 ) >= 0 )
        a = a+1;
        
    end    
end

width = a;

prof = sum(im,2);

Nb = double(prof/width);
plot(Nb,'r');


return;