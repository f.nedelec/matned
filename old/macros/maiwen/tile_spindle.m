function tile = tile_spindle( avg );

fichier = dir('REG*.tif');

nbi = size( fichier, 1 );
width = fix( sqrt( nbi ) );
height = ceil( nbi / width );

for cnt = 1 : size( fichier, 1 )
    
    inname = fichier(cnt).name;
    
    register = tiffread( inname );
    
    
    if ( cnt == 1 )
        imsize = size( register.data );
        disp(sprintf('reference image %s of size %i %i', inname, imsize(1), imsize(2)));
        pix_size = [ imsize(1) * width, imsize(2) * height ];
        tile = zeros( pix_size(1), pix_size(2));
    end
    
    
    x = mod( cnt-1, width );
    y = (cnt - 1 - x) / width;
    xp = x * imsize(1);
    yp = y * imsize(2);
    
    if all( imsize == size(register.data) )
        if ( nargin >= 1 )
            spin = double( register.data );
            spin = spin/ sum(sum( spin ));
            tile( 1+xp:imsize(1)+xp, 1+yp:imsize(2)+yp ) = spin - avg;
        else
            tile( 1+xp:imsize(1)+xp, 1+yp:imsize(2)+yp ) = register.data;
        end
    else
        disp(sprintf('size differ: %i %i  and %i %i', imsize(1), imsize(2), ...
        size(register.data,1), size(register.data,2) ));
    end
end

showim(tile);