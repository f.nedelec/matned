function [ avg, sec ] = avg_spindle;

%fichier = dir('REG*.tif');
fichier = dir('REG*.tif');
nbi = size( fichier, 1 );

for cnt = 1 : size( fichier, 1 )
    
    inname = fichier(cnt).name;
    register = tiffread( inname );
    
    if ( cnt == 1 )
        imsize = size( register );
        avg = zeros( imsize(1), imsize(2));
        sec = zeros( imsize(1), imsize(2));
    end
    
    scaled = double( register.data );
    scaled = scaled ./ sum(sum( scaled ));
    
    avg = avg + double( scaled );
    sec = sec + double( scaled ) .^ 2;

end

avg = avg / nbi;
sec = ( sec - nbi * (avg .^ 2) ) / nbi;

showim(avg);
showim(sec);