function result = olivier(name, im)

if nargin < 2
    im = tiffread(name);
    %animate (im);
end

showim( im(1) );
poly = ROImousepoly;
[mask, roi] = maskpolygon( poly );
close;

   
result = zeros(length(im), 3);
cnt = 0;
while cnt < length(im)
    cnt=cnt+1;

    cpic = double(mask .* crop( double(im(cnt).data), roi ));
    
     if cnt == 1
         [figID, hIm] = showim(cpic,'axisticks','trueratio');
         pointer = get(gcf, 'pointer');
         set(gcf, 'pointer', 'fullcrosshair');
         texth = text (10, 10, ['image 1/', num2str(length(im))], 'color', 'r');
     else
         cpic = log(cpic);
         m=min(min(cpic));
         scal= 256/(max(max(cpic))-m);
         i = uint8( scal*(cpic-m)  );
         set( hIm, 'CData', i );
         set(texth,'string',['image',num2str(cnt), '/', num2str(length(im))]);
    end
    %disp(['frame', num2str(cnt)]);
    
    figure(figID);
    k = waitforbuttonpress;
    
    %stop if key pressed or right mouse button:
    if k==0 & strcmp( get( figID, 'SelectionType' ) ,'alt' )
        break;
    end 
    
    if k==1 & strcmp( get( figID, 'CurrentCharacter' ) ,' ' )
       continue; 
    end
    
    if k==1 & strcmp( get( figID, 'CurrentCharacter' ) ,'q' )
       cnt=cnt-2;
       continue;
   end
    
    point = get(gca, 'CurrentPoint');     % button down detected
    P = point(1, 2:-1:1);                 % extract x and y
    result(cnt, 1:2)=P;
    result(cnt, 3)=1;
end

set( gcf, 'pointer', pointer );
%figure;
%showim(cpic,'axisticks','nofigure','trueratio');
hold on;
for cnt = 1:length(im)
    if result (cnt, 3)==1
    plot( result(cnt,2), result(cnt,1) );
    plot( result(cnt,2), result(cnt,1), 'bx', 'MarkerSize',10, 'LineWidth', 2);

%plot( result(:,2), result(:,1) );
%plot( result(:,2), result(:,1), 'bx', 'MarkerSize',10, 'LineWidth', 2);
    end
end

set( gca, 'xtick', [ 0:50:size(cpic,2) ] , 'xticklabel', [0:50:size(cpic, 2)]);
set( gca, 'ytick', [ 0:50:size(cpic,1) ] , 'yticklabel', [ 0:50:size(cpic,1) ]);
    
for cnt = 1 :length(im)
    if result (cnt, 3) ==1
    text(result(cnt,2), result(cnt,1),[' ',num2str(cnt)]);
    
    end
end

dataname = strrep(name, '.stk', '.dat');
save(dataname, 'result', '-ascii');
disp(['saved in ', dataname] );

%=============prepare for printing:

setprint;

