function result = square( filename, square_size );
% usage:  1. square()
%         2. square(filename)
%         3. square(filename, square_size)
%
%  let the user click on a picture, and spits out data on
%  the brightness value in these square. background subtracted

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ( nargin < 2 )
    square_size = 20;
end

disp('click right mouse button to quit');
disp(sprintf('default square size is %i x %i', square_size, square_size));

square_radius = square_size / 2 ;

if ischar( filename )
    im = tiffread( filename );
    %animate (im);
else
    im = filename;
    filename = 'file not specified';
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

base1       = min(min( im ));
base2       = imbase( im );


figure = showim(im);
hold on;
axis manual;

savedpointer = get(figure, 'pointer');
set(figure, 'pointer', 'fullcrosshair');
set(figure, 'units', 'pixels')

click_nb = 1;
while ( click_nb < 100 )

    drawnow;
    k = waitforbuttonpress;
   
    %stop if key pressed or right mouse button:
    if k | strcmp( get( gcf, 'SelectionType' ) ,'alt' )
        break;
    end
    
    click  = get(gca, 'CurrentPoint');     % button down detected
    center = click(1, 2:-1:1);             % extract x and y
    
    rect = [ center - square_radius, center + square_radius ];
    
    x = [rect(1), rect(1), rect(3), rect(3), rect(1) ];
    y = [rect(2), rect(4), rect(4), rect(2), rect(2) ];
    plot(y, x);
    text(center(2)-square_radius, center(1), num2str( click_nb ), 'Color', 'blue' );
    drawnow;
    
    subim = crop( im, rect );
    pixel_area = size(subim, 1 ) * size(subim, 2);
    total_intensity = sum(sum( subim )) / pixel_area;
    total_corrected = sum(sum( subim - base2 )) / pixel_area;
    disp(sprintf('click %2i : avg. pix. val. %8.1f    black subtracted: %8.1f  a.u.',...
        click_nb, total_intensity, total_corrected ));
    click_nb = click_nb + 1;
    
end

set( figure, 'pointer', savedpointer );

return