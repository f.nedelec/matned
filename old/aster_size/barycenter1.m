function [ cen, sv ] = barycenter1 ( im )
% function [ cen, sv ] = barycenter1 ( im )
% compute the barycenter 'cen' of the pixels,
% taking the pixel values as weight
% sv is the sum of all values

global debug;

im = double( im );

[sizex, sizey] = size( im );

sv = sum( sum( im ) );
if ( sv == 0 ) 
   cen = [1+sizex, 1+sizey] / 2;  %the middle
   return;
end

ax = [1:sizex]  * sum( im, 2 );
ay = sum( im, 1 ) * [1:sizey]';

cen = [ ax/sv, ay/sv ];


return;

if debug==9
   show1( im ); 
   plot(cen(2), cen(1),'bo','MarkerSize',5,'Linewidth',4);
end

