function y = maskdistindx(n, bin)
% function y = maskdistindx(n, bin)
%   Draw a circular gradient in a (2n)^2 square
%   values are greater than 1 and can be used as array index
%
% f nedelec, nedelec@embl.de


if ( nargin < 2 ) bin=1; end

y = floor( maskgrad1( n, bin ) ) + 1;

%maximum where the circle is still complete:
mx = y( n+1, 1 ) + 1;

%flatten the corners of the square, leaving the central circle:
y = min( y, mx*ones(size(y)) );

