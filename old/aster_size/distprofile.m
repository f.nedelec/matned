function [ m, s, c ] = distprofile ( im, center, bin, radius, mask )
% function [ m, s, c ] = distprofile ( im, center, bin, radius, mask )
%
% return the mean ( m ) , the variance ( s ), and the count of pixels ( c ),
% all as functions of the distance from the center, in units of bin pixels.
% when the optional mask is given ( same size as im ),
% only those pixels corresponding to mask = 1 are considered

if ( nargin > 4 ) & any( size(mask) ~= size(im) )
    disp('error, mask and im should have the same sizes');
    return;
end

if ( nargin < 4 )
    radius = floor( max( [ center, size( im ) - center ] ) );
end

if ( nargin < 3 )
    bin = 2;
end

if ( isfield(im, 'data') ) 
    im = double( im.data );
end

indx = maskdistindx( radius, bin );

last  = indx( 1, 1 );
cen   = round( center );
imc   = crop( im,   [ cen - radius, cen + radius ], 0 );
indxc = crop( indx, [ [1 1]-cen, size(im)-cen ] + radius + 1, 0 );

if  exist('mask', 'var')
    maskc = crop( mask,   [ cen - radius, cen + radius ], 0 );
end

mn = min( min( indxc ) );
mx = max( max( indxc ) );
m  = zeros( 1, mx );
s  = zeros( 1, mx );
c  = zeros( 1, mx );

for i = mn:mx
    which  = find( indxc == i ); 
    if exist( 'mask', 'var' )
        in  = find( maskc( which ) == 1 );
        which = which( in );
    end
    m( i ) = sum( sum( imc( which ) ));
    s( i ) = sum( sum( imc( which ).^2 ));
    c( i ) = length( which );
end

% remove the last point:
if ( size(c,2) >= last )
   m  = m(1:last-1);
   c  = c(1:last-1);
   s  = s(1:last-1);
   mx = last - 1;
end

% processing m and s:

m(mn:mx) = m(mn:mx) ./ c(mn:mx);
s(mn:mx) = sqrt( s(mn:mx) ./ c(mn:mx) - m(mn:mx).^2 );

