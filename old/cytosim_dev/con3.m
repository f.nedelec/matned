function con3
%simulate a string of points, of fixed length and given rigidity

%dynamic of a string of points with fixed length, and rigidity
N        = 41;        %nb of points
L        = 0.5;         %length of the bonds:
km       = 100;

rigid    = 20;        %rigidity
rkm      = rigid / ( L ^ 3 );
fkm      = 1;         %feed back coefficient

dt       = 2.5e-4;
nbsteps  = 100*10;
rec      = 10;

%mobility of the rods, of length L
mu    = 1/L;

LN = L * N;


%matrix defining external spring forces:
A = zeros(2*N, 1);              %attachement points
B = zeros(2*N, 2*N);            %attachment points on the mt
z = 0.5;
t = 1/2 - LN/3;
A(1)       =  t;
A(2)       = -z;
B(1,1)     = -1;
B(2,2)     = -1;

A(N)       =  0;
A(N+1)     =  z;
B(N,N)     = -1;
B(N+1,N+1) = -1;

A(2*N-1)   = -t;
A(2*N)     = -z;
B(2*N-1,2*N-1) = -1;
B(2*N,2*N) = -1;

A = km * A;
B = km * B;




figure('Position',[ 200, 200, 800, 800], 'MenuBar','None', 'Name','Euler');
set(gca,'Position',[0 0 1 1]);


%vector x contains [x,y] of all successive points

x  = zeros(N*2, 1);          %positions
F  = zeros(N*2, 1);          %external applied forces
FI = zeros(N*2, 1);          %internal forces due to rigidity
FV = zeros(N*2, 1);          %internal forces due to inextensibility

%initial position is straight:
for i=1:N
   t = ( i-(N+1)/2 ) * L ;
   x(2*i-1:2*i) = [ t, 0];
end

%building the matrix for rigidity forces:
M1 = zeros(N,N);
for i=1:N;     M1(i, i) = -6;  end
for i=1:N-1;   M1(i, i+1) =  4; M1(i+1, i) =  4;  end
for i=1:N-2;   M1(i+2, i) = -1; M1(i, i+2) = -1;  end
M1(1:2, 1:2)     = [-1 2; 2 -5];
M1(N-1:N, N-1:N) = [-5 2; 2 -1];

M = zeros(2*N, 2*N);
M(1:2:2*N, 1:2:2*N) = M1;
M(2:2:2*N, 2:2:2*N) = M1;
M = M * rkm;
   
substep = round( nbsteps / rec );
sav=1;

%start the timer:
tic

%integrating the motion:
for t=0:nbsteps
   
   %display the position
   if ( rem( t, substep ) == 0 )
      plot( x(1:2:2*N-1), x(2:2:2*N), '-ko');
      axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
      hold on;
      %plot the force vectors:
      j=1;
      if ( 1 == 0 )
      for i=1:N
         plot( [ x(2*i-1), x(2*i-1) + j*F(2*i-1) ],...
            [ x(2*i), x(2*i) + j*F(2*i)], '-k', 'Linewidth', 3);
         
         plot( [ x(2*i-1), x(2*i-1) + j*( FI(2*i-1) ) ],... 
            [x(2*i), x(2*i) + j*( FI(2*i) )], '-b', 'Linewidth', 3);
         
         plot( [ x(2*i-1), x(2*i-1)+j*FV(2*i-1) ],...
            [x(2*i), x(2*i)+j*FV(2*i)], '-r', 'Linewidth', 3);
      end
      end
      pause(0.1);
      %hold off;
      %saving position:
      sol(sav, 1:2*N) = x';
      sav = sav + 1;
   end
      
   %building the jacobian of length constraints:
   J = zeros(N-1, 2*N);
   for r=1:N-1
      J(r, 2*r-1) = x(2*r-1) - x(2*r+1);
      J(r, 2*r)   = x(2*r)   - x(2*r+2);
      J(r, 2*r+1) = x(2*r+1) - x(2*r-1);
      J(r, 2*r+2) = x(2*r+2) - x(2*r);
   end   
   
   %J*J' is symetric definite positie
   
   JJJJ = eye(2*N) - J' * inv(J * J') * J;
      
   %adding the feed-back term on the length constraints:
   %(non linear)
   l = zeros(N-1,1);
   for i=1:N-1
      d    = sqrt( ( x(2*i+1) - x(2*i-1) )^2 + ( x(2*i+2) - x(2*i) )^2 );
      l(i) = ( L - d ) / d;
   end
   FV = fkm * J' * l;
   
   %external spring force:
   F = A + B * x;

   %Forward Euler:
   x = x + ( JJJJ * ( F + M * x ) + FV ) .* ( mu * dt );
end

toc

clf;
set(gca,'Position',[0 0 1 1]);
for i=1:sav-2
   for j=1:N
      plot( [ sol(i,j*2-1), sol(i+1,j*2-1)], [sol(i,j*2), sol(i+1,j*2)], '-ko');
      hold on;
   end
end
axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
%plot the force vectors:


dis1 = sqrt( ( sol(:,4) - sol(:,2) ) .^ 2 + ( sol(:,3) - sol(:,1) ) .^ 2 );
dis2 = sqrt( ( sol(:,6) - sol(:,4) ) .^ 2 + ( sol(:,5) - sol(:,3) ) .^ 2 );
dis3 = sqrt( ( sol(:,8) - sol(:,6) ) .^ 2 + ( sol(:,7) - sol(:,5) ) .^ 2 );

figure('Name','Euler');
plot(dis1);
hold on;
plot(dis2);
plot(dis3);
hold off;