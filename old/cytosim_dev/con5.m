function con5
%simulate a string of points, of fixed length and given rigidity
%implicit integration

%dynamic of a string of points with fixed length, and rigidity
N        = 41;        %nb of points
L        = 2;         %length of the bonds
rigid    = 100;       %rigidity of polymer
km       = 1;         %rigidity of spring-links

rkm      = rigid / ( L ^ 3 );
fb       = 0.33;      %feed back coefficient for length constraint

dt       = 100e-1;
nbsteps  = 200;
rec      = 30;

%mobility of the rods, of length L
mu    = 1/L;

LN    =  L * N;
mudt  = mu * dt;

figure('Position',[ 200, 200, 800, 800], 'MenuBar','None', 'Name','Implicit');
set(gca,'Position',[0 0 1 1]);

%matrix defining external spring forces:
A = zeros(2*N, 1);              %attachement points
B = zeros(2*N, 2*N);            %attachment points on the mt
z = 3;
t = 1/2 - LN/3;

%mt-mt link:
B(1,1)     = -1;
B(2,2)     = -1;

B(1,2*N-1) = 1;
B(2*N-1,1) = 1;

B(2,2*N)   = 1;
B(2*N,2)   = 1;

B(2*N-1,2*N-1) = -1;
B(2*N,2*N)     = -1;

%grafted link:
%A(N)       =  0;
%A(N+1)     =  z;
%B(N,N)     = -1;
%B(N+1,N+1) = -1;


A = km * A;
B = km * B;


%vector x contains [x,y] of all successive points

x  = zeros(N*2, 1);          %positions

%initial position is circular:
LN = L * N;
R  = LN / pi / 2;
for i=1:N
   t = ( i-(N+1)/2 )*L / R;    %angle
   x(2*i-1:2*i) = [ R*sin(t), R*(cos(t))];
end

%building the matrix for rigidity forces:
M1 = zeros(N,N);
for i=1:N;     M1(i, i)   = -6;  end
for i=1:N-1;   M1(i, i+1) =  4; M1(i+1, i) =  4;  end
for i=1:N-2;   M1(i+2, i) = -1; M1(i, i+2) = -1;  end
M1(1:2, 1:2)     = [-1 2; 2 -5];
M1(N-1:N, N-1:N) = [-5 2; 2 -1];
M1 = M1 * rkm;

M = zeros(2*N, 2*N);
M(1:2:2*N, 1:2:2*N) = M1;
M(2:2:2*N, 2:2:2*N) = M1;
   
substep = round( nbsteps / rec );
sav=1;

%start the timer:
tic

%integrating the motion:
for t=0:nbsteps
   
   %display the position
   if ( rem( t, substep ) == 0 )
      plot( x(1:2:2*N-1), x(2:2:2*N), '-ko');
      axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
      hold off;
      pause(0.1);
      %hold off;
      %saving position:
      sol(sav, 1:2*N) = x';
      sav = sav + 1;
   end

         
   %building the jacobian of length constraints:
   J = zeros(N-1, 2*N);
   for r=1:N-1
      J(r, 2*r-1) = x(2*r-1) - x(2*r+1);
      J(r, 2*r)   = x(2*r)   - x(2*r+2);
      J(r, 2*r+1) = x(2*r+1) - x(2*r-1);
      J(r, 2*r+2) = x(2*r+2) - x(2*r);
   end   
   
   %J*J' is symetric definite positie
   
   JJJJ = eye(2*N) - J' * inv(J * J') * J;
   
   PL = mudt .* ( JJJJ * ( M + B ) );
   %implicit, mid-point method:
   x = inv( eye(2*N) - PL/2 ) * ( ( eye(2*N) + PL/2 ) * x + mudt * ( JJJJ * A ) ) ;
   
   %Feed-back on the length constraints:
   dx = zeros(2*N-1, 1);
   dy = zeros(2*N-1, 1);
   for i=1:N-1
      d  = sqrt( ( x(2*i+1) - x(2*i-1) )^2 + ( x(2*i+2) - x(2*i) )^2 );
      dl = ( d - L ) / d;
      dx(i) = dl * ( x(2*i+1) - x(2*i-1) );
      dy(i) = dl * ( x(2*i+2) - x(2*i) );
   end
   
   x(1)  = x(1) + fb * dx(1);
   x(2)  = x(2) + fb * dy(1);
   for i=2:N-1
      x(2*i-1) = x(2*i-1) + fb * ( dx(i) - dx(i-1) );
      x(2*i)   = x(2*i)   + fb * ( dy(i) - dy(i-1) );
   end
   x(2*N-1)  = x(2*N-1) - fb * dx(N-1);
   x(2*N)    = x(2*N)   - fb * dy(N-1);

end

toc

clf;
set(gca,'Position',[0 0 1 1]);
for i=1:sav-2
   for j=1:N
      plot( [ sol(i,j*2-1), sol(i+1,j*2-1)], [sol(i,j*2), sol(i+1,j*2)], '-k');
      hold on;
   end
end
axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
%plot the force vectors:


dis1 = sqrt( ( sol(:,4) - sol(:,2) ) .^ 2 + ( sol(:,3) - sol(:,1) ) .^ 2 );
dis2 = sqrt( ( sol(:,6) - sol(:,4) ) .^ 2 + ( sol(:,5) - sol(:,3) ) .^ 2 );
dis3 = sqrt( ( sol(:,8) - sol(:,6) ) .^ 2 + ( sol(:,7) - sol(:,5) ) .^ 2 );

figure('Name','Implicit');
plot(dis1);
hold on;
plot(dis2);
plot(dis3);
hold off;