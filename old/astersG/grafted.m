%macro to plot the data for Current Biology review figure, July 2003

figure;
hold on;
sim_total = 0; 
sim_good  = 0;
    
for iii = 0 : 16;
    
    folder = sprintf('graft%i', iii);
    disp(folder);
    cd(folder);
    load collect;
   
    load speedF;
    sim_nb = size( speedF, 1 );
    sim_total = sim_total + sim_nb;
    
    for i = 1 : sim_nb;
        
        name   = speedF( i, 1 );
        speed1 = speedF( i, 2 );
        speed2 = speedF( i, 3 );
        frame  = speedF( i, 4 );
        
        index  = find( collect(:,1) == name );
        if ~ isempty( index )
            lifetime = collect( index, 2 );
            distmin  = collect( index, 3 );
            linkmin  = collect( index, 7 );
        else
            disp(sprintf('missing collect info for %i\n', name));
            continue;
        end
        
        if ( frame < 21 )
            disp(sprintf('unfinished %i\n', name ));
            continue;
        end
        
        if ( frame > 100 )
            
            sim_good = sim_good + 1;
            
            if ( abs(speed1) < 0.03 ) || ( abs( speed2 ) < 0.03 )
                %plot( speed1, speed2, 'x', 'MarkerSize', 8);
                if ( speed1 > 0.3 )
                    disp(sprintf('check %i', name)); 
                else
                    %disp(sprintf('trivial: %i : %f %f', name, speed1, speed2));
                end
            else
                %plot( speed1, speed2, 'o', 'MarkerFaceColor', 'b');                
                %disp(sprintf('good:    %i : %f %f', name, speed1, speed2));
            end
        
        end
        
        markersize = 0.1 + ( lifetime - 200 ) / 80;
        if ( distmin < 1.1 )
            plot( speed1, speed2, '+', 'MarkerEdgeColor', [0.6 0.6 1], 'MarkerFaceColor', 'none', 'MarkerSize', markersize+2);
        else      
            plot( speed1, speed2, 'o', 'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor', 'none', 'MarkerSize', markersize);
        end
        %if ( frame > 100 )
        %    plot( speed1, speed2, '.', 'MarkerFaceColor', 'none', 'MarkerSize', 6);
        %end
    end
    
    cd ..;
end

xlabel('Speed of homo-complex ( um/s )','FontSize', 12);
ylabel('Speed of grafted motor ( um/s )','FontSize', 12);
set(gca,'FontSize', 12);
set(gca,'Box', 'on');
set(gca,'XTick', [-1, -0.5, 0, 0.5, 1] );
set(gca,'YTick', [-1, -0.5, 0, 0.5, 1] );

disp( sprintf('total sim %i, good %i\n', sim_total, sim_good ));