function pombeEval2()

  global GPD;
  
  % set path
  CD.basePath    = '/disks/nedelec1/foethke/SCREEN_4';
  CD.savePath    = fullfile(CD.basePath, 'SAVE');
  CD.analysePath = fullfile(CD.basePath, 'ANALYSE');
  CD.dataFile    = fullfile(CD.analysePath, 'complete.dat');
  
  % read data
  [gr, shr, cat, res, mean, spread, name] = textread([CD.dataFile], '%f %f %f %f %f %f %s');
  if ~ ((size(gr,1) == size(shr,1)) && ...
        (size(gr,1) == size(cat,1)) && ...
        (size(gr,1) == size(res,1)) && ...
        (size(gr,1) == size(mean,1)) && ...
        (size(gr,1) == size(spread,1)) && ...
        (size(gr,1) == size(name,1)))
    error('error reading datafile: columns don not have equal length!');
  end
  numPoints = size(gr,1);
  

  % set structs PD, GPD and fields  
  PD.xLabel    = 'growth speed / {\mu}m s^{-1}';
  PD.yLabel    = 'catastrophe frequency / s^{-1}';
%  PD.showColorBar = 0;
  
  GPD.data1      = zeros(numPoints,3);
  GPD.data1(:,1) = gr;
  GPD.data1(:,2) = cat;
  GPD.data1(:,3) = spread;
  GPD.infoStr1   = cell(numPoints, 1);
  GPD.execStr1   = cell(numPoints, 1);
  for n=1:numPoints
    GPD.infoStr1{n} = sprintf('Simulation %s: growth: %f, shrinkage: %f, catastrophe: %f, rescue: %f', ...
                            name{n}, gr(n), shr(n), cat(n), res(n));
    GPD.execStr1{n} = sprintf('play file=%s &', fullfile(CD.savePath, name{n}, '/result.out'));
  end

  fields.data = 'data1';
  fields.info = 'infoStr1';
  fields.exec = 'execStr1';
  
  % go
  plotData( fields, PD );
  return;
