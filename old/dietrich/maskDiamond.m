% y = maskDiamond(n);
%
% Draw a solid diamond of ones with diameter n pixels 
% in a square of zero-valued pixels.
%
% Example: y = maskDiamond(128);
% CVS: $Id: maskDiamond.m,v 1.2 2004/09/20 15:18:29 foethke Exp $

function y = maskDiamond(n)

  if rem(n,1) > 0, 
    disp(sprintf('n is not an integer and has been rounded to %1.0f',round(n)))
    n = round(n);
  end

  if n < 1     % invalid n
    error('n must be at least 1')
   
  elseif n < 3 % trivial n
    y = ones(n);

  elseif rem(n,2) == 0,  % even n
    quarterDiamond = zeros(n/2);

    for h=1:n/2
      for w=1:h
        quarterDiamond(h,w) = 1;
      end
    end

    upperDiamond = [fliplr(quarterDiamond) quarterDiamond];
    y = [upperDiamond; flipud(upperDiamond)];

  else   % odd n
    quarterDiamond = zeros((n-1)/2);

    for h=2:(n-1)/2
      for w=1:h-1
        quarterDiamond(h,w) = 1;
      end
    end
  
    upperDiamond = [fliplr(quarterDiamond) ones((n-1)/2,1) quarterDiamond];
    y = [upperDiamond; ones(1,n); flipud(upperDiamond)];
  end
  return
  