function h = imhist( im )

maxval = max( max( im ) );
maxval = round( double( maxval ));

h = sum( histc( round( double( im ) ), 0:maxval ), 2);
%h = hist( reshape( double(im), size(im,1) * size(im,2),1 ), 1:maxval);

return;
