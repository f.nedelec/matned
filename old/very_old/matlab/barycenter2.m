function [cen, mom, rot, sv] = barycenter2( im, debug )

% [cen, mom, rot, sv] = barycenter2( im )
%
% compute the barycenter <cen> of the pixels
% and their second moments along the axis:
%     <mom> = [ mxx, myy, mxy ]
% <rot> = [ ang, dmax, dmin ]:
%     <ang>  angle of maximum moment
%     <dmax> major axis size = sqrt( mom(ang) / sv ) 
%     <dmin> minor axis size = sqrt( mom(ang+pi/2) / sv ) 
% <sv> sum of all pixel values 
%
% F. Nedelec

if ( nargin < 2 )
    debug = 0;
end

im = double( im );

[sizex, sizey] = size( im );

sv = sum( sum( im ) );

if (sv == 0) 
   disp( 'barycenter2: sum of pixel ==  0' );
   cen = [ sizex+1, sizey+1]/2;  %the middle
   mom = [ 0 0 0 ];
   rot = [ 0 0 0.01 ];
   return;
end

ssy =  sum( im, 2 );
xx  = [1:sizex];
sx  = xx * ssy;
sxx = (xx.^2) * ssy;

ssx = sum( im, 1 );
yy  = [1:sizey]';
sy  = ssx * yy ;
syy = ssx * (yy.^2);

sxy = xx * im * yy;

mxx = sxx - sx^2/sv;
myy = syy - sy^2/sv;
mxy = sxy - sx*sy/sv;

cen = [ sx/sv, sy/sv ];
mom = [ mxx, myy, mxy ];

mmm = (mxx - myy)^2 + (2*mxy)^2;
if ( mxy ~= 0 )
   ang = atan( ( myy - mxx + sqrt( mmm ) ) / ( 2 * mxy ) );
else
   if ( mxx > myy )
      ang = 0;
   else
      ang = pi/2;
   end
end

mmax = cos(ang)^2 * mxx + sin(ang)^2 * myy + 2*cos(ang)*sin(ang) * mxy;
mmin = sin(ang)^2 * mxx + cos(ang)^2 * myy - 2*cos(ang)*sin(ang) * mxy;

ddmax = sqrt( mmax / sv );
ddmin = sqrt( mmin / sv );

rot = [ ang, ddmax, ddmin ];


%draws the center, and two pairs of points for its axis:
if (debug == 8)
    
   showim(im, 'small' );
   plot(cen(2), cen(1), 'bo', 'MarkerSize', 17, 'Linewidth', 2);
   dx = ddmax * cos(ang);
   dy = ddmax * sin(ang);
   plot(cen(2)+dy, cen(1)+dx,'bo','MarkerSize',5,'Linewidth',4);
   plot(cen(2)-dy, cen(1)-dx,'bo','MarkerSize',5,'Linewidth',4);
   dx = -ddmin * sin(ang);
   dy =  ddmin * cos(ang);
   plot(cen(2)+dy, cen(1)+dx,'bo','MarkerSize',3,'Linewidth',2);
   plot(cen(2)-dy, cen(1)-dx,'bo','MarkerSize',3,'Linewidth',2);
   
end

%draws an ellipse surrounding the features on top of the picture:
if (debug == 9)
    
   mask = maskellipse( [ ang, 2*[ddmax ddmin] ], 5 );
   r = filteredge1( mask ) > 0;
   p = imoverlay( im, max(max(im))*r, round( cen - size(r)/2 ), 2 );
   showim(p);
   
end

if (debug == 10)
    
   drawellipse( cen, [ ang, 2*[ddmax ddmin] ] );
   
end


return;