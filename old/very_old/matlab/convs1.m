function f = convs1 ( h, c )
% f = conv1( h, c ) compute the convolution of h by c
% defined by f = sum( c(i) * h(x+dx) ) / sum( c );
% example of guaussian kernels:
% c = [ 1 1 1 2 3 4 7 13 26 55 26 13 7 4 3 2 1 1 1 ];

if ( rem( length(c) + 1, 2 ) ) 
   disp( 'error in conv1: function c must have an odd size' );
   return;
end

dx = [1:length(c)] - ( length(c) + 1 )/2;

l = 1 + max(dx);
u = length(h) - max(dx);

hs = zeros( size(h) );

for i = 1:length(c);
   hs(l:u) = hs(l:u) + c(i) * h( l+dx(i):u+dx(i) );
end

sumc = sum( c );
if (sumc)
   f = hs ./ sumc;
else
   f = hs;
end