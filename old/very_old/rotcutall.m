function names = rotcutall ( bin, mode )

% function rotcut, performs a circular intergration of a profile.
textsize=12;

%find all the image files:
[filenb, names] = findfiles;

centers = zeros(2,filenb);


if (mode == 0)
   figure('Name','Click on the center');
   colormap(gray);
   set(gca,'Position',[0 0 1 1]);
   axis off;
   for indx = 1:filenb;
      name = char(names(indx));
      %load the picture:
      im = tiffread(filename(name,'motor'));
      %get the mouse click:
      imagesc( im );
      center = round( ginput(1) )';
      centers(1,indx) = center(2);
      centers(2,indx) = center(1);
   end;
end


%go thru the pictures:
for indx = 1:filenb;
   name = char(names(indx));
   fileexists = dir(filename(name,'data'));
   if ( size(fileexists,1) == 0 )
      %compute the profiles:  
      if (mode == 1)
         im = tiffread(filename(name,'motor'));
         centers(1:2,indx) = centercompute( im );
      end
      if (mode == 2)
         %or just define it:
         im = tiffread(filename(name,'motor'));
         centers(1,indx) = size(im,1) ./ 2.0;
         centers(2,indx) = size(im,2) ./ 2.0;
      end
      center(1:2)=centers(1:2,indx);
      rotcut(name, bin, center);
   end
      
   close all;
   %rcplot(name);
   
   disp(strcat('done with :',name));
   %pause(2);
end;

