function figid = show2( im1, im2, textcode, varargin)
%show the two pictures, side by side
%with center if provided
if ( exist('textcode','var') == 0 ) textcode='scale8bit'; end

imsize1 = size( im1 );
imsize2 = size( im2 );

if ( imsize1 ~= imsize2 ) disp('image of different size!'); end

scale  = min( 600 ./ imsize1 );
if (scale > 1) scale = round(scale); end

figpos = [10 40 2*scale*imsize1(2) scale*imsize1(1)];
fig    = figure('Position', figpos, 'MenuBar', 'None');

set(gca,'Position',[0 0 0.5 1]);
show1(im1, [textcode 'figdone'], varargin);

figure(fig);
axes('Position',[0.5 0  0.5  1]);
show1(im2, [textcode 'figdone'], varargin);

return;