function [cen1, cen2] = centerfind2s( im, radius, level, debug )

if (exist('radius','var') == 0) radius = 50; end
if (exist('level','var')  == 0) level  = ''; end
if (exist('debug' ,'var') == 0) debug  = 0;  end

[sx sy] = size(im);
mask    = maskcircle1(2*radius);
imd     = double( im );

%------------------------find the first center:
cen1 = barycenter1( imd, level, debug );

%------------------------mask it in the image:
cx = round ( cen1(1) );
cy = round ( cen1(2) );
mask2  = masktranslate(mask, cx-radius-1, cy-radius-1, sx, sy);
im2 = imd .* (1-mask2);

%------------------------find the second center:
cen2 = barycenter1( im2, level, debug );

%------------------------we push them away, if they are equal:
if ( round(cen1) == round(cen2) )
   [cen, mom, ang, sv] = barycenter2( im , level, debug )
   mmz = sqrt( sum(mom)/sv )
   dx = mmz * sin(ang);
   dy = mmz * cos(ang);
   cen1 = cen .+ [dx dy];
   cen2 = cen .- [dx dy];
end

if (debug==2)
   debugfig = show1(im);
   plot(cen1(2), cen1(1),'bo','MarkerSize',10);
   plot(cen1(2), cen1(1),'bo','MarkerSize',radius);
   plot(cen2(2), cen2(1),'ro','MarkerSize',10);
   plot(cen2(2), cen2(1),'ro','MarkerSize',radius);
end

%------------------------iterate to refine the centers:
[sx sy] = size(im);
mask    = maskcircle1(2*radius);

for i=1:10   
   %remove a circle around first center:
   cx = round ( cen1(1) );
   cy = round ( cen1(2) );
   mask1  = masktranslate(mask, cx-radius-1, cy-radius-1, sx, sy);
   cx = round ( cen2(1) );
   cy = round ( cen2(2) );
   mask2  = masktranslate(mask, cx-radius-1, cy-radius-1, sx, sy);
   
   %refine the centers:
   im2 = imd .* (1-mask2) .* mask1 ;
   %if (exist('debug','var')) show1(im2, cen1); end
   cen1 = barycenter1( im2, level, debug );
   
   im2 = imd .* (1-mask1) .* mask2 ;
   %if (exist('debug','var')) show1(im2, cen2); end
   cen2 = barycenter1( im2, level, debug );
      
   if (debug==2)
      plot(cen1(2), cen1(1),'b+','MarkerSize',20);
      plot(cen1(2), cen1(1),'bo','MarkerSize',radius);
      plot(cen2(2), cen2(1),'r+','MarkerSize',20);
      plot(cen2(2), cen2(1),'ro','MarkerSize',radius);
   end
end

return;
