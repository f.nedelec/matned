function mask = maskipl( v, h )

if ( nargin < 2 )
    h = v(2);
    v = v(1);
end

radius = h*0.95;
circle = maskcircle1( radius );
ch = round( ( h - size(circle, 1) ) / 2 );
cv = round( ( v - size(circle, 2) ) / 2 );
mask = image_crop( circle, [-cv, -ch, v-cv-1, h-ch-1 ], 0);
