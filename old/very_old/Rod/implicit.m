
function sol = implicit();
%try to solve the equation for movement of a solid under all the springs

%maple('restart');

mom = 'mom(x,y,dx,dy) = dx * ( a - Ax*y + Ay*x ) - dy * ( b - Ax*x - Ay*y )';
fx  = 'fx(x,y,dx,dy)  = Gx - dx * Ax + dy * Ay - K * x';
fy  = 'fy(x,y,dx,dy)  = Gy - dy * Ax - dx * Ay - K * y';

maple(mom)
maple(fx)
maple(fy)

%eq1 = 'dxn - dx =  ( mom )^2 * dxn / 2 - ( mom ) * dyn ';
%eq2 = 'dyn - dy =  ( mom )^2 * dyn / 2 + ( mom ) * dxn ';

eq1 = 'eq1 := dxn - dx =  - mom(xn,yn,dxn,dyn ) * dy ';
eq2 = 'eq2 := dyn - dy =    mom(xn,yn,dxn,dyn ) * dx ';

%eq1 = 'dxn =  cos( mom ) * dx  -  sin( mom ) * dy ';
%eq2 = 'dyn =  cos( mom ) * dy  +  sin( mom ) * dx ';


eq1 = strrep( eq1, 'mom', mom );
eq2 = strrep( eq2, 'mom', mom );

eq3 = 'xn - x = fx(x,y,dx,dy)';
eq4 = 'yn - y = fy(x,y,dx,dy)';

disp( sprintf('eq1 := %s',eq1) );
disp( sprintf('eq2 := %s',eq2) );
disp( sprintf('eq3 := %s',eq3) );
disp( sprintf('eq4 := %s',eq4) );


cmd = sprintf('sol := solve({%s,%s,%s,%s},{xn,yn,dxn,dyn})',...
    eq1, eq2, eq3, eq4);



maple(cmd)


maple('sx  := rhs( sol[1] );');
maple('sy  := rhs( sol[2] );');
maple('sdx := rhs( sol[3] );');
maple('sdy := rhs( sol[4] );');

maple('sx')
maple('sy')
maple('sdx')
maple('sdy')

maple('numx := numer( sx );')
maple('numy := numer( sy );')
maple('numdx := numer( sdx );')
maple('numdy := numer( sdy );')

maple('denx := denom( sx );')
maple('deny := denom( sy );')
maple('dendx := denom( sdx );')
maple('dendy := denom( sdy );')

maple('simplify(dendx / denx );')
maple('simplify(dendy / denx );')
maple('simplify(deny / denx );')
%maple('simplify(sdx);')
%maple('simplify(sdy);')


return



%with angle as a parameter:

eq1a = 'tn = t + mom';
moma = 'cos(tn) * ( a - Ax*yn + Ay*xn ) - sin(tn) * ( b - Ax*xn - Ay*yn )';
eq1a = strrep( eq1a, 'mom', moma );

eq3a = 'xn - x = Gx - cos(tn) * Ax + sin(tn) * Ay - K * xn ';
eq4a = 'yn - y = Gy - sin(tn) * Ax - cos(tn) * Ay - K * yn ';

disp( sprintf('eq1a : %s',eq1a) );
disp( sprintf('eq3a : %s',eq3a) );
disp( sprintf('eq4a : %s',eq4a) );

cmd = sprintf('sol := solve({%s,%s,%s},{xn,yn,tn})',eq1a, eq3a, eq4a);


