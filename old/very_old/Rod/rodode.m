function varargout = rodode(t,y,flag,p)

switch flag
 case ''                                 % Return dy/dt = f(t,y).
   varargout{1} = f(t,y,p);
 case 'init'                             % Return default [tspan,y0,options].
   [varargout{1:3}] = init(p);
 otherwise
   error(['Unknown flag ''' flag '''.']);
end

return


function dy = f(t,y,p)
K   = p(1);
alp = p(2);
bet = p(3);
Ax  = p(4);
Ay  = p(5);
Gx  = p(6);
Gy  = p(7);
et  = p(8);
mu  = p(9);

dy = zeros(4,1);

AsX = y(1)*Ax + y(2)*Ay;
AvX = y(2)*Ax - y(1)*Ay;

dy(1) =  mu * ( Gx - y(3)*Ax + y(4)*Ay - K*y(1) );
dy(2) =  mu * ( Gy - y(4)*Ax - y(3)*Ay - K*y(2) );
%dy(3) =  et * ( cos(y(3)) * ( alp - AvX ) - sin(y(3)) * ( bet - AsX ) );
mom   =  y(3) * ( alp - AvX ) - y(4) * ( bet - AsX ) ;
dy(3) = -et * mom * y(4);
dy(4) =  et * mom * y(3);
 
function [tspan, y0, options] = init(a,b,d)
%initial condition for solving:
tspan=[];
y0 = [0];
options=odeset('OutPutFcn','OdePlot');
