function mov = rodsolve3

%euler with constrained dynamics:

dt       = 1e-4;
km       = 100;
krigid   = 10/km;
visc     = 0.1;
nbsteps  = 10;

%mobility of the rods, of length L
L     = 2;
hydro = log( 2 / 0.025 );
mu    =   hydro / ( 4*pi*visc*L );
et    = 3*hydro / ( pi*visc*L^3 );


%definition of the springs:   % stiffness, rod1, ab1, rod2, ab2
sp(1,1:5) = [ 5 1  1 2 -1 ];  
%sp(2,1:5) = [ 5 2  1 3 -1 ];  
%sp(3,1:5) = [ 5 3  1 4 -1 ]; 
%sp(4,1:5) = [ 5 4  1 5 -1 ]; 
%sp(4,1:5) = [ km 1 -1 2  0 ]; 

%initial position of the rods:
%[ x y dx dy ] : position, direction
d=sqrt(2)/2;
a=0;
d=1;
initc(1,1:4) = [ -4*d   0  a  d ]; 
initc(2,1:4) = [ -2*d   0  a -d ];            
%initc(3,1:4) = [  0*d   0  a  d ]; 
%initc(4,1:4) = [  2*d   0  a -d ]; 
%initc(5,1:4) = [  4*d   0  a  d ]; 


%computing the matrix:
nbrod = size(initc,1);

N = zeros(nbrod);
A = zeros(nbrod);
C = zeros(nbrod);

for i=1:size(sp,1)
   
   k  = sp(i,1);
   r1 = sp(i,2);
   a1 = sp(i,3);
   r2 = sp(i,4);
   a2 = sp(i,5);
   
   N(r1,r2) = N(r1,r2) + k;
   N(r2,r1) = N(r2,r1) + k;
   N(r1,r1) = N(r1,r1) - k;
   N(r2,r2) = N(r2,r2) - k;
   
   A(r1,r1) = A(r1,r1) - k * a1;
   A(r2,r2) = A(r2,r2) - k * a2;
   A(r2,r1) = A(r2,r1) + k * a1;
   A(r1,r2) = A(r1,r2) + k * a2;

   C(r1,r2) = C(r1,r2) + k * a1*a2;
   C(r2,r1) = C(r2,r1) + k * a1*a2;
   
end

%links rigidity:
for i=1:nbrod-1
   C(i,i+1) = C(i,i+1) + krigid;
   C(i+1,i) = C(i+1,i) + krigid;
end



%solving the motion:
Z = zeros(nbrod);

NN = [ N, Z; Z, N ];
AA = [ A, Z; Z, A ];
CC = [ C, Z; Z, C ];

%linear approximation for the forces:
E  =  [ NN,  AA; AA', CC ];

%building the mobility matrix in the simple coordinate system:
diagmob  = km * mu * dt * diag( [ 2*ones(1, nbrod), ones(1, nbrod) ] );
diagmobt = km * et * dt * diag( [ zeros(1,nbrod), ones(1,nbrod) ] );

%building the initial vector of all rod's coordinates:
for r=1:nbrod
   x(r,1)         = initc(r, 1);
   x(r+nbrod,1)   = initc(r, 2);
   x(r+2*nbrod,1) = initc(r, 3);
   x(r+3*nbrod,1) = initc(r, 4);
end


for i=1:nbsteps
   
   if ( i < 10 )
      figure('Position',[ 300, 200, 500, 250],'menubar', 'none');
   else
      clf;
   end
   set(gca,'Position',[0 0 1 1]);
   drawrod2( x, sp, 'b:');

   %coordinate switching matrix: from normal to rod's directions
   D = [ diag(x(1+2*nbrod:3*nbrod)), -diag(x(1+3*nbrod:4*nbrod)); ...
         diag(x(1+3*nbrod:4*nbrod)),  diag(x(1+2*nbrod:3*nbrod)) ];
   
   %mobility matrix:
   M  = [ D * diagmob * D' , zeros(2*nbrod); zeros(2*nbrod), D * diagmobt * D' ];

   %building the jacobian for constraints:
   J = zeros(nbrod, 4*nbrod);
   for r=1:nbrod
      J(r, r+2*nbrod) = x(r+2*nbrod, 1);
      J(r, r+3*nbrod) = x(r+3*nbrod, 1);
   end
   J(nbrod+1, 1) = x(1,1)-x(2,1);
   J(nbrod+1, 2) = x(2,1)-x(1,1);
   
   
   %free forces vector:
   F = M * E * x;
      
   %finding the lagrange multipliers:
   l = cgs( J * M * J', J * M * F );
   
   
   x = x + F - J' * l;
      
   drawrod2( x, sp, 'b-');
   axis( [ -5 5 -2.5 2.5 ] );
   pause(0.5);
   mov(i)=getframe;

end

movie(mov,10);


return
