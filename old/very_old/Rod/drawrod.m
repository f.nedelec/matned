function drawrod( rod, spg, spa );

%plot( [ rod(1), rod(1)+rod(3) ], [ rod(2), rod(2)+rod(4) ], 'b-', 'LineWidth', 3 );
plot( [ rod(1)-rod(3), rod(1)+rod(3) ], [ rod(2)-rod(4), rod(2)+rod(4) ], 'b:', 'LineWidth', 3 );
hold on;
plot( rod(1), rod(2), 'kx', 'LineWidth', 4 );

rot = [ rod(3), -rod(4); rod(4), rod(3) ];
tra = [ rod(1), rod(2) ]';

axis( [ -1.5 1.5 -1.5 1.5 ] );
if ( nargin == 1 )  return;  end

for i=1:size(spg,1)
   att = tra + rot * [ spa(i,1), spa(i,2) ]';
   plot( [ att(1), spg(i,1) ], [ att(2), spg(i,2) ], 'k-', 'LineWidth', 2);
end

