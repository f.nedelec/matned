%this plots the theory, and the simulations on the same graph
% "silentode" should have run with the right parameters before

figname=strcat('conc. of bound and free motors (',int2str(monb),')');
FigC(monb)=figure('Name',figname,'Position',[20+20*monb 250+20*monb 600 450]);
figure(FigC(monb));
xlabel('distance from center (microns)','FontSize',textsize);
ylabel('motor concentrations','FontSize',textsize);

%free motors:
errorbar(simres(:,1),simres(:,2)./UniC,simres(:,3)./(2*UniC),'b.');
hold on;
plot(simres(:,1),simres(:,2)./UniC,'ko','MarkerFaceColor','k', 'MarkerSize',6); 
plot(x,cf./UniC,'b-','LineWidth',1); 

%bound motors:
errorbar(simres(:,1),simres(:,4)./UniC,simres(:,5)./(2*UniC),'k.');
plot(simres(:,1),simres(:,4)./UniC,'k<','MarkerFaceColor','k', 'MarkerSize',7); 
plot(x,cb./UniC,'k--','LineWidth',1);

%bound+free motors:
plot(simres(:,1),(simres(:,2)+simres(:,4))./UniC,'bs','MarkerFaceColor','b', 'MarkerSize',7); 
plot(x,(cb+cf)./UniC,'k-','LineWidth',1);

%base line:
plot([0 L],[1 1],'k-');

%axis:
axis([0 L 0 vsize]);

%print:
if (mod(monb,2) == 1)
   print -deps2 expu.eps;
else
   print -deps2 conc.eps
end;
