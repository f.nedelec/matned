function ignore(param, data, indx)

Colors='rgbkm';
Shapes='+o*xs^v<>';

figure('Name',['ignore ' num2str(indx)]);

others = setdiff( [1:size(param,2)], indx );
tr     = param(:, others);
uniq   = unique( tr, 'rows' );

if ( length(others) == 2 )
   uniq1 = unique( uniq(:,1) );
   uniq2 = unique( uniq(:,2) );
end

for u=1:size(uniq,1)
   
   equal = zeros(size(tr,1),1);
   for i=1:size(tr, 1)
      equal(i) = all( tr(i,:) == uniq(u,:) );
   end
   sel = find( equal == 1 );
   
   if ( length(others)==2 )
      ci = find( uniq(u, 1) == uniq1 );
      cs = find( uniq(u, 2) == uniq2 );
   else
      ci = u;
      cs = u;
   end
   
   ci = 1 + mod(ci-1, length(Colors));
   cs = 1 + mod(cs-1, length(Shapes));
   linespec = ['-' Colors(ci) Shapes(cs)];  
   
   val='';
   for i=1:size(uniq,2)
      val = [val sprintf('%8.2f  ', uniq(u,i))];
   end
   
   avg = anplot( param(sel,:), data(sel,:), linespec );
   
   disp([linespec ' :' val  ' (' num2str(size(sel,1)) ' :' avg,')']);
   
   hold on;
   
end

return