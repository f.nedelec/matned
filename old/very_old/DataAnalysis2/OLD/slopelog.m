function [s, a] = slopelog(d, val, recut, debugflag);

%compute the loglog-slope:

if ( length(d) ~= length(val) )
   size(d)
   size(val)
   error('wrong size');
end

%we recut if necessary:
if ((exist('recut','var')==1) & (recut==1))
   m  = 1; %threshold(d, 1);         % one micron...
   m  = max( [m, 1] );
   MM = min( find( val < 0.01) );
   MM = min( [MM, length(val)] );
   %disp(strcat('m=',num2str(m),'M=',num2str(MM)));
   d   = d(m:MM);
   val = val(m:MM)
end

ld = log( d );
lv = log( val );

p = polyfit(ld, lv, 1);
s = p(1);
a = p(2);

if (exist('debugflag','var') == 1)
   figure('Name','debug logslope');
   plot(ld,lv,'b-');
   hold on;
   plot(ld, polyval(p,ld),'k-' );
end