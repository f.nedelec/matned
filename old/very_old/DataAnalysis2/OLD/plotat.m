function plotat()

load a;
load t;

figure('Position',[20 530 550 450],'MenuBar','None');
set(gca,'Position',[0.05 0.05 0.9 0.9]);

x=size(a,1);
plot(a(2:x,1), a(2:x,2:4), 'LineWidth',1);
hold on;
plot(a(2:x,1), a(2:x,2:4), 'o','LineWidth',2);
plot(t(:,1), t(:,2:4), '-.', 'LineWidth',3);

print -deps fig1.eps;

figure('Position',[650 530 550 450],'MenuBar','None');
set(gca,'Position',[0.05 0.05 0.9 0.9]);

hold on;
plot(a(2:x,1), a(2:x,5), 'k-', 'LineWidth',1);
plot(a(2:x,1), a(2:x,5), 'k.', 'LineWidth',3);
plot(t(:,1), t(:,5), 'k-.', 'LineWidth',2);

plot(a(2:x,1), a(2:x,6), 'g-', 'LineWidth',3);
plot(a(2:x,1), a(2:x,7), 'b-', 'LineWidth',3);
plot(a(2:x,1), a(2:x,8), 'k-', 'LineWidth',3);



return