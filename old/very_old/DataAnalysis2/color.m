function color( data, indx )

if ( size( indx, 2 ) ~= 3 ) return; end

axisX = indx(1);
axisY = indx(2);
color = indx(3);

colorcodes = 'rgbmyk';
uniq = unique( data(:,color) );

for u=1:100 %length(uniq)
   
   if ( 1 == mod( u, 50 ) )
      drawnow;
      figure('Name',['nb: ' num2str(u) ],...
         'Position',[20 40 1250 950], 'MenuBar', 'none');
      set(gca, 'Position', [0.05 0.05 0.9 0.9]);
      axis([ 100 500 -30 30]);
   end

   sel = find( data(:,color) == uniq(u) );
   
   ci = 1 + mod( floor((u-1)/4), length(colorcodes));   
   si = 1 + mod( u-1, 4);
   switch si
   case 1
      linespec = [colorcodes(ci) '-']; 
   case 2
      linespec = [colorcodes(ci) '--']; 
   case 3
      linespec = [colorcodes(ci) ':']; 
   case 4
      linespec = [colorcodes(ci) '.-']; 
   end

   val = sprintf('%8.2f  ', uniq(u));
   %disp([val ' : ' linespec]);
   dselx = data( sel, axisX );
   dsely = data( sel, axisY );
   sel
   
   plot( dselx, dsely , linespec,'LineWidth', 1, 'MarkerSize', 1 );
   hold on;
   [maxy imax] = max( dsely );
   [miny imin] = min( dsely );
   if ( -miny > maxy ) maxy=miny; imax=imin; end
   text( dselx(imax), maxy , num2str(uniq(u)), 'FontSize', 8);
   
end
