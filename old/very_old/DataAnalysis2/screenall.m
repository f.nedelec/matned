function seld = screenall( numbers )

if nargin < 1 
   numbers = [ 6, 7, 14:19, 24:29 ];
   numbers = [ 24:29 ];
end

seld = zeros( 0, 17 );

for i = 1:length( numbers )
   
   nb = numbers( i );
   
   %load and screen the file
   sname = sprintf('c%02i', nb );
   disp( [ 'loading   ' sname ] );
   as = load(sname);
   s = screen2( as, [ 2 3 1 ] );
      
   sname = sprintf('t%02i', nb );
   disp( [ 'loading   ' sname ] );
   ap = load(sname);
   nbpam = size( ap, 2 );
   
   %find the corresponding parameters
   p = zeros( size( s, 1 ), 16 );
   for j = 1 : size( s, 1 )
      pline = find( ap(:,1) == s( j, 1 ) );
      if ( size( pline ) == [ 1, 1 ] )
         p( j, 1:nbpam ) = ap( pline, : );
         p( j, 16 ) = nb;
         p( j, 17 ) = s( j, 2 );
      else
         disp(['problem with ' num2str(s(j)) ' in ' num2str(nb)]);
      end
   end
   
   %concatenate to make the big list:
   seld = cat( 1, seld, p );
   
end
