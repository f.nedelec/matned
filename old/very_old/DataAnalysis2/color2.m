function color( data, indx )

if ( size( indx, 2 ) ~= 3 ) return; end

axisX = indx(1);
axisY = indx(2);
color = indx(3);

colorcodes = 'krbg';
linewidths = [ 2; 2; 2; 2; 3 ];
cnt=1;
curves=100;
dstart=1;
oldf=0;

for u=1:size(data, 1)
   
   if ( data(u, color) ~= cnt )
      dend = u-1;
      if ( dend > dstart ) 
         dx = data( dstart:dend, axisX );
         dy = data( dstart:dend, axisY );
      else
         disp(['problem with  ' num2str( cnt )]);
      end
      dstart = u;
      
      if ( curves > 9 )
         curves = 0;
         if ( cnt == 1 ) 
            figure('Name',['nb: ' num2str(cnt) ],...
               'Position',[20 300 1250 700], 'MenuBar', 'none');
         end
         if ( cnt > 1 )
            drawnow;
            wait = input( [ num2str( cnt ) ' ( press return )'], 's' );
            clf;
         end
         set(gca, 'Position', [0.05 0.05 0.9 0.9]);
         plot( [ 0 1000 ], [ 0 0 ] ,'k:');
         axis([ 0 1000 -2.5 2.5]);
         set(gca, 'YTick',[]);
         %set(gca,'YTick',[-2;-1;0;1;2]);
         %set(gca,'YTickLabel',[-2;-1;0;1;2]);
         hold on;
         plot( [ 0 1000 ], [ -2 -2 ] ,'k:');
         plot( [ 0 1000 ], [ -1 -1 ] ,'k:');
         plot( [ 0 1000 ], [  1  1 ] ,'k:');
         plot( [ 0 1000 ], [  2  2 ] ,'k:');
      end
      
      curves = curves + 1;
      
      ci = 1 + mod( curves, length(colorcodes));   
      lw = linewidths( ci );
      switch mod( floor(curves/4), 3)
      case 0
         linespec = [colorcodes(ci) '-']; 
      case 1
         linespec = [colorcodes(ci) '--']; 
      case 2
         linespec = [colorcodes(ci) ':']; 
      end
      
      disp([ num2str(cnt) ' : ' linespec]);
      plot( dx, dy , linespec, 'LineWidth', lw, 'MarkerSize', lw );
            
      indx = [1 size(dx,1) 0 0];
      [val indx(3)] = max( dy );
      [val indx(4)] = min( dy );
      for i=1:4
         text( 5+dx(indx(i)), dy(indx(i)) , num2str( cnt ), 'FontSize', 8);
      end
 
      cnt = data( u, color );
   end
      
   
end
