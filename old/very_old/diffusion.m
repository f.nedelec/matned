%macro to test hypothesis on diffusion related to an article to review

function ncc = all;

length=5000;
figure;
axis([0 10 0 3*length]);
hold on;
ncc = zeros(9,1);
rep=500;

for ll = 1:rep
    for kk = 1:8
        step = exp(-kk);
        nbsteps = length / step;
        [nc, x] = diffusion( nbsteps, step );
        plot( kk, nc, 'x' );
        ncc(kk) = ncc(kk) + nc;
    end
    drawnow;
end
    
ncc = ncc ./ rep;
plot( ncc, 'bl');
ncc = ncc ./ length;

return

function [nbcross, x] = diffusion( N, step )

nbcross = 0;
x = rand;
ii = fix(x);

%plot(1, x);
%hold on;

for s=1:N
    
    if ( rand > 0.5 )
        x = x + step;
    else
        x = x - step;
    end
    
    %plot(s, x);
    
    jj = fix(x);
    
    if ( jj ~= ii )
        ii = jj;
        nbcross = nbcross + 1;
    end
    
end
        