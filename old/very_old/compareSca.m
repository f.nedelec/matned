%this plots the theory, and the simulations on the same graph
% "silentode" should have run with the right parameters before

figname=strcat('Thery vs. Scaling');
FigScaling=figure('Name',figname,'Position',[20 250 600 450]);
figure(FigScaling);
xlabel('distance from center (microns)','FontSize',textsize);
ylabel('motor concentrations','FontSize',textsize);
hold on;

%free motors:
plot(Sx, Scf./UniC,'b--','LineWidth',3); 
plot(x, cf./UniC,'b-','LineWidth',2); 

%bound motors:
plot(Sx, Scb./UniC,'r--','LineWidth',3);
plot(x, cb./UniC,'r-','LineWidth',2);

%bound+free motors:
plot(Sx, (Scb+Scf)./UniC,'k--','LineWidth',3);
plot(x, (cb+cf)./UniC,'k-','LineWidth',2);

%base line:
plot([0 L],[1 1],'k-');

%axis:
axis([0 L 0 vsize]);




%total Localization:
figname=strcat('Thery vs. Scaling (Localization)');
FigScaling2=figure('Name',figname,'Position',[640 250 600 450]);
figure(FigScaling2);
%set(gca,'Box','on','XTick',[ ],'YTick', [ ]);
xlabel('distance from center (microns)','FontSize',textsize);
ylabel('total motor localization','FontSize',textsize);
hold on;

plot(Sx, Sx .* (Scb+Scf)./UniC,'k--','LineWidth', 3);
plot(x, x .* (cb+cf)./UniC,'k-','LineWidth', 2);
%base line (Uniform conc.):
plot([0 L],[1 1],'k-');

%axis:
axis([0 L 0 L*estimate]);

%print:
