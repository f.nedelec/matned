function showall;

global selcnt;
selcnt = 0;

screen = load('screen');
params = load('tel');

for u=1:size(screen, 1)
      
      nbr  = screen(u, 1);
      name = sprintf('data%04i', nbr );
      
      distmin    = screen( u, 2 );
      distmean   = screen( u, 3 );
      distmax    = screen( u, 4 );
      minlinks   = screen( u, 5 );
      meanlinks  = screen( u, 6 );
      intralinks = screen( u, 7 );
      paralinks  = screen( u, 8 );
      antilinks  = screen( u, 9 );
      
      pline = find( params(:,1) == nbr );
      if ( size( pline ) == [ 1, 1 ] )
          speed1 = params( pline, 9  );
          speed2 = params( pline, 10 );
          pend1 = params( pline, 15);
          pend2 = params( pline, 16);
          
          speeds = sprintf(' speeds = %.2f %.2f ', speed1, speed2 );
      else
          disp(['cannot find ' name ' in <tel>' ]);
      end
      
      if ( minlinks > 5 ) & ( distmin > 1 ) & ( speed1 + speed2 < -0.2 ) & ( pend2 > 20 ) & ( distmean > 10 )
         selcnt = selcnt + 1;
      
         %npwd = strrep( pwd, '\', '/' );
         %webaddress = sprintf( 'file:///%s/%s/last.gif', npwd, name);
      
         %=================display the positions of the asters:
         %screenplot( asd(:,1 ) , dist / 7, u );
         plotasterpos1D( name );
         
         %web( webaddress, '-browser' );
         
         %=================find the parameters:
         
         disp( [ name, speeds, ', pend1 = ' num2str( pend1 ), ', pend2 = ' num2str( pend2 ) ] );
         
         pause(2);
      end
end
         
return;