function res = analyse2( seld, alld )

nbbin = 5;
nbpam = size(alld, 2);
nbsel = size(seld, 1);
nbtot = size(alld, 1);
res = zeros( nbpam, 40);

for p=nbpam:-1:1
   
   mn = min( alld(:,p) );
   mx = max( alld(:,p) );
   
   %[mn mx]
   if ( mx > mn )
      
      interval = mn:(mx-mn)/nbbin:mx;
      res(p, 1+[0:nbbin]) = interval; 
      interval(1)       = -inf;
      interval(1+nbbin) =  inf;
      %interval
      %size( histc( seld(:, p), interval ) )
      res(p, 20+[1:nbbin+1]) = nbbin * histc( alld(:,p), interval )' ./ nbtot;
      res(p, 30+[1:nbbin+1]) = nbbin * histc( seld(:,p), interval )' ./ nbsel;
      res(p, 40+[1:nbbin]) = res(p, 30+[1:nbbin]) ./ res(p, 20+[1:nbbin]);
      
      figure( 'Name', num2str(p), 'Position', [10 300 600 650] );
      interval = ( res(p, [1:nbbin] ) + res(p, 1+[1:nbbin] ) )/ 2;
      bar( interval, res(p, 40+[1:nbbin] ),'y' );
      hold on;
      xb = [ res(p, [1:nbbin] ); res(p, 1+[1:nbbin] )];
      yb = [ res(p, 20+[1:nbbin] ); res(p, 20+[1:nbbin] )];
      plot( xb, yb, 'k', 'LineWidth', 3);
      
      xb = [ res(p, [1:nbbin] ); res(p, 1+[1:nbbin] )];
      yb = [ res(p, 30+[1:nbbin] ); res(p, 30+[1:nbbin] )];
      plot( xb, yb, 'b', 'LineWidth', 3);
      
      
      plot( [mn, mx], [1, 1] , 'k:', 'LineWidth', 2);
      axis( [ mn mx 0 5] );
      
   end
end