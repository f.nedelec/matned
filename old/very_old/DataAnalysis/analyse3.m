function res = analyse3( seld, alld )

nbpam = size(alld, 2);

for p=1:nbpam
for q=p+1:nbpam
   
   analysecross( seld, alld, p, q );

end
end