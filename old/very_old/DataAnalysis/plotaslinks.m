function plotasl;


figure( 'Name', 'para vs antipara links' );


for u=1:14
   
   name = sprintf('st%02i', u);
   
   disp( name );
   
   cd( name );
   
   load lin;
   plot( lin(:,4), lin(:,5), '.' );
   hold on;
   
   cd ..;
   
end