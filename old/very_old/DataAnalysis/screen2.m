function sel = screen2( data, indx )

if ( nargin < 2 ) | size( indx, 2 ) ~= 3 
    indx=[ 2 3 1 ]; 
end


global plotcnt;
plotcnt = 0;

sel    = zeros( 0, 1 );
selcnt = 0;

axisX = indx(1);
axisY = indx(2);
color = indx(3);

name   = 0;
dstart = 1;

for u=1:size(data, 1)
   
   if ( data(u, color) ~= name ) 
      dend = u-1;
      
      if ( dend > dstart )
         dx = data( dstart:dend, axisX );
         dy = data( dstart:dend, axisY );
         dstart = u;
         
         ldx = size( dx, 1 );
         hldx = floor( 2 * ldx / 3 );
         dym = dy( hldx:ldx );
         
         dymmax = max( dym );
         dymmin = min( dym );
         
         if ( dymmax < 0 )
            tmp = dymmax;
            dymmax = -dymmin;
            dymmin = -tmp;
         end
         
         test1 = ( dymmax < 1.97 );
         test2 = ( dymmin > 1.03 );
         test3 = ( dymmax - dymmin ) < 0.1;
         
         %if ( test1 & test2 & test3 )
            screenplot( dx, dy, name );
            selcnt = selcnt + 1;
            sel( selcnt, 1 ) = name;
            sel( selcnt, 2 ) = 2 - ( dymmax + dymmin )/2;
            %end
      end      
      
      name = data( u, color );
   end
end

return;