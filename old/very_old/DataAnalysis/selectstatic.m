function sel = select;

selcnt = 0;

%load the files:
screen = load('screen');
params = load('tel');

%open output file:
fid   = fopen( 'sel', 'w' );
fprintf( fid, '%%%s', date );

for u = 1:size(screen,1)

   name       = screen( u, 1 );
   distmin    = screen( u, 2 );
   distmean   = screen( u, 3 );
   distmax    = screen( u, 4 );
   minlinks   = screen( u, 5 );
   meanlinks  = screen( u, 6 );
   intralinks = screen( u, 7 );
   paralinks  = screen( u, 8 );
   antilinks  = screen( u, 9 );
   
   overlap = 15 * 2 - distmean;
   
   test    = zeros( 1, 5 );
   test(1) = ( distmax < 15 * 1.9 );
   test(2) = ( distmin > 15 * 1.1 );
   test(3) = ( distmax - distmin ) < 15 * 0.4;
   test(4) = ( meanlinks > 50 );
   test(5) = ( antilinks > paralinks );

   alltest = all( test );
   
   %save the selection:
   s = sprintf( '%04i %i %i %i %i %i %i',...
      name, alltest, test(1), test(2), test(3), test(4), test(5));

   disp(s);
   fprintf( fid, '%s\n', s );
   
   if ( alltest )
      %screenplot( dx, dy, name );
      %web( [ 'file:///' pwd '/last.gif' ] , '-browser' );

      
      %==================find the corresponding parameters:
      selcnt = selcnt + 1;
      
      pline = find( params(:,1) == name );
      if ( size( pline ) == [ 1, 1 ] )
         sel( selcnt, 1:size(params, 2) ) = params( pline, : );
      else
         disp(['cannot find ' name ' in <tel>' ]);
      end
      
      sel( selcnt, size( params, 2) + 1 ) = overlap;        %the overlap
  end
   
end

fclose(fid);
         
return;