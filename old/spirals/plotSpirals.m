function aH = plotSpirals(aH)

cwd  = pwd;
runs = dir('run*');

nrun = size(runs, 1);
res  = zeros(nrun,2);
pam  = zeros(nrun,2);

for u = 1 : nrun
    
    if runs(u).isdir
        cd(runs(u).name);
        P = getParameters;
        fprintf( 'dt = %5.3f rl=%4.2f  ', P.dt, P.mtrodlength);
        pam(u,1) = P.dt;
        pam(u,2) = P.mtrodlength;
        [ res(u,1), res(u,2) ] = spiral(4);
    end
    cd(cwd);

end

if ( nargin < 1 )
    figure('Name', cwd);
    fontSize = 14;
    
    aH(1) = axes('Position', [0.15 0.15 0.8 0.35]);
    set(gca, 'xTickLabel', 1000*pam(:,1), 'FontSize', fontSize);
    xlabel('time step (ms)', 'FontSize', fontSize+4);
    ylabel('radius (\mu m)', 'FontSize', fontSize+4);
    axis([1 10 0.7 1.7]);
    hold on;
    
    aH(2) = axes('Position', [0.15 0.57 0.8 0.35]);
    set(gca, 'xTickLabel', '', 'FontSize', fontSize);
    ylabel('speed (turn/s)', 'FontSize', fontSize+4);
    axis([1 10 0.04 0.07]);
    hold on;
end

axes(aH(1));
plot(res(:,1), '-dk', 'LineWidth', 1, 'MarkerSize', 7, 'MarkerFaceColor', 'k');
%title('radius (um)');

axes(aH(2));
plot(abs(res(:,2)), '-dk', 'LineWidth', 1, 'MarkerSize', 7, 'MarkerFaceColor', 'k');



