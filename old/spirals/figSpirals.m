function figSpirals

cwd  = pwd;
dirs = dir('screen*');

ndir = size(dirs, 1);

for u = 1 : ndir-2
    
    if dirs(u).isdir
        cd(dirs(u).name);
        if ~ exist('aH', 'var')
            aH = plotSpirals;
        else
            plotSpirals(aH);
        end
    end
    cd(cwd);

end
