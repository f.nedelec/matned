function sel = selectfig( crit );

% sel = selectfig( crit ) where the criteria is one of:
% 'all', 'cont', 'fuse', 'good', 'bad'
%
%


linkbase = 1;
distbase = 2;

sel=[];
selcnt = 0;

%----------------  load the different files:

screen = load('screen');

if ( exist( 'forces' ) == 2 )
    forces = load('forces');
end

if ( exist( 'tel' ) == 2 )
    params = load('tel');
end
    
    

%----------------  open output file:
fid   = fopen( 'sel', 'w' );
fprintf( fid, '%%%s selectfig %s\n', date, crit );

%----------------  screen:
for u=1:size(screen, 1)
      
    nbr  = screen(u, 1);
    name = sprintf('data%04i', nbr );
      
    distmin    = screen( u, 2 );
    distmean   = screen( u, 3 );
    distmax    = screen( u, 4 );
    distdev    = screen( u, 5 );
      
    linkmin    = screen( u, 6 );
    linkmean   = screen( u, 7 );
    linkintra  = screen( u, 8 );
    linkpara   = screen( u, 9 );
    linkanti   = screen( u, 10 );
        
    f=[];
    if 1 == exist( 'forces', 'var' )
        line = find( forces(:,1) == nbr );
        if size( line ) == [ 1, 1 ]
            f = forces( line, 2:9 )';
        end
    end
    
    km=[];
    specials=[];
    speeds=[];
    pends=[];
    if 1 == exist( 'params', 'var' )
        line = find( params(:,1) == nbr );
        if size( line ) == [ 1, 1 ] 
            p = params( line, : );
        end
        psize = size( params, 2 );          
        km = params( line, 2 );
        specials = [ params( line, psize-11 ); params( line, psize-10 ) ];
        speeds = [ params( line, psize-7 ); params( line, psize-6 ) ];
        pends  = [ params( line, psize-1 ); params( line, psize ) ];
    end
    
    
    S = 0;
    switch crit
    case 'all'
        S = 1;
        
    case 'cont'
        S = ( linkmin > linkbase );
        
    case 'fuse'
        S = ( distmin < distbase );
        
    case 'fusebis'
        S = ( linkmin > linkbase ) & ( distmin < distbase ) & ( distmean > distbase );
        
    case 'good'
        S = ( linkmin > linkbase ) & ( distmin > distbase );
        
    case 'bad'
        S = ( linkmin < linkbase ) | ( distmin < distbase );
        
    case 'oscl'
        S = ( linkmin > linkbase ) & ( distmin < distbase ) & ( distmax > distbase );
        
    case 'few'
        S = ~( ( linkmin > linkbase ) & ( distmin > distbase ) );
            
    case 'S1'
        S = ( linkmin > linkbase ) & ( distmin > distbase )...
            & ( f(1) < -1 ) & ( f(3) > 1 )...             %i.e. anti-parallel motors are pulling
            & ( f(8) - f(6) < 0.1 * ( f(7) - f(5) ) );    %i.e. not too many Ts
        
    case 'S2'
        S = ( linkmin > linkbase ) & ( distmin > distbase )...
            & ( f(1) > 1 ) & ( f(2) < -1 )...             %i.e. anti-parallel motors are pushing
            & ( f(8) - f(6) > 0.1 * ( f(7) - f(5) ) );    %i.e. the sum of X-forces does not cancel well.
        
    case 'S3'
        S = ( linkmin > linkbase ) & ( distmin > distbase )...
            & ( f(1) < -1 ) & ( f(2) > 1 )...             %i.e. anti-parallel motors are pushing
            & ( f(8) - f(6) > 0.1 * ( f(7) - f(5) ) );    %i.e. the sum of X-forces does not cancel well.
        
    case 'S4'
        S = ( linkmin > linkbase ) & ( distmin > distbase )...
            & ( f(1) < -1 ) & ( f(4) > 1 )...             %i.e. anti-parallel motors are pulling
            & ( f(8) - f(6) > 0.5 * ( f(7) - f(5) ) );    %i.e. the sum of X-forces does not cancel well.
        
    case 'slow'
        S = ( linkmin > linkbase ) & ( distmin > distbase )...
            & ( speeds(1) < 0.16 ); % & ( speed1 + speed2 > 0 )
        
    case 'S2bis'
        S = ( linkmin > linkbase ) & ( distmin > distbase )...
            & ( f(1) > 2 ) & ( f(2) < -2 )...             %i.e. anti-parallel motors are pushing
            & ( f(8) - f(6) > 0.1 * ( f(7) - f(5) ) )...  %i.e. the sum of X-forces does not cancel well.
            & ( speeds(1) + speeds(2) > -0.1 );
        
    case 'S5'
        if ( size( speeds, 1 ) > 1 ) & ( size(f,1) > 0 )
            S = ( linkmin > linkbase ) & ( distmin > distbase )...
                & ( f(1) > 5 ) & ( f(2) < -5 )...             %i.e. anti-parallel motors are pushing
                & ( f(8) - f(6) > 0.1 * ( f(7) - f(5) ) )...
                & ( speeds(1) + speeds(2) < -0.1 );
        else
            S = 0;
        end

    end               
    
    if ( S )
        fprintf( fid, '%04i\n', nbr );
        %display( sprintf( '%04i', nbr ) );
        selcnt = selcnt + 1;
        sel( selcnt, 1 ) = nbr;
    end
 
end
         
fclose( fid );

return;