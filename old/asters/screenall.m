function screenall;

files=dir('CENTL*');

for u=1:size(files, 1)
   
   name = files(u).name;
   
   disp( name );
   filename = [ name, '/screen' ];
   
   if exist( name, 'dir' ) & ( exist( filename, 'file' ) == 0 )
       
       cd( name );
       screenit;
       cd ..;
   
   end
       
end