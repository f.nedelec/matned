function calc_dist;
%writes a file 'cencen' containing several caracteristics of each simulation

startdir = pwd;

%open output file:
fid   = fopen( 'cencen', 'w' );

fprintf( fid, '%%%s\n', date );
fprintf( fid, '%%file  dist-min   dist-mean  dist-max  dist-dev   link-min   link-mean  link-intra  link-para  link-anti\n');
disp('%%file  dist-min   dist-mean  dist-max  dist-dev   link-min   link-mean  link-intra  link-para  link-anti\n');


files=dir('data*');

for u=1:size(files, 1)
      
    name = files(u).name;
    nbr  = str2num(name(5:8));
    
    if  0 == exist( [name '/aspos.out'], 'file' ) 
        disp(['file ', name, '/aspos.out not found']);
        continue; 
    end
   
   
    %================== aster to aster distance
    distmin  = 0;
    distmean = 0;
    distmax  = 0;
    distdev  = 0;
    
    aspos  = load([name '/aspos.out']);
   
    nbl = size( aspos, 1 );
    
    if ( nbl < 10 )
        disp(['file ', name, '/aspos.out is too short']);
        continue; 
    end
    
    midd = floor( nbl / 2 );
    aspos = aspos( midd:nbl, : );
    
    asd = sqrt( sum( ( aspos(:, [2 3 4]) - aspos(:, [5 6 7]) ) .^ 2 , 2) );
    
    distmin  =  min( asd );
    distmean = mean( asd );
    distmax  =  max( asd );
    distdev  =  std( asd );
    
    %================== number of links
   
    linkmean  = 0;
    linkintra = 0;
    linkpara  = 0;
    linkanti  = 0;
    linkmin   = 0;
    
    if ( 1 == exist( 'lin', 'var' ) )
        
        pline = find( lin(:,1) == nbr );
        if ( size( pline ) ~= [ 1, 1 ] )
            disp([ 'screenit : cannot find <', num2str( name ), '> in links'] );
            continue;
        end  
        
        linkmean  = lin( pline, 2 );
        linkintra = lin( pline, 3 );
        linkpara  = lin( pline, 4 );
        linkanti  = lin( pline, 5 );
        linkmin   = lin( pline, 6 );
        
    end
    
    %================= find the correct nb of links in aspos.out if available: (after oct. 3. 2001)
    
    if ( size( aspos, 2 ) >= 13 )

        linkpara  = mean( aspos( :, 8 ) ) + mean( aspos( :, 11 ));
        linkanti  = mean( aspos( :, 9 ) ) + mean( aspos( :, 12 ));
        if ( size(aspos, 2 ) >= 13 )
            linkintra = mean( aspos( :, 10 ) ) + mean( aspos( :, 13 ) );
        end
        linkmean  = linkpara + linkanti;
        linkmin   =  min( aspos( :, 8 ) + aspos( :, 9 ) + aspos( :, 11 ) + aspos( :, 12 ) );
        
    end
    
    
    %=================print and save the selection:
    s = sprintf('%04i  %9.2f %9.2f %9.2f %9.2f    %9.2f %9.2f %9.2f %9.2f %9.2f', nbr,... 
        distmin, distmean, distmax, distdev, linkmin, linkmean, linkintra, linkpara, linkanti );
    fprintf( fid, '%s\n', s);      
    
    %=================display:
    disp( s );
    
end

fclose( fid );
         
return;