function plotasterpos( nbr );

mtlength=20;

name = sprintf('data%04i', nbr);

filename = [ name, '/aspos.out' ];
if exist(filename,'file') == 0
    return; 
end
aspos = load(filename);


figure( 2 );
set( gcf, 'Name', name );
clf;

middle_x = mean( aspos(:,2) ) / 2 + mean( aspos(:,5) ) / 2;
middle_y = mean( aspos(:,3) ) / 2 + mean( aspos(:,6) ) / 2;

set(gca, 'Position', [0.05 0.55 0.4 0.4]);
ax1 = gca;
title('Aster positions');
axis([-mtlength mtlength -mtlength mtlength]);
hold on;

plot(aspos(:,2) - middle_x, aspos(:,3) - middle_y, 'b.', 'MarkerSize', 1);
plot(aspos(:,5) - middle_x, aspos(:,6) - middle_y, 'k.', 'MarkerSize', 1);

s = size( aspos, 1 );
plot(aspos(s,2) - middle_x, aspos(s,3) - middle_y, 'bx', 'LineWidth', 2, 'MarkerSize', 8);
plot(aspos(s,5) - middle_x, aspos(s,6) - middle_y, 'kx', 'LineWidth', 2, 'MarkerSize', 8);


%===============================================================================

ax2 = axes('Position', [0.55 0.55 0.4 0.4]);
title('Aster-aster distance');
axis([0 1000 0 2*mtlength]);
hold on;

dx   = aspos(:, 2:4) - aspos(:, 5:7);
dist = sqrt( sum( dx .^ 2, 2 ) );

plot( aspos(:,1), dist(:), 'b-' );

%===============================================================================

if ( size(aspos, 2 ) == 13 ) 
    
    ax3 = axes('Position', [0.05 0.05 0.4 0.4]);
    title('links');
    axis([ 0 1000 0 200 ] );
    hold on;
    
    plot( aspos(:,1), aspos(:,8), 'k-');
    plot( aspos(:,1), aspos(:,9), 'b-');
    
end
    
%===============================================================================

if exist('forces', 'file')
    
    forces = load('forces');
    line = find( forces(:,1) == nbr );
    if ( ~isempty( line ) )
       
        forces = forces( line, 2:9 );
        ax4 = axes('Position', [0.55 0.05 0.4 0.4]);
        title('Forces');
        T = max( abs( forces ) );
        T = 10 * floor( T/10 ) + 10;
        axis( [-T T/2 -T/2 T] );
        hold on;       
        plot( [-T, T/2], [T, -T/2], 'k--');
        plot( [-T/2, T/2], [-T/2, T/2], 'k--');
        
        plot( forces(1), forces(3), 'ks', 'MarkerFaceColor', 'k'); text( forces(1), forces(3), '  X a/p' );
        plot( forces(2), forces(4), 'ko', 'MarkerFaceColor', 'k'); text( forces(2), forces(4), '  T a/p' );
        plot( forces(5), forces(7), 'bs', 'MarkerFaceColor', 'b'); text( forces(5), forces(7), '  X l/h' );
        plot( forces(6), forces(8), 'bo', 'MarkerFaceColor', 'b'); text( forces(6), forces(8), '  T l/h' );
        
    end
end


drawnow;

%===============================================================================
filename = [ name, '/last.gif' ];
if exist( filename, 'file' )
    
    npwd = strrep( pwd, '\', '/' );
    webaddress = sprintf( 'file:///%s/%s/last.gif', npwd, name);
    web( webaddress, '-browser' );

end
%wait = input( [ name ' ( press return )'], 's' );

