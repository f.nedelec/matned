function allsel = countAll( screen )

if nargin < 1 
    screen=3;
end

switch screen
case 0
    files = [ 60:62, 63:64,    43:45, 71:73, 50:52,    65:66, 79:80,  40:42, 74:77,    30:36 ];  % all screens
case 1
    files = [ 60:62, 63:64 ];               % screen 1 homo
case 2
    files = [ 43:45, 71:73, 50:52 ];        % screen 2 hetero
case 3
    files = [ 65:66, 79:80 ];               % screen 3 mixed
case 4
    files = [ 40:42, 74:77 ];               % screen 4 sticking hetero
case 5
    files = [ 30:36 ];                      % screen 5 sticking hetero
case 6
    files = [ 67:70, 78, 83 ];              % reruns and internal controls
case 7
    files = [ 8:9 ];                        % 1D homo
case 8
    files = [ 11:13, 20:22 ];               % 1D homo sticking
end

for u = files;
    
    folder = sprintf('CENTL%02i', u);
   
    for disk = 1:6
        if ( disk == 1 ) cd K:\nedelec; end
        if ( disk == 2 ) cd L:\nedelec; end
        if ( disk == 3 ) cd M:\nedelec; end
        if ( disk == 4 ) cd N:\nedelec; end
        if ( disk == 5 ) cd O:\nedelec; end
        if ( disk == 6 ) return; end
        if ( exist( folder, 'dir' ) == 7 )
            cd( folder );
            break;
        end
    end
   
   if ( exist('screen', 'file') ~= 2 )
       disp( [ 'missing file <screen> in <', folder, '>' ] );
   end
   
   sel = selectgood;
   
   disp( [ folder, ' : ', num2str( size(sel, 1 ) ) ] )
   
   if ( size( sel, 1 ) > 0 )
       
       asel = zeros( size(sel) + [ 0 1 ] );
       asel(:, 1 ) = u;
       asel(:, 2:1+size(sel,2) ) = sel;
       
       if ( 0 == exist( 'allsel', 'var' ) )
           allsel = asel;
       else
           allsel = cat( 1, allsel, asel );
       end
   
   end
   
   cd ..;
   
end

disp(['total = ' num2str( size( allsel, 1 ) ) ] );