function sel = screenoscl;

%open output file:
fid   = fopen( 'oscl', 'w' );
fprintf( fid, '%%%s\n', date );

for nbr=1:500
   
   name = sprintf('data%04i', nbr);
   if ( size( dir([name '/aspos.out']), 1 ) < 1 ) continue; end
   
   %================== aster to aster distance
   
   aspos  = load([name '/aspos.out']);
   
   tail = size( aspos, 1 );
   midd = floor( tail / 2 );
   aspos = aspos( midd:tail, : );
   
   asd = sqrt( sum( ( aspos(:, [2 3 4]) - aspos(:, [5 6 7]) ) .^ 2 , 2) );
   
   %================= compute the number of oscillations
   
   mdist = mean( asd );
   
   above = find( asd > 3 );
   below = find( asd < 2 );

   %figure( 3 ); clf;
   %plot( above, asd(above), 'k-');
   %hold on;
   %plot( below, asd(below), 'r-');
   
   inx = 0;
   
   nbo = 0;
   while( ~ isempty( inx ) )
       
       nex = find( above > inx );
       if ( isempty( nex ) ) break; end
       
       inx  = above( nex( 1 ) );
       %above = above( nex( 1 ) : length( above ) );
       
       %plot( inx, asd( inx ), 'ko' );
       
       nex  = find( below > inx );
       if ( isempty( nex ) ) break; end
       
       inx  = below( nex( 1 ) );
       %below = below( nex( 1 ) : length( below ) );
       
       %plot( inx, asd( inx ), 'bo' );

       nbo  = nbo + 1;
       
   end;
   
   %============================= output
             
   fprintf( fid, '%04i  %2i\n', nbr, nbo );
          
   disp( [ name , ' oscil. = ', num2str(nbo) ] );
       
end
         
fclose( fid );

return;