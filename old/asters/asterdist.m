function [tim, asd ] = asterdist( nbr )

filename = sprintf('data%04i/aspos.out', nbr);

if exist(filename, 'file') == 0
    return; 
end

aspos = load(filename);

tim  = aspos(:, 1);
dv   = aspos(:, 2:4) - aspos(:, 5:7);
asd  = sqrt( sum( dv .^ 2, 2 ) ) ./ 8;

return;