function data = analyseGohta;
% this if for vale21

ignore = [ cellstr('record'), cellstr('file'), cellstr('randseed'), cellstr('info') ];


parent_dir = pwd;
files  = gohta_dir();
nfiles = size( files, 1);

stored_dataNb = -100;

cnt  = 0;
data = [];

for u = 1 : 500
    
    filename = files(u).name;
    dataNb   = sscanf(filename, 'data%d');
    
    if ( 0 == mod( dataNb, 2 ) )
        %stored_params = getParameters( filename );
        stored_ank    = load_ank( filename );
        stored_dataNb = dataNb;
    else
        if ( stored_dataNb == dataNb - 1 )
            %fprintf(1, 'pair %i, %i:', stored_dataNb, dataNb);
            
            params = getParameters( filename );
            ank    = load_ank( filename );

            %dif = diffParameters( stored_params, params, 1 );
            %dif = setdiff( dif, ignore );
            %disp(dif);
            
            %store the data points:
            cnt = cnt + 1;
            data( cnt, 1 ) = stored_dataNb;
            data( cnt, 2 ) = stored_ank(2);
            data( cnt, 3 ) = ank(2);
            
            %report better / worse:
            if ( abs( ank(2) - stored_ank(2) ) > 0.5 )
                fprintf(1, '%i : %f  ', stored_dataNb, stored_ank(2));
                fprintf(1, '%i : %f\n', dataNb, ank(2));
            end
           
        end
        
    end
end

figure('Position', [610 10 600 600]);
plot( data(:,2), data(:,3), 'o', 'MarkerSize', 4);
axis([0 5 0 5]);
hold on;
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'quality of focussing with dynein and Ncd', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'quality of focussing with dynein only', 'FontSize', 14, 'FontWeight', 'bold' );


return;


figure('Position', [10 10 600 600]);
plot( data(:,3), data(:,2), 'o', 'MarkerSize', 4);
hold on;
%plot( fitx, fity, '-' );
plot([0; 9], [0; 9], 'k:');
title( 'Dynein detaches instantly upon reaching the end of microtubules', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'Focusing by dynein alone (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'Focusing by dynein and Ncd (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );




figure('Position', [610 10 600 600]);
plot( data(:,3), data(:,2)-data(:,3), 'o', 'MarkerSize', 4);
hold on;
plot([0; 9], [0; 0], 'k:');
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'quality of focussing with dynein only (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'improvement brought by ncd (negative=improvement)', 'FontSize', 14, 'FontWeight', 'bold' );


figure('Position', [610 410 600 600]);
plot( data(:,4), data(:,2), 'o', 'MarkerSize', 4);
hold on;
plot([0; 9], [0; 0], 'k:');
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'best focussing with dynein only (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'focussing with ncd (low=better)', 'FontSize', 14, 'FontWeight', 'bold' );
