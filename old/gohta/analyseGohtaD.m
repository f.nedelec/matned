function [data, avg] = analyseGohtaD(data)
% to measure the influence of Dynein's dwell time on focussing
% use with vale27.
% F. Nedelec, July 11, 2005.


if ( nargin < 1 )
    
    files  = gohta_dir;
    nfiles = size( files, 1);

    cnt  = 0;
    data = [];

    for u = 1:nfiles

        filename = files(u).name;
        data_nb  = sscanf(filename, 'data%d');
        params   = getParameters( filename );
        ank      = load_ank( filename );

        cnt = cnt + 1;
        data(cnt, 1) = params.haenddetachrate(3);
        data(cnt, 2) = ank(2);
    end
    
end


avg = unique( data(:,1) )';
%avg=[2; 4; 8; 12; 16; 20; 30; 40; 50];
avg=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 16 18 22 30 40 ];
%compute the average:
for ii=1:size(avg,2)
    
    if ( ii < 2 )
        left = 0;
    else
        left = (avg(1, ii-1)+avg(1, ii))/2;
    end
    if ( ii >= size(avg,2) )
        right = 100;
    else
        right = (avg(1, ii)+avg(1, ii+1))/2;
    end
    
    sel = find( (data(:,1)>left) & (data(:,1)<right) );
    sp  = data(sel, 2);
    avg(1, ii) = mean( data(sel, 1) );
    avg(2, ii) = mean( sp );
    avg(3, ii) = std( sp );

end

dtime = 1000 ./ data(:,1);

figure('Position', [610 10 600 400]);

plot( dtime, data(:,2), 'ko', 'MarkerSize', 3, 'MarkerFaceColor', 'k');
axis([1000/50 1100/2 0 6]);
hold on;

%plot( 1000 ./ avg(:,1), avg(:,2), 'kx', 'MarkerSize', 6 );
errorbar( 1000 ./ avg(1,:), avg(2,:), avg(3,:), 'bo-', 'MarkerFaceColor', 'w', 'MarkerSize', 8 );
plot( [0 1000/50], [5 5], ':' );

%title( 'Influence of dwell-time of motor', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'Dwell-time (ms)', 'FontSize', 21 );
ylabel( 'K-fibers distance (um)', 'FontSize', 21 );
set(gca, 'FontSize', 16);

return;
