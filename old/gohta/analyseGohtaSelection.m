function analyseGohtaSelection(selection, pool, ip);
% this is for the post-decissive screen
% July 19 in woods hole


if nargin < 2
    error('missing arguments');
end
if nargin < 3 
    ip = 2;
end

consider = [ cellstr('record'), cellstr('file'), cellstr('randseed'), ...
    cellstr('info'), cellstr('file'), cellstr('path') ];



%first column is the data iid, others are the parameter we want, RUN:
% tell asmtmax cxmax haforce hattachrate haattachdist hadetachrate
% haenddetachrate > tell

params = load('tell');

selflag = ismember(params(:,1), selection);
polflag = ismember(params(:,1), pool);

all = selflag | polflag;
values = unique(params(find(all),ip));

if ( size(values,1) < 10 )
    mode = 1;
else
    mode = 2;
end


if ( mode == 1 )
    for ii = 1 : size(values,1)

        flag   = ( params(:,ip) == values(ii) );
        counts_pol(ii)  =  sum( flag & polflag, 1 );
        counts_sel(ii)  =  sum( flag & selflag, 1 );
        ratio(ii) = counts_sel(ii) / counts_pol(ii);

        fprintf(1,'%f : %5i %5i : %f\n', values(ii), ...
            counts_pol(ii), counts_sel(ii), counts_sel(ii)/counts_pol(ii) );
    end
else
    
    med = median( values );
    
    for ii = 1 : 2
   
        if ( ii == 1 )
            flag  = ( params(:,ip) <  med );
            mes = 'below';
        else
            flag  = ( params(:,ip) >= med );
            mes = 'above';
        end
    
        counts_pol(ii)  =  sum( flag & polflag, 1 );
        counts_sel(ii)  =  sum( flag & selflag, 1 );
        ratio(ii)       =  counts_sel(ii) / counts_pol(ii);

        fprintf(1,'%s %f : %5i %5i : %f\n', mes, med, ...
            counts_pol(ii), counts_sel(ii), ratio(ii) );
    end
end

figure('Position', [100 300 300 200], 'Menu', 'none');
bar( ratio );
title( info(ip) );

if ( mode == 1 )
    set(gca, 'XTickLabel', values);
else
    set(gca, 'XTickLabel', ['Lower|Higher']);
end
ylabel('sucessful ratio');
    
    
function msg = info(field)
switch field
    case 1
        msg = 'indx';
    case 2  
        msg = 'asmtmax';
    case 3  
        msg = 'elasticity';
    case 4
        msg = 'nb of Ncd';
    case 5
        msg = 'nb of Dynein';
    case 6
        msg = 'force of Ncd';
    case 7
        msg = 'force of Eb1';
    case 8
        msg = 'force of Dynein';
    case 9
        msg = 'binding rate of Ncd';
    case 10
        msg = 'binding rate of Eb1';
    case 11
        msg = 'binding rate of Dynein';
    case 12
        msg = 'binding distance of Ncd';
    case 13
        msg = 'binding distance of Eb1';
    case 14
        msg = 'binding distance of Dynein';
    case 15
        msg = 'detach rate of Ncd';
    case 16
        msg = 'detach rate of Eb1';
    case 17
        msg = 'detach rate of Dynein';
    case 18
        msg = 'end detach rate of Ncd';
    case 19
        msg = 'end detach rate of Eb1';
    case 20
        msg = 'end detach rate of Dynein';
    case 21
        msg = '';

end
return;


