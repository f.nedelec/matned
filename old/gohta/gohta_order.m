function [ files, groups, group_cnt ] = gohta_order()

% a macro to group simulations from a folder into similar ones.
% F. Nedelec June 2005

exclude = [ cellstr('record'), cellstr('file'), cellstr('randseed'), cellstr('info') ];
accept  = cellstr('cxmax');
ignore  = [ exclude, accept ];



%the master data to compare with:
master    = [];
group_id  = 0;
new_group = 1;


%--------------------------------------------------------------------------
% Finding the groups by a scan

parent_dir = pwd;
files  = gohta_dir();
nfiles = size( files, 1);

for u = 1 : nfiles
   
    filename = files(u).name;
    
    params = getParameters( filename );
    
    if ~isempty( master )
        new_group = ~isempty( differences( master, params, ignore ) );
    end

    if ( new_group )
        master    = params;
        group_id  = group_id + 1;
        rep( group_id ) = u;
        new_group = 0;
    end

    files(u).group = group_id;
    fprintf(1, '%s : group %i\n', filename, group_id );
end


%--------------------------------------------------------------------------
% Merging similar groups

for g = 1:group_id

    if ( files(rep(g)).group < g )
        continue;
    end

    Pg = getParameters( files(rep(g)).name );

    for h = g+1:group_id
      
       if ( files(rep(h)).group <= g )
           continue;
       end
       
       Ph = getParameters( files(rep(h)).name );
       
       diff = differences( Ph, Pg, ignore );
       if ( size(diff, 2) > 1 )
           %fprintf(1, '%i vs %i: %i\n', g, h, size(diff,2) );
       else
           fprintf(1, '%i (%s) vs %i (%s):', g, Pg.file, h, Ph.file );
           disp( diff );
       end
       
       if ( isempty(diff) )
           fprintf(1, ' merging \n');
           new_id = files(rep( g )).group;
           for u = 1:nfiles
               if ( files(u).group == h )
                   files(u).group = new_id;
               end
           end
           
       end
       
   end
end

%--------------------------------------------------------------------------
% counting the groups, building a list of parameters

group_cnt = [ files(1).group, 1 ];
groups    = [ cellstr(files(1).name) ];

for u = 2:nfiles
    
    group = files(u).group;

    if ~isempty( group )
       
        indx = find( group_cnt(:,1) == group );
        if isempty( indx )
            group_cnt = cat(1, group_cnt, [group, 1] );
            indx      = size( group_cnt, 1 );
        else
            group_cnt(indx, 2) = group_cnt(indx, 2) + 1;
        end
        
        groups( indx, group_cnt(indx, 2) ) = cellstr( files(u).name );

    end
end



return;

%--------------------------------------------------------------------------
function diff = differences( P, Q, ignore )
dif   = diffParameters( P, Q, 1 );
diff  = setdiff( dif, ignore );
return;


