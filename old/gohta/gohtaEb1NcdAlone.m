function gohtaEb1NcdAlone()

files  = gohta_dir;
nfiles = size( files, 1);
ignore = [ cellstr('record'), cellstr('file'), cellstr('randseed'), ...
    cellstr('info'), cellstr('file'), cellstr('path') ];


figure('Position', [610 10 600 600]);

plot([0.5, 5], [0.5, 5], ':', 'LineWidth', 2);
axis([0.5 5.5 0.5 5.5]);
hold on;
title( 'Plot 2: average distance K-distance', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'quality of focussing with Ncd-Eb1 only', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'quality of focussing with dynein and Ncd-Eb1', 'FontSize', 14, 'FontWeight', 'bold' );

for u = 1:nfiles
    
    filename1 = files(u).name;
    filename2 = ['../magic3/', filename1];
    param1    = getParameters( filename1 );
    param2    = getParameters( filename2 );
    ank1      = load_ank( filename1 );
    ank2      = load_ank( filename2 );

    dif = diffParameters( param1, param2, 1, ignore );
    fprintf(1, 'diff for: %s', filename1);
    disp(dif);

    plot(ank1(2), ank2(2), 'o');
 
    drawnow;
end
