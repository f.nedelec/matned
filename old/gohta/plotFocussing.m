function plotFocussing( directory, make_figure );


if ~exist( 'make_figure', 'var' )
    make_figure = true;
end

if ~exist( 'directory', 'var')
    directory = pwd;
    dist = load('statistics.out');
else
    if isnumeric( directory )
        directory = sprintf('data%04i', directory);
    end
    if isdir( directory )
        dist = load([directory, '/statistics.out']);
    end
end

paramsToShow = [ cellstr('asmtmax'), cellstr('cxmax'), cellstr('haattachrate'),...
    cellstr('haenddetachrate'), cellstr('hadetachrate') ];


%--------------------------------------------------------------------------

ank    = load_ank(directory);
params = getParameters(directory);


%--------------------------------------------------------------------------

time_start = 0;
indx_start = min(find( dist(:,1) > time_start ));
time_range = indx_start : size(dist,1);
time_end   = dist(size(dist,1),1);

time  = dist(time_range, 1);
pos_a_X = dist(time_range, 2);
pos_a_Y = dist(time_range, 3);
pos_k_X = dist(time_range, 4);


if ( make_figure )
    figure('Name', pwd, 'Position', [100 400 1000 500]);
end

set( gcf, 'Name', directory);
set( gca, 'Position', [0.05 0.1 0.9 0.8] );

clf;
plot( time, pos_a_X, 'b.', 'MarkerSize', 1);
hold on;
plot( time, pos_k_X, 'k.', 'MarkerSize', 1);
%aster Y in green:
plot( time, pos_a_Y+5, 'g.', 'MarkerSize', 1);

plot( [0, time_end], [0 0], 'b:');
axis( [0, time_end, -4, 8] );

text( time_end/10, 7, textParameters(params, paramsToShow), 'FontName', 'FixedWidth' );
text( time_end/10, 5.5, sprintf('k-distance = %6.2f', ank(2)));

return;



%--------------------------------------------------------------------------

