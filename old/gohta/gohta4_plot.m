function [ bdata, binfo ] = gohta4_plot( collect, names, params )

%index of the things to plot:
id  = [3, 5];
%index of the dilution
dil = 4;

nb = size(collect, 1);

ncd_nb = unique( collect(:,7) );
dyn_nb = unique( collect(:,8) );

ncd_id = size( ncd_nb, 1 );
dyn_id = size( dyn_nb, 1 );

ncd_max = ncd_nb(ncd_id);
dyn_max = dyn_nb(dyn_id);

clf;
set(gcf, 'Position', [50 200 1200 500]);
hold on;
text(0, -0.8, sprintf('Ncd:'));
text(0, -0.6, sprintf('Dyn:'));


cnt = 1;
for jj = dyn_id : -1 : dyn_id-dil;
    if ( jj < 1 ) continue; end
    
    for ii = ncd_id : -1 : ncd_id-dil;
        if ( ii < 1 ) continue; end

        ncd = ncd_nb(ii);
        dyn = dyn_nb(jj);

        [ index, data, sims ] = find_sim( ncd, dyn, collect );

        if ~ isempty( data )
            bdata(cnt, 1:2) = data(id)';
            ncdp = round( 100 * ncd / ncd_max );
            dynp = round( 100 * dyn / dyn_max );
            %label = sprintf('%02i N %02i D', ncdp, dynp);
            text_x=cnt-0.2;
            text(text_x, -0.8, sprintf('%02i', ncdp));
            text(text_x, -0.6, sprintf('%02i', dynp));
            text(text_x, -0.3, sims, 'FontSize', 8);
            cnt = cnt + 1;
        end
    end
end

bar( bdata );
set( gca, 'Position', [0.05 0.1 0.9 0.8] );
axis([0.5, cnt-0.5, -1, 6]);
legend( names(id), 'Interpreter', 'none' );

if exist('params', 'var')
    text( 1, 5.3, textParameters(params), 'FontName', 'FixedWidth' );
end


return;



%--------------------------------------------------------------------------
function [ index, data, sims ] = find_sim( ncd, dyn, collect )

index1 = find( collect(:,7) == ncd );
index2 = find( collect(:,8) == dyn );

index = intersect( index1, index2 );

if ~isempty( index )
    data = mean( collect(index, :), 1 );
else
    data = [];
end

sims=sprintf('%04i ', collect(index, 1) );
fprintf(1, 'ncd %6i dyn %6i : data %s\n', ncd, dyn, sims);

return;