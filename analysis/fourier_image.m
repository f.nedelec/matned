function im = fourier_image( cff, s )

% function im = fourier_image( cff, s )
%
% Sum the fourier coefficients to reconstitute an image of size s*s
%
% F. Nedelec

if nargin < 2
    s = 100;
end

ang = mask_angles(s);
im  = ( 0.5 * cff(1,1) ) * ones(s,s);


for n = 1:size(cff,1)-1
   
    im = im + cff(n+1,1) * cos( n*ang ) + cff(n+1,2) * sin( n*ang );
   
end