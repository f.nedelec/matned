function  m = imscan( im )

%scan image to find two centers

global debug minPixels maxPixels;

if ( ~ isa(im,'double') ) im = double(im); end

h  = imhist( im );
ch = cumsum( h );
ch = ch( length(ch) ) - ch;

mn = min( find( ch <= maxPixels  )) - 1;
mx = max( find( ch >= minPixels ) ) - 1;

if ( isempty( mn ) ) mn = min( find( h > 0 )) - 1; end;
if ( isempty( mx ) ) mx = length( h ) - 1; end;

if ( mx == mn )
   m = [ mn, 0, 0 ];
   return
end
%[ mn mx ]


steps = min( 20, mx - mn );
db    = ( mx - mn ) / steps;

m  = zeros( steps+1, 12);
confidence = 0;

for b = steps:-1:0
   
   ba = round( db * b + mn );
   
   if ( h( ba + 1 ) ~= 0 )
      
      above = ( im > ba ) ;
      
      [cen, mom, rot, sv] = barycenter2( above );
      
      %split the image into 2, and find the barycenter in each half:
      dx = -10 * sin( rot(1) );
      dy =  10 * cos( rot(1) );
      
      [l, r] = imsplit( above, cen + [dx dy], cen - [dx dy] );
      [cenl, moml, rotl, svl] = barycenter2( l );
      [cenr, momr, rotr, svr] = barycenter2( r );

   end
   
   %save the values:
   m(b+1, 1:12) = [ ba sv rot rotl rotr dist(cenr,cenl) ];
   
end

if ( debug )   %debugging figures:

   figure(222);
   clf;
   plot( m(:,1), m(:,4)/max(m(:,4)), 'r-','LineWidth', 3 );
   hold on;
   plot( m(:,1), m(:,7)/max(m(:,7)), 'g-','LineWidth', 3 );
   plot( m(:,1), m(:,10)/max(m(:,10)), 'b-','LineWidth', 3 );
   plot( m(:,1), m(:,4)/(m(:,7)+m(:,10)), 'k-','LineWidth', 3 );
   plot( m(:,1), m(:,12)/max(m(:,12)), 'm-','LineWidth', 3 );
   
end

return

function d=dist(a,b)
   d =(a(1)-b(1))*(a(1)-b(1)) + (a(2)-b(2))*(a(2)-b(2));
return