function [ cen1, cen2, found ] = dotfind2( im )

if ( isfield( im, 'data' ) ) im = im.data;  end
global debug hidingR;

mscan  = imscan( im );

[ confidence, indx ] = max( mscan(:,2) );
base = mscan(indx,1);
fprintf('2: base = %i, confidence = %.3f', base, confidence);

if ( confidence >= 0 )
   
   above = im - base ;
   above = above .* ( above > 0 );
   
   [cen, mom, rot, sv] = barycenter2( above );
   
   %split the image into 2, and find the barycenter in each half:
   dx = -10 * sin( rot(1) );
   dy =  10 * cos( rot(1) );
   
   [l, r] = imsplit( above, cen + [dx dy], cen - [dx dy] );
   cen1 = barycenter1( l );
   cen2 = barycenter1( r );
   found = 2;
   
else %low confidence, search for one center:
   
   [ confidence, indx ] = max( mscan(:,2) );
   base = mscan( indx, 1);

   fprintf('1: base = %i, confidence = %.3f', bsae, confidence);
      
   above = im - base ;    
   above = above .* ( above > 0 );
   cen1  = barycenter1( above );
   cen2  = cen1;
   
   if ( confidence > 1.8 )
      found = 1;
   else
      found = 0;
   end

end

if (debug==3)
   show(above);
   plot(cen1(2), cen1(1),'bx','MarkerSize',10,'Linewidth',3);
   plot(cen2(2), cen2(1),'rx','MarkerSize',10,'Linewidth',3);
end


return;
