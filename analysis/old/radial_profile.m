function [ profile_in, profile_out ] = radial_profile( im, center )

% With a binary image im, calculate the distance from the center,
% for each angle a = 1:360

cx = center(1);
cy = center(2);

show( im );
plot( cx, cy, 'bx');

for a = 1:360;
    
    angle = a * pi / 180;
    cs = cos( angle );
    sn = sin( angle );
    
    color_old = im( cx, cy );
    color_new = color_old;
    
    ab = 0;
    while ( color_new == color_old )
        
        ab = ab + 1;
        px = round( cx + ab * cs );
        py = round( cy + ab * sn );
        
        if ~inside_image( im, px, py )
            break;
        end
        
        color_new = im( px, py );
        
    end
    
    profile_in( a ) = ab;
    
    %-------------continue until we fall outside:
    while( inside_image( im, px, py ) )      
        
        ab = ab + 1;
        px = round( cx + ab * cs );
        py = round( cy + ab * sn );
                
   end

   % the point before should be inside the image:
   ab = ab - 1;
   px = round( cx + ab * cs );
   py = round( cy + ab * sn );

   color_old = im( px, py );
   color_new = color_old;
   
   %move inward until we change color:
   while ( color_new == color_old )
        
        ab = ab - 1;
        px = round( cx + ab * cs );
        py = round( cy + ab * sn );
        
        if ( ab < 1 )
            break;
        end
        
        color_new = im( px, py );
        
    end

    profile_out( a ) = ab;
    
end



figure;
plot(profile_in);
hold on;
plot(profile_out);
max_size = max(size(im));
axis([0 360 0 max_size]);


function inside = inside_image( im, px, py )
inside = ( px > 0 ) && ( px <= size( im, 1 ) ) && ( py > 0 ) && ( py <= size(im,2) );
end