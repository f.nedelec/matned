function mov = finddic( i )

i = double(i);
dx =  0:5;
dy = -5:5;

si = size(i);
figure('Position',[200, 200, si(1), si(2)],'MenuBar','none');
colormap( gray(255) );

f=1;
for x=1:size(dx,2);
for y=1:size(dy,2);
   
   im = imsub(i, i, [ dx(x) dy(y) ] );
   ib = rebin( im, '8bits scale' );
   image(ib);
   set(gca,'Position',[0 0 1 1] );
   mov(f) = getframe; 
   f = f + 1;
end
end

movie(mov,4);
