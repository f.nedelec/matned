function im2 = abovebackgd( im )


h = imhist( im );
b = histmin( h );
im2 = ( double( im ) - b );
im2 = im2 .* ( im2 > 0 );
