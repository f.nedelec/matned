function [ cen1, cen2, i ] = dottrack2( im, cen1, cen2 )

global debug;
global searchR minPixels;

if (debug==4)
   debugfig = show1(im);
   plot(cen1(2), cen1(1),'bo','MarkerSize',10);
   plot(cen1(2), cen1(1),'bo','MarkerSize',hidingR);
   plot(cen2(2), cen2(1),'ro','MarkerSize',10);
   plot(cen2(2), cen2(1),'ro','MarkerSize',hidingR);
end

hidingM    = maskcircle1( 2 * searchR + 1);
searchM    = maskcircle1( 2 * searchR + 1);
[sx sy]    = size( im );
offset = 0;

c1 = round ( cen1 );
c2 = round ( cen2 );

for i=1:100
   
   %save old positions:
   oc1 = c1;
   oc2 = c2;
   
   
   c1 = round ( cen1 );
   c2 = round ( cen2 );
   
   %refine the centers:
   
   im21  = image_crop( im, [ c1-searchR, c1+searchR ], 0 );
   im21  = applymask( im21, hidingM, c2-c1+offset, 1 );
   im21  = im21 .* searchM;
   base1 = valuetop( im21, minPixels );
   above = im21 - base1;
   above = ( above > 0 ) .* above;
   cen1  = c1 + barycenter1( above ) - ( searchR + 1 );
  
   
   im22  = image_crop( im, [ c2-searchR, c2+searchR ], 0 );
   im22  = applymask( im22, hidingM, c1-c2+offset, 1 );
   im22  = im22 .* searchM;
   base2 = valuetop( im22, minPixels );
   above = im22 - base2;
   above = ( above > 0 ) .* above;
   cen2  = c2 + barycenter1( above ) - ( searchR + 1 );
   
   
   %make sure we stay inside:
   cen1 = max( [ 1 1 ] , cen1 );
   cen1 = min( size(im), cen1 );
   
   cen2 = max( [ 1 1 ] , cen2 );
   cen2 = min( size(im), cen2 );
   
   %check for convergence:
   conv1 = 0;
   conv2 = 0;
   if ( round(cen1(1)) == c1(1) ) & ( round(cen1(2)) == c1(2) ) conv1 = 1; end
   if ( round(cen1(1)) == oc1(1) ) & ( round(cen1(2)) == oc1(2) ) conv1 = 1; end
   if ( round(cen2(1)) == c2(1) ) & ( round(cen2(2)) == c2(2) ) conv2 = 1; end
   if ( round(cen2(1)) == oc2(1) ) & ( round(cen2(2)) == oc2(2) ) conv2 = 1; end
   if ( conv1 & conv2 ) break; end
   
   if ((debug==3) | (debug==4))
      plot(cen1(2), cen1(1),'bx','MarkerSize',10);
      plot(cen2(2), cen2(1),'rx','MarkerSize',10);
   end

end


if (debug==11)
   show1(im21>base1, 'scale 8bit small', [searchR+1, searchR+1] );
   show1(im22>base2, 'scale 8bit small', [searchR+1, searchR+1] ); 
end

return;