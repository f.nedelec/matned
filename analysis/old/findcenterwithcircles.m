function c = findcenterwithcirles( im, search_radius, debug )
% function c = findcenterwithcircles( im, search_radius, debug )
%
% finds a center in the image, of a shallow bright or a dark region
% the algorithms relies on looking in circular regions of decreasing size,
% at each step, a new center is calculated from the best fit of the local
% image by a quadradic function a(x^2+y^2) + bx + cy + f ( i.e. a circle )
%
% nedelec@embl-heidelberg.de    last modified 7 / 9 / 2002

if nargin < 3 
    debug = 0;
end

if ( nargin < 2 ) | isempty( search_radius )
    search_radius = [ 64, 256 ];
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

ell = fitellipse( im );
c   = centerofellipse( ell );

if ( debug )
        
    show( im );
    plot(c(2), c(1), 'yo', 'MarkerSize', 10, 'LineWidth', 1); 

end

searchR = max( search_radius );

while searchR > min( search_radius )
    
    if ( debug )
        
        cx = c(2) + searchR * cos( 0:0.1:6.3 );
        cy = c(1) + searchR * sin( 0:0.1:6.3 );
        plot( cx, cy, 'w-');
        plot(c(2), c(1), 'yx', 'MarkerSize', 10, 'LineWidth', 1); 
        
    end

    searchMask  = maskcircle1( 2 * searchR + 1 );

    for i=1:300
   
        co = c;

        if ( debug ) plot(c(2), c(1), 'y.'); end
           
        imc   = image_crop( im, [ c - searchR, c + searchR ], 0 ) .* searchMask;
        ell   = fitcircle( imc );
        c     = centerofellipse( ell ) + ( c - searchR );
        
        %force center to stay inside:
        c = max( [ 1, 1 ], c );
        c = min( size(im), c );
        
        %check for convergence:
        if ( round( c - co ) == [ 0  0 ] )
            break;
        end
   
    end
    
    searchR = searchR / 2;
    
end


return;