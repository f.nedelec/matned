function [ cen, sv ] = weighted_center( im )

% function [ cen, sv ] = weighted_center( im )
%
% Compute the center of the image, weighted by the the pixel values
% F. Nedelec

im = double( im );
sz = size( im );
sv = sum( sum( im ) );

%if the sum of pixel is zero, we return the middle of the image:
if sv <= 0 
   warning('   weighted_center(): sum of pixel values is negative!');
   cen = ( sz + [1 1] ) / 2;
   return;
end

ax = (1:sz(1))  * sum( im, 2 );
ay = sum( im, 1 ) * (1:sz(2))';

cen = [ ax/sv, ay/sv ];

end


