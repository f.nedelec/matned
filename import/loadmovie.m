function img = loadmovie(prefix,nums);

if nargin <= 1
   startnum = 1;
   stopnum = inf;
else
   startnum = nums(1);
   stopnum = nums(2);
end;
[startnum stopnum]

num = 1;
stop = 0;
while (~stop)
   fname = strcat(prefix,num2str(num+startnum-1,'%04d'));
   [fid,message] = fopen(fname);
   stop = (fid == -1) | (num+startnum-2) >= stopnum;
   if ~stop
      disp(['reading ',fname]);
      im = imread(fname);
      [xs,ys]= size(im);
      img(1:xs,1:ys,num) = im (:,:);
      num = num + 1;
   end;
end;

