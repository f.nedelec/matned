function savepdf(filename, fig)

% function savepdf(filename, fig)
%
% export figure 'fig' to PDF document, preserving its aspect ratio
% By default, fig='current figure'
%
% F. Nedelec, January 2017

if nargin < 2 || isempty(fig)
    fig = gcf;
end
if ischar(fig)
    fig = gcf;
end

%% check filename

if nargin > 0 && ~ isempty(filename)
    [~,~,ext] = fileparts(filename);
    if isempty(ext)
        filename = [ filename, '.pdf' ];
    elseif ~strcmpi(ext, '.pdf')
        error('incorrect file name extension');
    end
else
    filename = sprintf('figure%i.pdf', get(gcf, 'Number'));
end

%% check resolution 

pos_pixels = getpixelposition(fig);
scr = get(0, 'ScreenPixelsPerInch');
pos_inches = pos_pixels(3:4)/scr;

fig.PaperUnits = 'inches';
fig.PaperSize = pos_inches;
fig.PaperPosition = [0 0 pos_inches];

% This will preserve the background color:
fig.InvertHardcopy = 'on';

%saveas(fig, filename, 'pdf');
print(filename, fig, '-painters', '-dpdf');

end
