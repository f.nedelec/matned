function [ v, f ] = sliding(c, m, D, model)
% Estimates the sliding speed of a filament
% with 'c' crosslinkers and 'm' motors
% F. Nedelec, 20.03.2018---22.3.2018

if nargin < 4
    model = 1;
end

if nargin < 2
    c = 50;
    m = 50;
end

%% System
L = 1;
a = 0.008;
kT = 0.0042;

%% Motors

vm = 0.025;
fs = 6;

% speed at which the motors move:
% this is the usual linear force-velocity relationship
function v = speedM(f)
   v = vm * ( 1 - f / ( m * fs ) );
end
% derivative of speedM()
function v = derivM(~)
   v = -vm / ( m * fs );
end

%% Crosslinkers

if nargin < 3
    D = 0.0001;
end
% calculate a hopping rate from the diffusion:
rate = D / ( a * a );

% probability of a site to be unoccupied:
omega = 1 - c * a / L;

% force per crosslinker is fc=f/c and dependence is exp(fc/f?) with:
f1 = c * kT / a;
f2 = 2 * f1;

%% parameter for dependence of stepping on force

% speed at which the crosslinkers move:
% probably of hopping against the force are decreased by exp(-f/f0),
% while the probability of hopping with the force are increased exp(+f/f0)
% hence the mean flux varies like the sum of two exponentials

function v = speedC(f)
   v = rate * a * omega * ( exp(f/f2) - exp(-f/f2) );
end
% derivative of speedC()
function v = derivC(~)
   v = rate * a * omega * ( exp(f/f2) + exp(-f/f2) ) / f2;
end


% with the formula from Wang et al.
% kR = x/(1-exp(-x)) ~ 1 + x/2 + O(x^2)
% kL = x/(exp(x)-1)  ~ 1 - x/2 + O(x^2)
% kR - kL = x;
function v = speedW(f)
   v = rate * a * omega * f / f1;
end
% derivative of speedW()
function v = derivW(~)
   v = rate * a * omega / f1;
end

fW = 1 / ( rate * a * omega / ( vm * f1 ) + 1 / ( m * fs ));
vW = 1 / ( 1/vm + f1 / ( rate * a * omega * m * fs ) );
vW = 1 / ( 1/vm + c * kT / ( m * D * omega * fs ) );
% for the Wang relations:
% 1/v = 1/vm + f1 / ( rate * a * omega * m * fs );
% 1/v = 1/vm + c * kT / ( m * D * omega * fs );
% 26.03.2018
 v1 = D * fs / kT;
 cmax = L / a
% 1/v = 1/vm + 1/v1 * c / ( m * ( 1 - c / cmax ) );


if ( model )
    f = fW;
else
    % calculate force for which the two speeds are equal using Newton's method:
    f = 0;
    for i = 1 : 1000
        df = ( speedC(f) - speedM(f) ) / ( derivC(f) - derivM(f) );
        f = f - df;
        if ( abs(df) < 0.00001 )
            break
        end
    end
end

v = speedM(f);

if nargin < 1

    fprintf(1, 'cmax %f, v1 %f\n', cmax, v1);
    fprintf(1, 'speed %f, force %f, unoccupancy %f, ratio %f\n', v, f, omega, f/f1);
    fv = 0:0.01:100*ceil(fW/100);
    figure; hold on;
    plot(fv, speedC(fv));
    plot(fv, speedM(fv));
    plot(fv, speedW(fv));
    plot([f f], [0 vm], 'r-');
    ylim([0 vm]);
    xlabel('Force');
    ylabel('Speed');
end

end



