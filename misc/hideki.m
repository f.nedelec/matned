function hideki(image)

% Macro to analyse the size of microtubule asters
%
% F. Nedelec, EMBL, 2005 - 2008

if nargin == 1
    if ischar(image)
        im = tiffread(image);
    else
        im = image;
    end
end


%compatibility with tiffread:
if ( isfield(im, 'filename') ) 
    filename = im.filename;
end
if ( isfield(im, 'data') ) 
    im = double(im.data);
end


%% ==================   find the background level:

base1 = min(min(im));
base2 = image_background(im);
%disp( sprintf('background = %i,   %i', round(base1), round(base2) ) );

%% ==================   select a rectangular ROI:

poly = mouse_roi(im);
close;

%% ==================   extract the image from the ROI:

[mask, roi] = mask_polygon(poly);

subim  = image_crop(im, roi);
base3  = min(min( subim ));
subim  = mask .* subim + ( 1 - mask ) .* base2;

%% ==================   find the center of the aster, as the brightest spot:

center = find_bright_spot(subim);

%% ==================   compute the maximum radius allowed by ROI:

radius = floor(max([center, roi(3:4)-roi(1:2)-center]));
%disp( sprintf('radius = %i ', round(radius) ) );

%% ==================   compute the radial profile:

bin = 5;

[ cnt, val, var, dist ] = radial_profile(subim, center, bin, mask);


base4 = min(val);
disp( sprintf('backgrounds: min %.0f, base %.0f, roi_min %.0f, profile_min %.0f', base1, base2, base3, base4 ) );

%calculate the sum of pixel fluorescence:
profile    = ( val - base4 ) .* cnt;

%% ==================   total mass of the aster is the sum of all pixel values within

%the scalar 10^-5 is arbitrary:
mass  = 10^-5 * sum(profile);

%mass2 = 10^-5 * ( sum( sum( subim .* mask ) ) - base4 * sum(sum(mask)) ) ;
%disp( sprintf('check: mass  %.1f  ~  %.1f', mass, mass2 ) );


%% ==================   extract the mean radius of aster:
 
profile    = profile ./ max(profile);
meanlength = bin * sum(profile);

mess1 = sprintf('sum intensity %6.1f 10^5 au\n', mass );
mess2 = sprintf('mean radius   %6.1f pixels\n', meanlength );
disp([ mess1, mess2 ]);

%% ==================   make a figure:

figure('Name', 'radial profile', 'Position', [100, 100, 800, 400]);
hAxes = gca;
set(hAxes, 'Position', [0.05 0.05 0.45 0.95]);

show_image(log(subim), 'Handle', hAxes);

%--------------plot roi-polygon:
%plot( poly(:,2)-roi(2), poly(:,1)-roi(1), 'm-');

%--------------plot center:
plot(center(2), center(1), 'bx', 'MarkerSize', 15, 'LineWidth', 1);

%--------------plot circle of radius 'meanlength':
cx = center(2) + meanlength * cos(0:0.1:6.3);
cy = center(1) + meanlength * sin(0:0.1:6.3);
plot( cx, cy, 'b-');

%--------------plot outer circle defined by ROI:
cx = center(2) + radius * cos(0:0.1:6.3);
cy = center(1) + radius * sin(0:0.1:6.3);
plot( cx, cy, 'w-');

axes('Position', [0.58 0.12 0.4 0.7]);
plot(dist, profile );
axis([0, radius, 0, max(profile)]);
hold on;

if exist('filename', 'var')
    text(0, 1.2, filename, 'FontSize', 14, 'Interpreter', 'none', 'FontWeight', 'bold');
end
ylabel('intensity  ( arbitrary units )', 'FontSize', 12);
xlabel('distance   ( pixels )', 'FontSize', 12);
plot([ meanlength, meanlength], [ 0, max( profile ) ], 'k-');
text(0, 1.05, [mess1, mess2], 'FontSize', 14, 'FontName', 'fixedWidth');


%% =============record to file:

end
