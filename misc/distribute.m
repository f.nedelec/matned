function hits = distribute(L, d)

hits = [];
x = - d*log(rand);


while ( x < L )
    hits = cat(2, hits, x);
    x = x - d*log(rand);
end

