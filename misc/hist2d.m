function [ Z, Xedges, Yedges, Z4 ] = hist2d(P, Xedges, Yedges, color_range, levels)

% function [ Z, Xedges, Yedges ] = hist2d(P)
% function [ Z, Xedges, Yedges ] = hist2d(P, Xedges, Yedges)
% function [ Z, Xedges, Yedges ] = hist2d(P, Xedges, Yedges, [])
% function [ Z, Xedges, Yedges ] = hist2d(P, Xedges, Yedges, color_range)
%
% calculate a 2D histogram of data points P along bins specified by Xedges,
% and Yedges
%
% P should be specified by lines as P(n,:) = { X_n, Y_n }
% or with additional Z components P(n,:) = { X, Y, Z1, Z2, Z3 ... }
%
% Z(i,j,1) returns the number of point P in bin (i,j),
% which are the points satisfying:
%       Xedges(j) <= P(:,1) < Xedges(j+1)
%       Yedges(i) <= P(:,2) < Yedges(i+1)
% Z(i,j,n+1) returns the average of P(:,2+n) in bin (i,j)
% 
% Points outside the outer edges are not counted
% if Xedges and Yedges are ommited, they are calculated automatically,
% from the min/max values of the data, to have 16 points/bin on average
%
% F. Nedelec, September 2007, August 2008

if nargin < 2
    [ Z, Xedges, Yedges ] = histc2d(P);
else
    [ Z, Xedges, Yedges ] = histc2d(P, Xedges, Yedges);
end

max_indx = size(P,2)-1;

%% compatibility with older format

if nargout == 4
    Z4 = Z(:,:,2:max_indx);
end

%% display the results, with a 'jet' colormap:

bits = 16;  %color depth used for display

for c = 1:max_indx

    dz = Z(:,:,c);
    figure('Position', [200 400 600 420], 'Name', sprintf('%s(:,%i)', inputname(1), c+2));

    if isempty(color_range)
        zMin = min(min(dz));
        zMax = max(max(dz));
        fprintf('color range [%.2f %.2f]\n', zMin, zMax);
    else
        zMin = color_range(1);
        zMax = color_range(2);
    end

    if ( bits == 8 )
        im = uint8( (dz-zMin) * ( 2^bits / (zMax-zMin)) );
    else
        im = uint16( (dz-zMin) * ( 2^bits / (zMax-zMin)) );
    end
    image(Xedges, Yedges, im);
    caxis([0 2^bits]);
    colormap( jet(2^bits) );

    %add a colorbar:
    cbh=colorbar();

    %add labels to the colorbar
    if  nargin > 4
        set(cbh, 'YTickMode', 'manual');
        ticks = sort( 2^bits * ( levels - zMin ) / (zMax-zMin) );
        tvals = ticks/(2^bits) * (zMax-zMin) + zMin;
        labels=num2str(tvals(:), ' %.0f');
        set(cbh, 'YTick', ticks);
        set(cbh, 'YTickLabel', labels);
    end

end


end
