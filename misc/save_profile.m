function save_profile(filename, dataX, dataY)
%
%
% F. Nedelec, March 2014

if ~isvector(dataX)
    error('argument 2 (dataX) should be a vector');
end

len = length(dataX);

if size(dataY, 1) ~= len
    error('size(arg3,1) (dataY) should match size(arg2,1) (dataX)');
end

fid=fopen(filename, 'wt');
fprintf(fid, '%% F. Nedelec %s\n', date);

for i=1:len
    fprintf(fid, '%10.5f ', dataX(i));
    fprintf(fid, ' %10.5f', dataY(i,:));
    fprintf(fid, '\n');
end
fclose(fid);

end