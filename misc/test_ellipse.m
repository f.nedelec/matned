function test_ellipse(L)

V = 10;

% prolate:
W = sqrt(3*V/(4*pi*L));
Sp = ellipsoid_surface(L, W, W);

% oblate:  
R = 2;
T = 3*V/(4*pi*R*R);
So = ellipsoid_surface(R, R, T);

% surface energy
sigma = 35;

Es = sigma * ( So - Sp );

% polymer mass
RR = 1.6;
kappa = 20;
M = 140;
C = 1/RR;
Em = 0.5 * kappa * C^2 * M;

sigma = kappa * M / ( 4 * pi * RR^4 );

fprintf(1, 'Energy difference %f\n', Es-Em);