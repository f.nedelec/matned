function save_objects(objects, filename, info)

% Save objects to a file, which by default is 'objects.txt'
%
% Syntax:
%            save_objects(objects, filename)
%            save_objects(objects)
%
% The objects are saved in a column-based text file.
%
% See also
%      edit_objects, load_objects and show_objects
%
% F. Nedelec, 2012

if isempty(objects)
    fprintf(2, 'Warning: no object to save!\n');
end

if nargin < 1
    filename = 'objects.txt';
end

try
    movefile(filename, [filename, '.back']);
    fprintf(2, 'Pre-existing "%s" was renamed "%s.back"\n', filename, filename);
catch
end

%%

fid = fopen(filename, 'wt');
fprintf(fid, '%% %s\n', date);

if nargin > 2
    if ~ischar(info)
        error('3rd argument should be info string');
    end
    fprintf(fid, '%% %s\n', info);
end

fprintf(fid, '%% id pt1_X pt1_Y ...\n');
fprintf(fid, '\n');

for n = 1:length(objects)
    fprintf(fid, '%4i,', objects{n}.id);
    if isfield(objects{n},'info')
        fprintf(fid, ' %s,', objects{n}.info);
    else
        fprintf(fid, ' ,');
    end
    for d = 1:size(objects{n}.points, 1)
        fprintf(fid, '  %9.2f', objects{n}.points(d, 1:2));
    end
    fprintf(fid, '\n');
end
fclose(fid);

fprintf('%i objects saved in %s\n', length(objects), filename);


end
