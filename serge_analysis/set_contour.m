function [profiles] = set_contour(image,opt)

% spindles = save_regions(spindles, filename)
% Save the spindles to file filename or 'spindles.txt'(default)
% S. Dmitrieff, Nov 2012

filename='spindles.txt';
regfilename='regions.txt';
pointfilename='points.txt';
if nargin < 2
    opt=spin_default_options();
end
if isfield(opt,'seg_number')
    nc=opt.seg_number;
else
    defopt=spin_default_options;
    nc=defopt.seg_number;
end
%if isfield(opt,'clicks_filename')
%    clickfile=opt.clicks_filename;
%else
%    defopt=spin_default_options;
%    clickfile=defopt.clicks_filename;
%end
if isfield(opt,'max_polarity')
	npmax=opt.max_polarity;
else
	defopt=spin_default_options();
	npmax=defopt.max_polarity;
end
if  isfield(image, 'data') 
    if ( length(image) > 1 ) 
        disp('show_image displaying picture 1 only');
    end
    image = image(1).data;
end
% compatibility with tiffread color image
if  iscell(image)    
    tmp = image;
    image = zeros([size(tmp{1}), 3]);
    try
        for c = 1:numel(tmp)
            image(:,:,c) = tmp{c};
        end
    catch
        disp('show_image failed to assemble RGB image');
    end
    clear tmp;
end
if isfield(opt,'radius')
    rad=opt.radius;
else
    defopt=spin_default_options();
    rad=defopt.radius;
end

show_image(image);

spindles=load_objects(filename);
clickets=load_objects(pointfilename);
regions=load_regions(regfilename);
% Analysis of spindles
num_spindles=numel(spindles);
n_reg=numel(regions);
n_poles=numel(clickets);
xmax=size(image,1);
ymax=size(image,2);

% Variables
n_spinds=zeros(1,npmax+1);
seg_intens=cell(1,npmax+1); %intensity of segments per unit lengths
seg_width=cell(1,npmax+1); % width of segments
seg_dens=cell(1,npmax+1); % density (intens / width) of segments
tot_intens=cell(1,npmax+1); % total intensity per spindle (NOT per unit length)
tot_lengs=cell(1,npmax+1); % length of spindles along the segments =/= center-pole dist
tot_aspect=cell(1,npmax+1); %Aspect ration of the spindle (slope of width(l))
np=0;
contour{2*n_poles}.info=num2str(n_poles);
for n = 1:n_poles
    pl=clickets{n};
    clicks=pl.points;
    state=str2num(pl.info);
    if state>0
        ide=pl.id;
        center=clicks(1,:);
        points=clicks;
        coords=[center center]-[rad rad -rad -rad];
        xp_min=min(points(:,1));
        xp_max=max(points(:,1));
        yp_min=min(points(:,2));
        yp_max=max(points(:,2));
        % Making sure clicks are in the image, and image is well defined
        coords(1)=max( min(coords(1),xp_min) , 1);
        coords(2)=max( min(coords(2),yp_min) , 1);
        coords(3)=min( max(coords(3),xp_max) , xmax);
        coords(4)=min( max(coords(4),yp_max) , ymax);
        % Starting the analysis
        points=points-ones(nc+1,1)*coords(1:2);
        st=state+1;
        n_spinds(st)=n_spinds(st)+1/state;
        im=image(coords(1):coords(3),coords(2):coords(4));
        im=im-image_background(im);
        [profiles]=analyze_intens_onespind( points , im );
        lengths=segments_lengths(points);
        ints=zeros(1,nc);
        rms=zeros(1,nc);
        mea=zeros(1,nc);
        left=zeros(nc,1);
        right=zeros(nc,1);
        curv_abs=zeros(1,nc);
        curv_abs(1)=lengths(1)/2;
        for k=1:nc
            [ints(1,k),right(k),left(k)]=analyze_profile(profiles{1,k});
        end
        for k=2:nc
            curv_abs(k)=curv_abs(k-1)+(lengths(k-1)+lengths(k))/2;
        end
        [ri,le]=contour_from_profs(clicks,right,left);
        contour{n*2-1}.points=ri;
        contour{n*2-1}.info=num2str(n);
        contour{n*2-1}.id=2*ide-1;
        contour{n*2}.points=le;
        contour{n*2}.info=num2str(n);
        contour{n*2}.id=2*ide;
    end
end
        
% Saving 
save_objects(contour,'contour.txt');

end
