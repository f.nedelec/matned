function [ Damin,Pamin ] = dist_points_segments( points , segments )
% Returns signed dist and projections of points in image to segments
%   Sim  stores the dimension of the image and segments contains ns
%   segments.
%   Returns Da, the Sim(1)*Sim(2)*ns array of distances and Pa, the array
%   of the projections of all points on all the segment.
%   If the projection is outside the segment, gives the minimal distance
%   to a segment end (A or B).

if nargin<2
    error('Not enough arguments');
end
Sseg=size(segments);
Spts=size(points);
if Sseg(2)~=2
    error('Wrong segment array size');
elseif Spts(2)~=2
    error('Wrong points array size'),
end
segs=segments';
% 1- Variables
ns=Sseg(1)-1;
np=Spts(1);
Damin=zeros(np,1);
Pamin=zeros(np,1);
Da=zeros(np,ns);
Pa=zeros(np,ns);
lengths=segments_lengths(segments);
add_length=[0 lengths(1:end-1)];
P=points';

% 2- Computations
for i=1:ns
    a=segs(:,i);
    b=segs(:,i+1);
    iAB=b-a;
    nAB=norm(iAB);
    % > Computing the director vecto u and its normal v
    u=iAB'/nAB;
    v=[-u(2),u(1)];    
    A=a*ones(1,np);
    B=b*ones(1,np);
    % > Creation of vectors Points -> A (PA) and Points -> B (PB), and norms.
    PA=A-P;
    PB=B-P;
    nPA=sqrt(PA(1,:).^2+PA(2,:).^2);
    nPB=sqrt(PB(1,:).^2+PB(2,:).^2);
    % > Computing the signs
    Signs=sign(v*PA);
    % > Computing the distances
    Projs=-u*PA;
    vects=PA+u'*Projs;
    Da(:,i)=sqrt(vects(1,:).^2+vects(2,:).^2).*Signs;
    Pa(:,i)=Projs + add_length(i);
end

[Damin,ix]=min(Da,[],2);

for i =1:np
    Pamin(i)=Pa(i,ix(i));
end


end

