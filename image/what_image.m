function what_image(arg)

% what_image(arg)
%
% report some info about the image
% F. Nedelec, 26.04.2019

%%compatibility with tiffread:
if ( isfield(arg,'data') )
    if length(arg) > 1
        fprintf(2, '%3i x', length(arg));
        im = double(arg(1).data);
        for z = 2 : length(arg)
            im = im + double(arg(z).data);
        end
        im = im / length(arg);
    else
        im = arg.data;
        size(im)
    end
else
    if size(arg, 3) > 1
        fprintf(2, ' %3i x', size(arg,3));
        im = mean(arg, 3);
    else
        im = arg;
    end
end

fprintf(2, ' %4i x', size(im,1));
fprintf(2, ' %4i', size(im,2));
im = double(im);

val = reshape(im, numel(im), 1);

fprintf(2, ' %15s in [%12.5f %12.5f] mean %9.4f\n',...
    inputname(1), min(val), max(val), mean(val));

% Histogram

if 1
    show_image(im, 'Alpha', real(im>0));
    val = reshape(im, numel(im), 1);
    top = max(val);
    up = 500;
    h = histc((up/top)*val, 0:up);
    figure('Name', inputname(1), 'Position', [100 150 200+up 300], 'MenuBar', 'none');
    axes('Position', [0.04 0.1 0.93 0.85] );
    plot((1:up)*(top/up), h(2:end), 'bo', 'MarkerSize', 5);
    xlim([0 top]);
    title('Histogram of pixel values');
end

end
