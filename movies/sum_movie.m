function s = sum_movie(mov)

% function s = sum_movie(mov)
%
% Sum all images in a movie
%
%
% F. Nedelec, June 2014

nbi = length(mov);
s = double(mov(1).data);

for i = 2:nbi
    s = s + double(mov(i).data);
end

s = s / nbi;

end