%script function imc = cleanmovie2( im )

%define ROI by mouse clicks:
poly = ROImousepoly( im(1) );
[mask, roi] = maskpolygon( poly );

for indx = 1:length(im)
   
   disp(['frame ', num2str(indx)]);
   im2  = medfilt2( im(indx).data );
   im(indx).data = image_crop(im2, roi) ; %.* mask;  
   im(indx).width = size(im2,1);
   im(indx).heigth = size(im2,2);
   
end
