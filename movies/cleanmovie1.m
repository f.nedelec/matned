function imc = cleanmovie( im )

for indx = 1:max(10,length(im))
   h(indx) = histmin( imhist( im(indx).data ) );
end

backgd = mean( h );

%define ROI by mouse clicks:
poly = ROImousepoly( im(1) );
[mask, roi] = maskpolygon( poly );

for indx = 1:length(im)
   
   im2  = double( image_crop( im(indx).data, roi ) );
   im2  = ( im2 - backgd ) .* mask;
   im2  = im2 .* ( im2 > 0 );
   
   imc(indx).data = im2;
   
end
