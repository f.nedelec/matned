function [ mov, c1, c2, founds ] = movietrack2( im )

global debug;
global minDistance;

%define ROI by mouse clicks:
poly = ROImousepoly( im(1) );
[mask, roi] = maskpolygon( poly );
fig1 = gcf;

%track all movie:
for indx=1:length(im)  
   
   imc  = image_crop( im(indx).data, roi );
   imc  = double( imc ) .* mask;

   if ( indx == 1 )   %make the first guess:
      [d, e, found] = dotfind2( imc );
      avgd = 2;
      avge = 2;
   else
      [ d, e ] = dottrack2( imc, d, e);
   end
   
   %test if the two center have collapsed:
   same = dist( d, e ) < minDistance;
   
   %test if we have some features in the spots:
   lostd = ( avgd < 1.5 );
   loste = ( avge < 1.5 );
   
   %if we lost the points, research them:
   if ( lostd | loste | same  )
      [d, e, found] = dotfind2( imc );
      fprintf( 'lost track %.3f  %.3f | same %i | found %i \n',...
         avgd, avge, same, found );
   end

   %round it and translate:
   c1(indx, 1:2) = round(100*d(1:2))/100 + roi(1:2) - [ 1 1 ];
   c2(indx, 1:2) = round(100*e(1:2))/100 + roi(1:2) - [ 1 1 ];
   founds(indx)  = found;
   
   fprintf('%i | %.2f %.2f : %.2f | %.2f %.2f : %.2f ',...
      	indx, d(1), d(2), avgd, e(1), e(2), avge );
   
   %make a little movie, for checking afterward:     
   imb = rebin( imc, 'bin2 scale 8bit');
   if ( indx == 1 )
      smallmovie = show1( imb, 'raw small', indx);
   else
      figure( smallmovie );
      cla;
      image( imb );
   end
   plot(d(2)/2, d(1)/2, 'b-o','MarkerSize',10);
   plot(e(2)/2, e(1)/2, 'r-o','MarkerSize',10);
   pause(0.1);
   mov(indx)=getframe;
   
end


%reorder points according to smallest step:
[c1, c2] = reorderpoints( c1, c2);


%make a graph of the tracking:
figure( fig1 );
for indx=1:length(im)
   plot(c1(indx,2), c1(indx,1),'bo');
   plot(c2(indx,2), c2(indx,1),'ro');
   plot([c1(indx,2) c2(indx,2)], [c1(indx,1) c2(indx,1)],'y-');
end

if ( debug == 20 )
   tile(im, c1, c2);
end





return;


%support functions:

function x = dist(c, d)
x = sqrt( (c(1)-d(1))^2 + (c(2)-d(2))^2 );
return

function r = outofimage(c, d)
global border;
r = ( c(1) < 2 ) | ( c(1) >= d(1) ) | ( c(2) < 2 ) | ( c(2) >= d(2) );
return