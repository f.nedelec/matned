function plot_spindle(arg, save_images, lazy)
% function plot_spindle(arg, save_images, lazy)
% `arg` is a directory or a wild-card pattern (eg. `run*`)
%
% Francois's functions to plot the profile of simulated spindles
%
%
% Updated Nice, 18.09.2012; Vienna, 10.04.2014;
% Strasbourg 4.4.2015; 06.2015

if nargin < 3
    lazy = 1;
end

if nargin < 2
    save_images = 0;
end

if nargin < 1
    arg = '.';
end


if isdir(arg)
    cnt = 1;
    list{1} = fullfile(pwd, arg);
else
    p = fileparts(arg);
    d = dir(arg);
    cnt = 0;
    for i = 1:size(d,1)
        if d(i).isdir
            cnt = cnt + 1;
            list{cnt} = fullfile(pwd, p, d(i).name);
        end
    end
end

%%
global max_pos;
max_pos = 40;


for i = 1:cnt
    
    pth = list{i};
    
    if lazy && exist([pth, '/fibers.png'], 'file')
        continue
    end
    
    fprintf('%s:\n', pth);
          
    try
        plot_fibers(pth, 1);
        if save_images
            savepng(fullfile(pth, 'polarity_end.png'));
            close;
        end
    catch err
        fprintf(2, 'plot_fibers failed\n');
    end
    
    try
        plot_fibers(pth);
        if save_images
            savepng(fullfile(pth, 'fibers.png'));
            close;
        end
    catch err
        fprintf(2, 'plot_fibers failed\n');
    end
 
    try
        plot_profile(pth, 1);
        if save_images
            savepng(fullfile(pth, 'profile.png'));
            close;
        end
    catch err
        fprintf(2, 'plot_profile failed\n');
    end

    continue
    
    try
        plot_profiles(pth);
        if save_images
            savepng(fullfile(pth, 'profiles.png'));
            close;
        end
    catch err
        fprintf(2, 'plot_profiles failed\n');
    end
    
    continue

    try
        figure('Name', pth);
        subplot(2,1,1);
        plot_lengths(pth);
        subplot(2,1,2);
        plot_gammas(pth);
        if save_images
            savepng(fullfile(pth, 'gammas.png'));
            close;
        end
    catch err
        fprintf(2, 'plot_length or plot_gammas failed\n');
    end
    
    try
        plot_couples(pth);
        if save_images
            savepng(fullfile(pth, 'couples.png'));
            close;
        end
    catch err
        fprintf(2, 'plot_couples failed\n');
    end

end


