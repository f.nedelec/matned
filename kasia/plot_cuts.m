function plot_cuts(dst)
%
%
% Francois's function to analyze simulated spindles
% Mannheim 24.4.2015

if nargin < 1
    dst = '.';
end

global max_pos;

%% use all data

system(['tail +4 ', dst, '/cuts.txt | cut -c 16- > cut.txt']);
data = load('cut.txt');

bins = -max_pos:max_pos;
mids = ( bins(2:end)+bins(1:end-1) ) / 2;

cnts = histcounts(data, bins);

plot(mids, cnts);

title('MT Cuts');
xlabel('position / um');
ylabel('Bin Count');

end
