function plot_lengths(pth)
% function plot_lengths(pth)
%
% Plot lengths of fibers from cytosim simulation
%
% F. Nedelec, July 2 2015

if nargin < 1
    pth = '.';
end

filename = fullfile(pth, 'lengths.txt');

% create file
if ~exist(filename, 'file')
    cwd = pwd;
    cd(pth);
    %system('/Users/nedelec/bin/report1 info');
    status = system('/Users/nedelec/bin/report1 fiber:distribution output=lengths.txt');
    %status = system('/Users/nedelec/bin/report1 fiber:length > lengths.txt');
    cd(cwd);
    if status
        error('could not generate length.txt');
    end
end

data = load_after(filename, 4000);

len = (1:size(data, 2)) - 0.5;
avg = mean(data, 1);
dev = std(data, 1);

% plot distribution:

plot(len, avg, 'b-', 'LineWidth', 3);
hold on;

% indicate the mean:

a = sum( len .* avg ) / sum( avg );
plot([a, a], ylim, '-');
text(a, 0.75*max(avg), num2str(a), 'FontSize', 20);

% plot surface to indicate std-dev:
if size(data, 1) > 1
    
    patX = cat(2, len, flip(len));
    patY = cat(2, avg-dev, flip(avg+dev));
    patch(patX', patY', 'b', 'FaceAlpha', 0.2, 'EdgeColor', 'b', 'EdgeAlpha', 0.5);
    
end

% finish figure:
xlim([0, max(len)]);
ylim([0, max(avg+dev)]);

title('Fiber length distribution');
xlabel('Fiber length / um');
ylabel('Density');

end

