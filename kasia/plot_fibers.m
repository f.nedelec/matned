function plot_fibers(pth, opt)
%
%
% Francois's function to plot the profile of simulated spindles
% Updated Nice, 18.09.2012; Vienna, 10.04.2014; Strasbourg 4.4.2015

if nargin < 2
    opt = 0;
end

if nargin < 1
    pth = '.';
end

global max_pos;

%% only use data after 4000 sec

[data, cnt] = load_after(fullfile(pth, '/fibers.txt'), 4000);


if numel(data) == 0
    %error('Could not load data from <fibers.txt>');
    data = load([pth, '/fibers.txt']);
end


% idxL = column index for length;
% idxM = X-position of MINUS_END
% idxP = X-position of PLUS_END
if size(data, 2) == 9
    idxL = 3;
    idxM = 5;
    idxP = 8;
elseif size(data, 2) == 11
    idxL = 2;
    idxM = 3;
    idxP = 9;
elseif size(data, 2) == 14
    idxL = 2;
    idxM = 3;
    idxP = 9;
elseif size(data, 2) == 13
    idxL = 3;
    idxM = 5;
    idxP = 10;
elseif size(data, 2) == 15
    idxL = 3;
    idxM = 4;
    idxP = 10;
elseif size(data, 2) == 17
    idxL = 3;
    idxM = 5;
    idxP = 12;
else
    error('unsupported format in <fibers.txt>');
end

lenF = data(:, idxL);
endP = data(:, idxP);
endM = data(:, idxM);

%%

selR = logical( endM < endP );
selL = logical( endM > endP );
%selL = ~selR;

numR = sum(selR) / cnt;
numL = sum(selL) / cnt;
perc = round( 100 * [ numL, numR ] / (numL+numR) );

fprintf(' %i set', cnt);
fprintf(' %i left-pointing (%i%%) %i right-pointing (%i%%)\n', round(numL), perc(1), round(numR), perc(2));

bins = -max_pos:1:max_pos;
mids = ( bins(2:end)+bins(1:end-1) ) / 2;

%% Figure

figure('Name', pth, 'Position', [100 100 768 512]);

if opt
    
    plot_polarity_plus;
    xlim([-max_pos, max_pos]);
    plot([-max_pos, max_pos], [0, 0], 'g-');
    xlabel('Position / um');
    ylabel('Polarity');
    
else
    
    subplot(3,1,1);
    plot_minus_ends;
    xlim([-max_pos, max_pos]);
    ylabel('End density');

    subplot(3,1,2);
    plot_plus_ends;
    xlim([-max_pos, max_pos]);
    ylabel('End density');

    subplot(3,1,3);
    if 1
        plot_polarity_plus;
        xlim([-max_pos, max_pos]);
        plot([-max_pos, max_pos], [0, 0], 'g-');
    else
        subplot(3,1,3);
        plot_polarity_mass();
        xlim([-max_pos, max_pos]);
    end
    xlabel('Position / um');
    ylabel('Polarity');

end

if 1
    % signal with background if too few fibers
    if ( numL < 1000 || numR < 1000 )
        set(gcf, 'Color', [1,0.6,0.9]);
    end
end

return

%% SUB-FUNCTIONS

    function plot_minus_ends()
        
        lh = histcounts(endM(selL), bins) / cnt;
        rh = histcounts(endM(selR), bins) / cnt;
        
        plot(mids, lh, 'r-', 'LineWidth', 3);
        hold on;
        plot(mids, rh, 'b-', 'LineWidth', 3);
        title('Minus ends');
        legend('left-pointing', 'right-pointing');
        
    end


    function plot_plus_ends()
        
        lh = histcounts(endP(selL), bins) / cnt;
        rh = histcounts(endP(selR), bins) / cnt;
        
        plot(mids, lh, 'r-', 'LineWidth', 3);
        hold on;
        plot(mids, rh, 'b-', 'LineWidth', 3);
        title('Plus ends');
        legend('left-pointing', 'right-pointing');
        
        if 0
            posR = mean(endP(selR));
            posL = mean(endP(selL));
            limY = ylim;
            plot([posR posR], limY, 'b-');
            plot([posL posL], limY, 'r-');
            
            % signal with green background if polarity is reversed
            if ( posR < posL )
                set(gcf, 'Color', 'Green');
            end
        end
        
    end


    function plot_polarity_plus()
        
        lh = histcounts(endP(selL), bins) / cnt;
        rh = histcounts(endP(selR), bins) / cnt;
        pol = ( rh - lh ) ./ ( rh + lh );
        
        plot(mids, pol, 'k-', 'LineWidth', 3);
        hold on;
        
        % Calculating polarity assuming the commets are of length 'tip'
        comet = 2;
        for i = 1:length(mids)
            x = mids(i);
            nPL = sum(( endP(selL) < x ) & ( x < endP(selL) + comet )) / cnt;
            nPR = sum(( x < endP(selR) ) & ( x > endP(selR) - comet )) / cnt;
            pol(i) = ( nPR - nPL ) / ( nPR + nPL );
        end
        plot(mids, pol, 'b-', 'LineWidth', 3);
        
        ylim([-1.1 1.1]);
        title('PLUS-END Polarity');
        
    end

%% polymer mass reloaded
    function plot_polarity_mass()
        
        polR = zeros(size(mids));
        polL = zeros(size(mids));
        
        for i = 1:length(mids)
            x = mids(i);
            polL(i) = sum(( endP(selL) < x ) & ( x < endM(selL) )) / cnt;
            polR(i) = sum(( endM(selR) < x ) & ( x < endP(selR) )) / cnt;
        end
        
        plot(mids, polL, 'r-', 'LineWidth', 3);
        hold on;
        plot(mids, polR, 'b-', 'LineWidth', 3);
        xlim([-max_pos, max_pos]);
        title('Polymer');
        legend('left-pointing', 'right-pointing');
    end

%% infer birth place from the length and flux speed:
    function plot_birthplace()
        growth_speed = 0.17;
        flux_speed = 0.05;
        
        ageR = lenF(selR) / growth_speed;
        ageL = lenF(selL) / growth_speed;
        
        nucR = histcounts(endM(selR)+flux_speed*ageR, bins) / cnt;
        nucL = histcounts(endM(selL)-flux_speed*ageL, bins) / cnt;
        
        plot(mids, nucL, 'r-', 'LineWidth', 2);
        hold on;
        plot(mids, nucR, 'b-', 'LineWidth', 2);
        xlim([-max_pos, max_pos]);
        title('Inferred nucleation spots');
        legend('left-pointing', 'right-pointing');
    end

end
