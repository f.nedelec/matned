function plot_couples(pth)
% function plot_couples(pth)
%
% Plot number of Couple in each state from cytosim simulation
%
% F. Nedelec, July 7 2015

if nargin < 1
    pth = '.';
end

filename = fullfile(pth, 'couples.txt');

% create file
if ~exist(filename, 'file')
    cwd = pwd;
    cd(pth);
    %system('/Users/nedelec/bin/report1 info');
    status = system('/Users/nedelec/bin/report1 couple output=couples.txt');
    cd(cwd);
    if status
        error('could not generate length.txt');
    end
end

fid = fopen(filename, 'r');

time = get_time(filename);

data = textscan(fid, '%s %u %u %u %u %u %u\n', 'CommentStyle', '%');

% plot number as a function of time:

figure('Name', pth);
hold on;
    
names = unique(data{1});
colors = 'rgbkm';

for i = 1:numel(names)
    col = colors(i);
    name = names{i};
    idx = logical( strcmp(data{1}(:), name) == 1 );
    nbs = data{7}(idx);
    plot(time, nbs, col, 'LineWidth', 2);
    nbs = data{5}(idx)+data{6}(idx);
    plot(time, nbs, [col, ':'], 'LineWidth', 2);
end

% finish figure:

legend(reshape(cat(1,names',names'), numel(names)*2, 1));

title('Number of attached Couples');
xlabel('time (s)');
ylabel('Amount');

end

%% subfunction

function time = get_time(filename)

[status, val] = system(['grep "^% time" ', filename, ' | cut -c 8-']);

if status == 0
    time = str2num(val);
else
    time = [];
end

end

