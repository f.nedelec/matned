function [ plane, score, inplane ] = robust_planar_fit(PX, PY, PZ, PW, verbose)

%function [ plane, score, projections ] = robust_planar_fit(PX, PY, PZ, PW, verbose)
%
% returns the plane fitting best the cloud of points (PX, PY, PZ)
% PX, PZ and PZ should be vectors of identical dimensions
%
% This implements robust orthogonal fitting, where the sum of the absolute values
% of the distances of the points to their projection on the line is minimized.
%
% The best plane is returned as:
% plane = [nx, ny, nz, s];
% where [nx, ny, nz] is the normal to the plane, and the plane is defined
% n * pts + s = 0
%
% The goodness of fit is returned in `score`.
%
% F. Nedelec 26.4.2019

if nargin < 5
    verbose = 0;
end

if nargin < 4 || isempty(PW)
    PW = ones(size(PX));
else
    if any(size(PX) ~= size(PW))
        error('X and W should have similar size');
    end
end

if any(size(PX) ~= size(PY)) || any(size(PX) ~= size(PZ))
    error('X, Y and Z should have similar size');
end

%% Optimization

    function res = residual3(arg)
        nx = sin(arg(1))*cos(arg(2));
        ny = sin(arg(1))*sin(arg(2));
        nz = cos(arg(1));
        %[nx, ny, nz, arg(3)]
        h = nx * PX + ny * PY + nz * PZ + arg(3);
        % robust fitting:
        res = sum(PW .* abs(h));
        % squared residuals:
        % res = sum(pW .* h.^2);
    end

    off = 0;
    function res = residual1(nx, ny, nz, arg)
        h = nx * PX + ny * PY + nz * PZ + arg;
        % robust fitting:
        res = sum(PW .* abs(h));
        % squared residuals:
        % res = sum(pW .* h.^2);
    end

    function res = residual2(arg)
        nx = sin(arg(1))*cos(arg(2));
        ny = sin(arg(1))*sin(arg(2));
        nz = cos(arg(1));
        % optimizing the offset for this given normal:
        off = -mean(nx * PX + ny * PY + nz * PZ);
        off = fminsearch(@(x) residual1(nx,ny,nz,x), off);
        
        %[nx, ny, nz, arg(3)]
        h = nx * PX + ny * PY + nz * PZ + off;
        % robust fitting:
        res = sum(PW .* abs(h));
        % squared residuals:
        % res = sum(pW .* h.^2);
    end

if 0
    %optimizing all parameters simultaneously
    [fit, score, exitflag] = fminsearch(@residual3, [0,0,-mean(PZ)]);
    off = fit(4);
else
    %two stage optimization
    [fit, score, exitflag] = fminsearch(@residual2, [0,0]);
end

a1 = fit(1);
a2 = fit(2);

nx = sin(a1)*cos(a2);
ny = sin(a1)*sin(a2);
nz = cos(a1);

plane = [nx, ny, nz, off];

[ ex, ey, ~ ] = orthonormal_basis([nx, ny, nz]);

inplane = cat(2, ex(1)*PX+ex(2)*PY+ex(3)*PZ, ey(1)*PX+ey(2)*PY+ey(3)*PZ);

if verbose > 0
    
    figure('Position', [20 20 768 768], 'Color', [0.0 0.0 0.5]);
    col = repmat(PW./max(PW), 1, 3);
    scatter3(PX, PY, PZ, 128, col, 'filled');
    set(gca,'Color', [0 0 0]);
    hold on;
       
    v = plane(1) * PX + plane(2) * PY + plane(3) * PZ + plane(4);
    ix = PX - v * nx;
    iy = PY - v * ny;
    iz = PZ - v * nz;
    s = plot3(ix, iy, iz, '.');

    %s.MarkerEdgeColor = [1 1 1];
    %s.MarkerEdgeAlpha = 0;
    %s.MarkerFaceColor = [0 0 1];
    %s.MarkerFaceAlpha = 0.1;
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
    axis equal;
    drawnow;
end

if verbose > 1
    
    figure('Position', [20 20 768 768]);
    scatter(inplane(:,1), inplane(:,2), 128, col, 'filled');
    set(gca,'Color', [0 0 0]);      
    xlabel('inplane X');
    ylabel('inplane Y');
    axis equal;

end

end



