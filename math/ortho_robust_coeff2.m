function [ slope,offset,score ] = ortho_robust_coeff2(PX, PY)
%give the slope fitting the cloud of points PTS
% Pts is as :
%   x1 x2 x3 ... xn
%   y1 y2 y3 ... yn
% Uses robust orthogonal fitting

if nargin > 1
    PTS = horzcat(PX, PY);
else
    PTS = PX;
end

t0=pi/2;
[t,score]=fminsearch(@(t) out_of_lineness2D(PTS,t),t0);
slope=-tan(t);
x=mean(PX);
y=mean(PY);
offset=y-x*slope;

%pts=PTS(2,:)-slope*PTS(1,:)-offset;
%dev=sqrt(mean(pts.^2));
end

