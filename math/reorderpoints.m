function [ P1, P2, P3, P4 ] = reorderpoints( P1, P2, P3, P4 )

%[ P1, P2 ] = reorderpoints( P1, P2 );
%
% reorder the points, exchanging values in P1 and P2,
% to minimize the 'moved' distance by each point


if ( nargin == 2 )
   
   if (length(P1) ~= length(P2))
      disp('point list have different length');
   end
   
   for indx=1:(min(length(P1), length(P2))-1)
      
      d11=dist(P1(indx+1,:), P1(indx,:));
      d12=dist(P1(indx+1,:), P2(indx,:));
      d21=dist(P2(indx+1,:), P1(indx,:));
      d22=dist(P2(indx+1,:), P2(indx,:));
      
      if ( d12 + d21 < d11 + d22 ) %exchange them
         tmp(1:2)=P1(indx+1,1:2);
         P1(indx+1,1:2) = P2(indx+1,1:2);
         P2(indx+1,1:2) = tmp(1:2);
      end
      
   end
   
else
   
   d11=dist(P1, P3);
   d12=dist(P1, P4);
   d21=dist(P2, P3);
   d22=dist(P2, P4);
   
   if ( d12 + d21 < d11 + d22 ) %exchange them
      tmp= P4;
      P4 = P3;
      P3 = tmp;
   end
   
end
return;

function d=dist(a,b)
   d=(a(1)-b(1))*(a(1)-b(1)) + (a(2)-b(2))*(a(2)-b(2));
return