function [ exponent, scale, score ] = powerlaw_fit(PX, PY, verbose)
%function [ exponent, scale, score ] = powerlaw_fit(PX, PY, verbose)
%
% give the slope of the line fitting the cloud of points (PX, PY)
% PX and PY should be of identical dimensions:
%   x1 x2 x3 ... xn
%   y1 y2 y3 ... yn
%
% The function returns the best powerlaw fit to the provided data:
% fit = scale * x ^ exponent
%
% score is the summed squared residual
%
% F. Nedelec 29.09.2017

if nargin < 3
    verbose = 0;
end

if any( size(PX) ~= size(PY) )
    error('missmatch in size: X and Y should have similar size');
end

%% direct method
% http://mathworld.wolfram.com/LeastSquaresFittingPowerLaw.html

n = length(PX);
bn = n * sum(log(PX).*log(PY)) - sum(log(PX)) * sum(log(PY));
bd = n * sum(log(PX).^2) - sum(log(PX))^2;
exponent = bn / bd;

scale = exp(( sum(log(PY)) - exponent * sum(log(PX)) ) / n);

fit = scale * PX .^ exponent;
score = sum((PY - fit).^2);

if verbose > 0
    msg = sprintf('Y = %.4e * X ** %5.3f', scale, exponent);
    fprintf(1, 'best fit %s;  err = %f\n', msg, score);
    figure('Name', 'Powerlaw fit', 'Color' ,'white')
    h = scatter(PX, PY, 16);
    h.MarkerFaceColor = [ 0 0.1 1 ];
    h.MarkerFaceAlpha = 0.2;
    h.MarkerEdgeAlpha = 0;
    hold on;
    xmin = min(PX);
    xmax = max(PX);
    vx = xmin + (0:0.01:1) * (xmax-xmin);
    plot(vx, scale * vx .^ exponent, 'k-');
    title(msg);
end

%% alternative method that scan values:

if 0
    score = inf;
    exponent = 0;

    for e = 0.1:0.01:10
        [f, r] = power_fit(PX, PY, e);
        %fprintf(1, '%3.2f  err = %f\n', e, err);
        if r < score
            exponent = e;
            score = r;
        end
    end
    fprintf(1, '2 exponent = %3.2f  err = %f\n', exponent, score);

    if verbose > 0
        fit = power_fit(PX, PY, exponent);
        plot(PX, fit, 'r.');
    end
end


%% accessory function for alternative method:

    function [fit, err] = power_fit(x, y, e)
        
        xe = x .^ e;
        sc = sum( xe .* y ) / sum( xe .* xe );
        
        % best fit:
        fit = sc * xe;
        
        % sum of squared error:
        err = sum((y - fit).^2);
        
    end

end