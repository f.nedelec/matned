function t = threshold(di, value)

% mysterious function?



if (di(length(di)) > di(1) )
   m = max( find( di <= value ) );
   M = min( find( di >= value ) );
   if (isempty(m)) m=1; end
else
   m = min( find( di <= value ) );
   M = max( find( di >= value ) );
   if (isempty(m)) m=length(di); end
end


t=m;

return