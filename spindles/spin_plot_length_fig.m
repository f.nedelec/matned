function spin_plot_length_fig()

% function spin_plot_length_fig
%
% F. Nedelec, Feb. 2009
%
%


X = 20:1:46;

%210508_05pg
%Length = 30.56 +/- 3.28 (N=384)
h1 = [ 0 1 3 3 10 12 29 27 38 39 47 45 45 32 20 19 5 4 2 0 2 1 0 0 0 0 0 ];

%230508_05pg
%Length = 30.61 +/- 3.82 (N=424)
h2 = [ 4 1 6 11 11 14 23 35 31 46 44 43 53 18 27 22 15 10 4 3 2 1 0 0 0 0 0 ];

%290109_23pg
%Length = 33.31 +/- 3.68 (N=422)
h3 = [ 0 0 0 2 3 2 9 18 23 19 41 39 39 40 51 36 31 33 12 8 4 8 3 0 1 0 0 ];

%290208_50pg
%Length = 33.11 +/- 2.84 (N=422)
h4 = [ 0 0 0 1 1 3 5 6 15 19 44 48 60 62 55 38 28 15 15 5 2 0 0 0 0 0 0 ];


ha = h1+h2;
hb = h3;


figure('Toolbar', 'none', 'Position', [100 100 800 500]);
h = bar(0.5+X, cat(2, ha', hb'), 1.6);
hold on;

Xfin = 20:0.25:46;


[ma, sa] = mean_std(ha, X);
fita = exp( -((Xfin-ma)/sa).^2 ./ 2 );
fita = fita * ( sum(ha) * length(Xfin) / (length(X)*sum(fita)));
plot(Xfin, fita, 'k--');
g=0.0; set(h(1), 'FaceColor', [g g g], 'LineWidth', 1);
fprintf('Length = %f +/- %f  (N=%i)\n', ma, sa, sum(ha));


[mb, sb] = mean_std(hb, X);
fitb = exp( -((Xfin-mb)/sb).^2 ./ 2 ); 
fitb = fitb * ( sum(hb) * length(Xfin) / (length(X)*sum(fitb)));
plot(Xfin, fitb, 'k--');
g=0.3; set(h(2), 'FaceColor', [g g g], 'LineWidth', 1);
fprintf('Length = %f +/- %f  (N=%i)\n', mb, sb, sum(hb));


legend('0.5pg DNA/bead', '2.3pg DNA/bead');


set(gca, 'FontSize', 16);
set(gca, 'Position', [0.2 0.2 0.6 0.6]);

xlabel('Length / um', 'FontSize', 16, 'FontWeight', 'bold');
ylabel('Count', 'FontSize', 16, 'FontWeight', 'bold');

%title('Spindle length distributions', 'FontSize', 20);
xlim([min(X), max(X)]);

    function [m,s] = mean_std(h, X)
        m = sum(h.*(X+0.5) / sum(h));
        v = sum(h.*(X+0.5).^2 / sum(h));
        s = sqrt( v - m^2 );
    end

end