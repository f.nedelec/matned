function opt = spin_options

% Provide the options to analyse
%
% They are loaded by spin_load_option.m,
% which also check for a local 'spin_options.m'
%
%

opt.radius           = 110;
opt.visual           = 3;
opt.local_background = 1;
opt.flatten_image    = 0;
opt.auto_center      = 0;
opt.use_mask         = 0;
opt.mask             = [];
opt.time_shift       = 0;
opt.catch_exceptions = 1;
opt.seg_number       = 3;
opt.max_polarity     = 4;

return