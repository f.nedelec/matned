function res = analyzeSIMposition_dyn(varargin)
%
%
% Modified 13 Nov 2014

filename = 'nucleusPosition.cms';

if ~ exist(filename, 'file')
    system(['~/bin/reportR ashbya:position prefix=time output=', filename]);
end

%% Find flow

%usual flow rate
flow = 0.009;

%% Find number of Dynein
try
    %Find flow value from config file:
    [val, str] = system('grep -o -E "new [0-9]+ single dynein" config.cym');
    if val == 0
        num = regexp(str,'[0-9]+','match');
        res.numofdyn = str2double(num);
    end
catch
    fprintf(['could not extract flow `', str, ' from config file\n']);
end

%% Load nuclei data + flow data
data = load(filename);

% Get number of nuclei
nrNuclei = max(data(:,3));

% Reoder the data
nucleus = cell(nrNuclei, 1);
for i = 1:nrNuclei
    tmp = data(logical(data(:,3)==i),[1,3:end]);
    % Get dt
    dt_pre = tmp(2,1)-tmp(1,1);
    % Resample the data to dt 30s
    nucleus{i} = tmp(1:30/dt_pre:end,:);
end

clear data
%%
% Get new dt
dt = nucleus{1}(2,1)-nucleus{1}(1,1);

% Get number of time frame
nrTime = size(nucleus{i},1);

% Get total time in minutes
totTime_min = ((nrTime-1)*dt)/60;

forward_events = [];
backward_events = [];
tumbling_events = [];

% Compute forward, backward and tumbling events
thr = flow; %flow set in simulation to 0.009 0 0

for i = 1:nrNuclei
    
    speed{i} = sqrt(sum(diff(nucleus{i}(:,3:5)).^2 ,2))/dt;
    speed_mod{i} = sqrt(sum(diff(nucleus{i}(:,12:14)).^2,2))/dt;
    directionX{i} = sign(diff(nucleus{i}(:,3)));
        
    % categorize motion:
    forwards{i}  = directionX{i}.*speed{i}  >= thr;
    [ swi, ~, val ] = find( diff(forwards{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'forward %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_forwards(i) = length(mov);
    forward_events = [ forward_events; mov ];
   
    tumblings{i} = (directionX{i}.*speed{i} <thr) & ( directionX{i}.*speed{i}>=0 );
    [ swi, ~, val ] = find( diff(tumblings{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'tumbling %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_tumblings(i) = length(mov);
    tumbling_events = [ tumbling_events; mov ];
    
    backwards{i} = directionX{i}.*speed{i}  < 0;
    [ swi, ~, val ] = find( diff(backwards{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'backward %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_backwards(i) = length(mov);
    backward_events = [ backward_events; mov ];
    
end

%% Compute movements maximum speed
for i = 1:nrNuclei
    m_speed(i) = max(speed{i}(:,1));
    % Compute movements average speed
    a_speed(i) = mean(speed{i}(:,1));
end

%% Forward, backward and tumbling total number, average duration and maximum duration of events

maxspeed = max(m_speed);
maxspeed_min = maxspeed*60;
avgspeed_min = mean(a_speed)*60;

totNb_forwards = numel(forward_events)/(nrNuclei*totTime_min);
totAvgDur_forwards_min = mean(forward_events)/60;
maxDur_forwards_min = max(forward_events)/60;

totNb_tumblings = numel(tumbling_events)/(nrNuclei*totTime_min);
totAvgDur_tumblings_min = mean(tumbling_events)/60;
maxDur_tumblings_min = max(tumbling_events)/60;

totNb_backwards = numel(backward_events)/(nrNuclei*totTime_min);
totAvgDur_backwards_min = mean(backward_events)/60;
maxDur_backwards_min = max(backward_events)/60;

mvt_ratio = totNb_forwards/totNb_backwards;

%% Compute bypassing events

% create asymmetric matrix to count the events
mat_bypass = zeros(nrNuclei, nrNuclei);

for i = 1:nrNuclei
    for j = i+1:nrNuclei
        d_position = nucleus{j}(:,12) - nucleus{i}(:,12);
        events = diff(sign(d_position))/2 .* ( abs(d_position(2:end)) < 5 ) .* ( abs(d_position(1:end-1)) < 5 );
        mat_bypass(i,j) = mat_bypass(i,j) + sum(abs(events));
    end
end
totNb_bypass = sum(sum(mat_bypass)) / ( nrNuclei * totTime_min );
%mat_bypass

%% Cut data into sections that do not cross the periodic edges
j = 1;
for n = 1:nrNuclei
    
    jumps = [ find( abs(diff(nucleus{n}(:,12))) > 20 ); size(nucleus{n},1) ];
    s = 1;
    for ij = 1:size(jumps, 1)
        e = jumps(ij);
        if e > s
            range = s:e;
            %fprintf(1, ' cut nucleus %i: %i %i\n', n, s, e);
            nucleusP{j} = nucleus{n}(range, [1 12]);
            %fprintf(1, ' pos %f %f\n', nucleusP{j}(1,2), nucleusP{j}(end,2))
            j=j+1;
            s=e+1;
        end
    end
end

if ( 0 )
    figure
    hold on
    for j = 1:length(nucleusP)
        plot(nucleusP{j}(:,1), nucleusP{j}(:,12))
    end
end
        
%% Compute mean square displacement and convection
deltamsd = 600/dt;
for i = 1:nrNuclei
    for d = 1:deltamsd
        nuc = nucleus{i};
       
        msd2_x(i,d) = mean((-nuc(1:end-d,3)+nuc(d:end-1,3)).^2); % MSDx
        conv2_x(i,d) = mean(-nuc(1:end-d,3)+nuc(d:end-1,3)).^2; % MDx^2
        msd_conv_x(i,d) = msd2_x(i,d) - conv2_x(i,d); % MSDx - MDx^2
        conv_x(i,d) = mean(-nuc(1:end-d,3)+nuc(d:end-1,3)); % MDx
        
        msd2_y(i,d) = mean((-nuc(1:end-d,4)+nuc(d:end-1,4)).^2);
        conv2_y(i,d) = mean(-nuc(1:end-d,4)+nuc(d:end-1,4)).^2;
        msd_conv_y(i,d) = msd2_y(i,d) - conv2_y(i,d);
        conv_y(i,d) = mean(-nuc(1:end-d,4)+nuc(d:end-1,4));
        
        msd2_z(i,d) = mean((-nuc(1:end-d,5)+nuc(d:end-1,5)).^2);
        conv2_z(i,d) = mean(-nuc(1:end-d,5)+nuc(d:end-1,5)).^2;
        msd_conv_z(i,d) = msd2_z(i,d) - conv2_z(i,d);
        conv_z(i,d) = mean(-nuc(1:end-d,5)+nuc(d:end-1,5));
        
        msd2(i,d) = msd2_x(i,d) + msd2_y(i,d) + msd2_z(i,d);
        conv2(i,d) = conv2_x(i,d) + conv2_y(i,d) + conv2_z(i,d);
        msd_conv(i,d) = msd2(i,d) - conv2(i,d);
    end
end


%% Plot

%%

res.nrNuclei = nrNuclei;
res.totNb_forwards = totNb_forwards;
res.totNb_backward = totNb_backwards;
res.mvt_ratio = mvt_ratio;
res.totNb_bypass = totNb_bypass;

end
