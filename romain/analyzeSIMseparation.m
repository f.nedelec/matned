function analyzeSIMseparation(arg)
%
% Analyze separation of nuclei in simulation data
% F. Nedelec, 3 Jan 2017

filename = 'nucleusPosition.cms';

list = {};

if nargin < 1
    list{1} = '.';
elseif isdir(arg)
    list{1} = fullfile(pwd, arg);
else
    [s, out] = system(['echo ', arg]);
    if s == 0
        list = strsplit(deblank(out));
    end
end

hdir = pwd;

%% Collect data from multiple files
    
dnn = [];
pos = [];

for i = 1:length(list)

    cd(list{i});
    
    %Find period from config file:
    period = 30;
    try
        [val, str] = system('grep -o -E "cylinderP +[0123456789.]+" properties.cmo');
        if val == 0
            period = 2 * str2double(str(10:end));
        end
    catch
        fprintf(['could not extract period `', str, ' from config file\n']);
    end
    
    if ~ exist(filename, 'file')
        system(['~/bin/reportR ashbya:position prefix=time output=', filename]);
    end
    
    data = load(filename);
    
    % Resample data at 30 sec interval:
    dt = 30;
    nrNuclei = length(unique(data(:,3)));
    sel = logical( rem(data(:, 1), dt) == 0 );
    data = data(sel, :);
    
    if ( rem(size(data,1), nrNuclei)  )
        error('unexpected data format');
    end
    if ( data(1+nrNuclei,1) - data(1,1) ~= dt )
        error('incorrect time sampling');
    end
    
    timepoints = unique(data(:,1))'
    for t = timepoints
        % use column 13, X-position where periodic modulo is removed
        nux = data(logical( data(:,1) == t ), 13);
        if ( size(nux, 1) ~= nrNuclei )
            error('unexpected data format');
        end
        nux = sort(nux');
        dis = diff(nux);
        % distance between last and first around the periodic boundaries:
        dos = nux(1)+period-nux(end);
        dnn = horzcat(dnn, dis);
        pos = horzcat(pos, nux);
    end

    cd(hdir);
end

%% Process data:

if 0
    % save data to file:
    fid = fopen('separation_sim.txt', 'wt');
    fprintf(fid, '%% Nuclei separation data %s\n', date);
    fprintf(fid, '%% Simulations\n', date);
    fprintf(fid, '%10.5f\n', dnn);
    fclose(fid);
end

figure('Name', 'Separation');
hist(dnn, 0.5:17);
xlim([0, 15]);

if 1
    % calculate distribution from a independent random tossing
    rep = 2^20;
    nux = sort(period*rand(5, rep));
    dis = diff(nux);
    xbins = 0.5:17;
    his = histc(reshape(dis, numel(dis), 1), xbins);
    his = his * ( sum(dnn) / sum(sum(dis)) );
    hold on;
    plot(xbins, his, 'm-', 'LineWidth', 2);
end

xlabel('Separation between nuclei / micrometer');
ylabel('Histogram counts');
title('Nuclei Separation in Simulations');

savepng('separation_sim.png');
savepdf('separation_sim.pdf');

fprintf(1, 'separation number of records: %i\n', length(dnn));
fprintf(1, 'separation between nuclei: %f +/- %f um\n', mean(dnn), std(dnn));


%% histogram of positions:

figure('Name', 'Position');
hist(pos);
xlim([-15, 15]);
xlabel('Position of nuclei / micrometer');
ylabel('Histogram counts');
title('position of nuclei (simulation)');


end

