function [nrNuclei] = analyzeSIMposition(varargin)
%
%
% Modified 13 Nov 2014
% Modified 28.2.2015 : log-log plot have equal axes

params = struct('showplot',0,'savefig',0,'output','');
params = inputArgsToStruct(params, varargin);

filename = 'nucleusPosition.cms';

if ~ exist(filename, 'file')
    system(['~/bin/reportR ashbya:position prefix=time output=', filename]);
end

%% Find flow

%usual flow rate
flow = 0.009;

try
    %Find flow value from config file:
    [val, str] = system('grep -o -E "flow += [0123456789.]+" properties.cmo');
    if val == 0
        evalc(str);
    end
catch
    fprintf(['could not extract flow `', str, ' from config file\n']);
end

if exist('flowdata.txt','file')
    flowdata = fileread('flowdata.txt');
    flow = str2num(cell2mat(regexp(flowdata,'(0\.)+[0-9]+','match')));
end

fprintf(1, 'using flow = %6.3f\n', flow);


%% Load nuclei data + flow data
data = load(filename);

% Get number of nuclei
nrNuclei = max(data(:,3));

% Reoder the data
nucleus = cell(nrNuclei, 1);
for i = 1:nrNuclei
    tmp = data(logical(data(:,3)==i),[1,3:end]);
    % Get dt
    dt_pre = tmp(2,1)-tmp(1,1);
    % Resample the data to dt 30s
    nucleus{i} = tmp(1:30/dt_pre:end,:);
end

clear data
%%
% Get new dt
dt = nucleus{1}(2,1)-nucleus{1}(1,1);

% Get number of time frame
nrTime = size(nucleus{i},1);

% Get total time in minutes
totTime_min = ((nrTime-1)*dt)/60;

forward_events = [];
backward_events = [];
tumbling_events = [];

% Compute forward, backward and tumbling events
thr = flow; %flow set in simulation to 0.009 0 0

for i = 1:nrNuclei
    
    speed{i} = sqrt(sum(diff(nucleus{i}(:,3:5)).^2 ,2))/dt;
    speed_mod{i} = sqrt(sum(diff(nucleus{i}(:,12:14)).^2,2))/dt;
    directionX{i} = sign(diff(nucleus{i}(:,3)));
    
    % categorize motion:
    forwards{i}  = directionX{i}.*speed{i}  >= thr;
    [ swi, ~, val ] = find( diff(forwards{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'forward %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_forwards(i) = length(mov);
    forward_events = [ forward_events; mov ];
   
    tumblings{i} = (directionX{i}.*speed{i} <thr) & ( directionX{i}.*speed{i}>=0 );
    [ swi, ~, val ] = find( diff(tumblings{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'tumbling %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_tumblings(i) = length(mov);
    tumbling_events = [ tumbling_events; mov ];
    
    backwards{i} = directionX{i}.*speed{i}  < 0;
    [ swi, ~, val ] = find( diff(backwards{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'backward %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_backwards(i) = length(mov);
    backward_events = [ backward_events; mov ];
    
end

%% Compute movements maximum speed
for i = 1:nrNuclei
    m_speed(i) = max(speed{i}(:,1));
    % Compute movements average speed
    a_speed(i) = mean(speed{i}(:,1));
end

%% Forward, backward and tumbling total number, average duration and maximum duration of events

maxspeed = max(m_speed);
maxspeed_min = maxspeed*60;
avgspeed_min = mean(a_speed)*60;

totNb_forwards = numel(forward_events)/(nrNuclei*totTime_min);
totAvgDur_forwards_min = mean(forward_events)/60;
maxDur_forwards_min = max(forward_events)/60;

totNb_tumblings = numel(tumbling_events)/(nrNuclei*totTime_min);
totAvgDur_tumblings_min = mean(tumbling_events)/60;
maxDur_tumblings_min = max(tumbling_events)/60;

totNb_backwards = numel(backward_events)/(nrNuclei*totTime_min);
totAvgDur_backwards_min = mean(backward_events)/60;
maxDur_backwards_min = max(backward_events)/60;

mvt_ratio = totNb_forwards/totNb_backwards;

%% Compute bypassing events

% create asymmetric matrix to count the events
mat_bypass = zeros(nrNuclei, nrNuclei);

for i = 1:nrNuclei
    for j = i+1:nrNuclei
        d_position = nucleus{j}(:,12) - nucleus{i}(:,12);
        events = diff(sign(d_position))/2 .* ( abs(d_position(2:end)) < 5 ) .* ( abs(d_position(1:end-1)) < 5 );
        mat_bypass(i,j) = mat_bypass(i,j) + sum(abs(events));
    end
end
totNb_bypass = sum(sum(mat_bypass)) / ( nrNuclei * totTime_min );
nb_bypass = sum(mat_bypass + mat_bypass', 2);
%mat_bypass

%% Cut data into sections that do not cross the periodic edges
j = 1;
for n = 1:nrNuclei
    
    jumps = [ find( abs(diff(nucleus{n}(:,12))) > 20 ); size(nucleus{n},1) ];
    s = 1;
    for ij = 1:size(jumps, 1)
        e = jumps(ij);
        if e > s
            range = s:e;
            %fprintf(1, ' cut nucleus %i: %i %i\n', n, s, e);
            nucleusP{j} = nucleus{n}(range, [1 12]);
            %fprintf(1, ' pos %f %f\n', nucleusP{j}(1,2), nucleusP{j}(end,2))
            j=j+1;
            s=e+1;
        end
    end
end

if ( 0 )
    figure
    hold on
    for j = 1:length(nucleusP)
        plot(nucleusP{j}(:,1), nucleusP{j}(:,12))
    end
end
        
%% Compute mean square displacement and convection
deltamsd = 600/dt;
                
for i = 1:nrNuclei
    nuc = nucleus{i};
    for d = 1:deltamsd
        dif = nuc(d:end-1,3) - nuc(1:end-d,3);
        conv_x(i,d) = mean(dif);
        msd2_x(i,d) = mean(dif.^2);
        msd_conv_x(i,d) = msd2_x(i,d) - conv_x(i,d).^2;
        
        dif = nuc(d:end-1,4) - nuc(1:end-d,4);
        conv_y(i,d) = mean(dif);
        msd2_y(i,d) = mean(dif.^2);
        msd_conv_y(i,d) = msd2_y(i,d) - conv_y(i,d).^2;
           
        dif = nuc(d:end-1,5) - nuc(1:end-d,5);
        conv_z(i,d) = mean(dif);
        msd2_z(i,d) = mean(dif.^2);
        msd_conv_z(i,d) = msd2_z(i,d) - conv_z(i,d).^2;
      
        msd2(i,d) = msd2_x(i,d) + msd2_y(i,d) + msd2_z(i,d);
        msd_conv(i,d) = msd_conv_x(i,d) + msd_conv_y(i,d) + msd_conv_z(i,d);
    end
end



%% Plot

if(params.showplot)
    
    toplot = 1:nrNuclei;
    for i = 1:length(toplot)
        legendstring{i} = num2str(toplot(i));
    end
    
    toplot2 = 1:length(nucleusP);
    for i = 1:length(nucleusP)
        legendstringP{i} = num2str(toplot2(i));
    end
    
    figsize1 = [0 0 400 400];
    figsize2 = [0 0 400 80*nrNuclei];
    figsize3 = [0 0 160*nrNuclei 80*nrNuclei];
    figsize4 = [0 0 800 400];
    figsize5 = [0 0 900 400];

    % Nuclei overall movements
    h1 = setupFigure(1,figsize1);
    cmap = colormap;
    step = round(size(cmap,1)/length(nucleusP));
    color(1:length(nucleusP), 1:3) = cmap(1:step:step*length(nucleusP),:);

    for i = 1:length(nucleusP)
        plot(nucleusP{i}(:,2),((nucleusP{i}(:,1))/60)-10,'color',color(i,:));
        set(gca,'YDir','reverse');
    end
    title ('Nuclei overall movements', 'FontWeight', 'bold');
    xlabel('Nucleus position (\mu m)');
    ylabel('Time (min)');
    %legend(legendstring);
    %legend boxoff;
    
    % Patterns of forward, backward and tumbling events
    h2 = setupFigure(2,figsize2);
    for i = toplot
        subplot(nrNuclei,1,i);
        plot(((nucleus{i}(1:end-1,1))/60)-10,forwards{i}-backwards{i},'color',color(i,:));
        set(gca, 'YLim', [-1.5 1.5], 'YTick', [-1, 0, 1], 'YTickLabel', ['-', '0', '+']);
        title(['Nucleus ' num2str(i)], 'FontWeight', 'bold');
    end
    xlabel('Time (min)', 'FontWeight', 'bold');
    
    % Speed of movements
    h3 = setupFigure(3,figsize3);
    for i = toplot
        subplot(nrNuclei,1,i);
        plot(((nucleus{i}(1:end-1,1))/60)-10,speed{i}(1:end),'color',color(i,:));
        title(['Nucleus ' num2str(i)], 'FontWeight', 'bold');
    end
    xlabel('Time (min)', 'FontWeight', 'bold');
   
    % Number of forward, backward, tumbling and by passing events
    h4 = setupFigure(4,figsize4);
    for i = toplot
        subplot (1,4,1)
        plot(nb_forwards(i),'s','color',color(i,:));
        ylabel('Number of events (#)');
        xlabel('Forward events');
        hold on
        subplot (1,4,2)
        plot(nb_backwards(i),'s','color',color(i,:));
        xlabel('Backward events');
        hold on
        subplot (1,4,3)
        plot(nb_tumblings(i),'s','color',color(i,:));
        xlabel('Tumbling events');
        hold on
        subplot (1,4,4)
        plot(nb_bypass(i),'s','color',color(i,:));
        xlabel('Bypassing events');
        hold on
    end
    
    % Mean square displacement
    h5=setupFigure(5,figsize5);
    dtime = (1:deltamsd)*dt;
    subplot(1,2,1)
    for i = toplot
        %plot(log(dtime),log(msd2(i,:)),'color',color(i,:));
        loglog(dtime, msd2(i,:), 'color',  color(i,:));
        hold on
    end
    axis square
    xlabel('time'); xlim([100 1000]);
    ylabel('MSD'); ylim([1 10]);
    subplot(1,2,2)
    for i = toplot
        loglog(dtime, msd_conv(i,:), 'color',  color(i,:));
        hold on
    end
    axis square
    xlabel('time'); xlim([100 1000]);
    ylabel('MSD - MEAN'); ylim([1 10]);
    %legend(legendstring);
    %legend boxoff;
    
    %Convection x/y/z
    h6=setupFigure(6,figsize5);
    subplot(1,2,1)
    for i = toplot
        plot(dtime, conv_x(i,:),'color',color(i,:));
        hold on
    end
    axis square
    xlabel('time');
    ylabel('MEAN X');
    subplot(1,2,2)
    for i = toplot
        plot(dtime, conv_y(i,:),'color',color(i,:));
        hold on
    end
    xlabel('time');
    ylabel('MEAN Y');
    axis square
    %legend(legendstring);
    %legend boxoff;
end

%% Save plots
if(params.savefig)
    saveas(h1,'Figure_1', 'eps');
    saveas(h2,'Figure_2', 'eps');
    saveas(h3,'Figure_3', 'eps');
    saveas(h4,'Figure_4', 'eps');
    saveas(h5,'Figure_5', 'eps');
    saveas(h6,'Figure_6', 'eps');
end

%% Export results

if  params.output
    if ~exist(params.output, 'file')
        fid = fopen(params.output,'w');
        fprintf(fid, '%%%s\n','nrNuclei totTime_min flow_speed F_forwards_min avgDur_forwards_min F_backwards avgDur_backwards_min mvt_ratio avgspeed_min F_pauses avgDur_pauses_min F_bypass_min');
    else
        fid = fopen(params.output,'a');
    end
    fprintf(fid, '%.2f %.2f %.4f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f \n',nrNuclei,totTime_min,thr,totNb_forwards,totAvgDur_forwards_min,totNb_backwards,totAvgDur_backwards_min,mvt_ratio,avgspeed_min,totNb_tumblings,totAvgDur_tumblings_min,totNb_bypass);
    fclose(fid);
end


end
