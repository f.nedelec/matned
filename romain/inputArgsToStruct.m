function[valStruct] = inputArgsToStruct(valStruct, valCell)
% function to input variable number of property-value pairs into structure
% variable
%
% arguments:
% valStruct = structure variable with pre-defined fields
% varargin  = variable number of property-value pairs
%
% outputs:
% valStruct = updated structure

if ~isstruct(valStruct)
    error('first input to inputArgsToStuct function must be a structure');
end

if ~iscell(valCell)
    error('second input to inputArgsToStuct function must be a cell-array');
end

if isempty( valCell )
    return;
end


names = fieldnames(valStruct);

for i=1:2:numel( valCell )-1
    
    if ischar(valCell{i}) && ~any( strcmp(valCell{i}, names) )
        warning('variable %s is not present in structure', valCell{i})
    end
    
    if ~strcmp( class(valStruct.(valCell{i})), class(valCell{i+1}) )
        error(['type of variable %s should be: %s, but variable' ,...
               'of type %s has been passed to function'], ...
               valCell{i}, class(valStruct.(valCell{i})),...
               class(valCell{i+1}) ); 
    end
    
    valStruct.(valCell{i}) = valCell{i+1};
    

end




