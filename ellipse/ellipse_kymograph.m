function kym = ellipse_kymograph(mov, quad, width, nb_bins)

% function kym = ellipse_kymograph(mov, quad, width, nb_bins)
% function kym = ellipse_kymograph(mov, [], width, nb_bins)
%
% Build a kymograph according to the ellipse defined by `quad`,
% or by using fit_ellipse if quad is given empty
%
%
% F. Nedelec, June 2014

nbi = length(mov);
im = mov(1).data;

%%

if isempty(quad)
    s = sum_movie(mov);
    [level, sig] = image_background(s);
    level = level + 2 * sig;
    level = manual_threshold(s, level);
    fprintf('Selected threshold = %f\n', level);
    quad = fit_ellipse(s, level); 
end

%%

[ dis, ang ] = ellipse_indices(size(im), quad);

inx = floor( ang * nb_bins / pi ) + 1;
inx = inx .* ( abs(dis) < width );

show_image(inx)
mxi = max(max(inx));

%%
kym = zeros(nbi, mxi);

for i = 1:nbi
    
    im = mov(i).data;
    if any( size(inx) ~= size(im) )
        error('size missmatch');
    end

    for v = 1:mxi
        ic = logical(inx==v);
        kym(i, v) = mean(im(ic));
    end
        
end

show_image(kym);

end