function q = quadratic_iso_fit( im, w )

% c = quadratic_iso_fit( im, mode )
%
% calculate the coefficients of the best fit for im,
% by  fit(x,y) = a*( X^2 + Y^2 ) + d*X + e*Y + f
% returns c = [ a, a, 0, d, e, f]   ( format compatible with ellipse )
% mode defines rejection:
%    0: if only consider strictly positive valued pixels(default)
%    1: consider all pixels
%
% nedelec@embl-heidelberg.de, Sept 7, 2002

if ( nargin < 2 ) mode = 0; end
if ( isfield(im, 'data') ) 
    im = double( im.data );         
end

%pixels to consider:
if  nargin < 2
    % by default, use all pixels
    w = ones(size(im));
end


%horizontal/vertical lines:
hx    = [1:size(im,1)];
hxx   = hx .^ 2;
hxxx  = hxx .* hx ;
hxxxx = hxx .* hxx;

vy    = [1:size(im,2)]';
vyy   = vy .^ 2;
vyyy  = vyy .* vy;
vyyyy = vyy .* vyy;

%sums of pixels coordinates:
s1     = sum( sum( w ));

sx     = sum( hx * w );
sy     = sum( w * vy );

sxx    = sum( hxx * w );
sxy    = hx * w * vy;
syy    = sum( w * vyy );

sxxx   = sum( hxxx * w );
sxxy   = hxx * w * vy;
sxyy   = hx * w * vyy;
syyy   = sum( w * vyyy );

sxxxx  = sum( hxxxx * w );
sxxxy  = hxxx * w * vy;
sxxyy  = hxx * w * vyy;
sxyyy  = hx * w * vyyy;
syyyy  = sum( w * vyyyy );


% sums of pixels values:
imw    = im .* w;

szxx   = sum( hxx * imw );
szyy   = sum( imw * vyy );
szxy   = hx * imw * vy;
szx    = sum( hx * imw );
szy    = sum( imw * vy );
sz     = sum(sum( imw ));



S = [ sxxxx + syyyy + 2 * sxxyy, sxxx + sxyy, sxxy + syyy, sxx + syy;...
      sxxx + sxyy, sxx, sxy, sx;...
      sxxy + syyy, sxy, syy, sy;...
      sxx  + syy,   sx,  sy, s1 ];


c = S \ [ szxx + szyy; szx; szy; sz ];

q = [ c(1), 0, c(1), c(2), c(3), c(4) ];

fprintf(' Quadratic fit: %.3f X^2 %+.3f XY %+.3f Y^2 %+.3f X %+.3f Y %+.3f\n', q);


end