function S = ellipsoid_surface(a, b, c)

% Return approximate surface of 3D ellipsoid of axes length a, b, c
%
%
% F. Nedelec, 28 Sept 2017

if nargin == 1 && numel(a) == 3
    c = a(3);
    b = a(2);
    a = a(1);
end

%% approximate formula:
% https://en.wikipedia.org/wiki/Ellipsoid#Surface_area

p = 1.6075;

x = power(a*b, p) + power(a*c, p) + power(b*c, p);

S = 4 * pi * power(x/3, 1/p);

end