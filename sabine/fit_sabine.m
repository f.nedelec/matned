function fit_sabine()

% Fit of total microtubule fluorescence,
% for both HICE-depleted extracts,
% and control extracts
%
% using the model described in Dinarina et al. Cell 2009:
% Figure 4 A, Enzymatic destruction
% F. Nedelec and Sabine Petry, this version: 8 Feb 2011


%% select model with 3 or 4 parameters:

nb_pam = 4;

if nb_pam == 3
    % 3 parameters : Enzymatic destruction
    initial_pam = [0.05, 0.1, 0.5];
elseif nb_pam == 4
    % 4 parameters : Autocatalitic
    initial_pam = [0.05, 0.1, 0.5, 0.4];
end


%% get data

data = get_data;

figure, hold on;
plot_data(data);


%% Fit model to control data:

options = optimset('Display', 'off');  %use 'iter' for monitoring
data3 = data;

fprintf('\n\n');
[ pam3c, err ] = minsearch1(@error3c, initial_pam, 32);
[ pam3c, err ] = fminsearch(@error3c, pam3c, options);
[data.con_fit_time, data.con_fit] = model(pam3c);

fprintf('Control: Fit error %f\n',  err);
fprintf('Control: Fit parameters: ');
fprintf('  %+f', pam3c);
fprintf('\n');

%% Fit model to depleted data
fprintf('\n');
[ pam3e, err ] = minsearch1(@error3e, initial_pam, 32);
[ pam3e, err ] = fminsearch(@error3e, pam3e, options);
[data.exp_fit_time, data.exp_fit] = model(pam3e);

fprintf('Dgt4 ID: Fit error %f\n',  err);
fprintf('Dgt4 ID: Fit parameters: ');
fprintf('  %+f', pam3e);
fprintf('\n');

figure, hold on;
make_figure(data);

return

%% load data from file

    function d = get_data()
        d.con1 = load('data1.txt');
        d.con2 = load('data2.txt');
        d.con3 = load('data3.txt');
        d.exp1 = load('data4.txt');
        d.exp2 = load('data5.txt');
        d.exp3 = load('data6.txt');
    end

%% plot function

    function plot_data(d)
        % blue is control:
        plot(d.con1(:,1), d.con1(:,2), 'bo', 'LineWidth', 2);
        plot(d.con2(:,1), d.con2(:,2), 'bo', 'LineWidth', 2);
        hc = plot(d.con3(:,1), d.con3(:,2), 'bo', 'LineWidth', 2);
        % red is experiment:
        plot(d.exp1(:,1), d.exp1(:,2), 'rx', 'LineWidth', 2);
        plot(d.exp2(:,1), d.exp2(:,2), 'rx', 'LineWidth', 2);
        he = plot(d.exp3(:,1), d.exp3(:,2), 'rx', 'LineWidth', 2);
        xlim([0 60]);
        legend([hc; he], 'control', 'experiment');
    end


%% nicer plot to generate the figure

    function make_figure(d)
        
        % blue is control:
        hc = plot(d.con1(:,1), d.con1(:,2), 'bo', 'LineWidth', 2);
        plot(d.con2(:,1), d.con2(:,2), 'bo', 'LineWidth', 2);
        plot(d.con3(:,1), d.con3(:,2), 'bo', 'LineWidth', 2);
        plot(d.con_fit_time, d.con_fit(:,1), 'b:', 'LineWidth', 2);

        % red is experiment:
        scale = 0.6199;
        shift = 11;
        
        he = plot(shift+d.exp1(:,1), scale*d.exp1(:,2), 'rx', 'LineWidth', 2);
        plot(shift+d.exp2(:,1), scale*d.exp2(:,2), 'rx', 'LineWidth', 2);
        plot(shift+d.exp3(:,1), scale*d.exp3(:,2), 'rx', 'LineWidth', 2);
        plot(shift+d.exp_fit_time, scale*d.exp_fit(:,1), 'r:', 'LineWidth', 2);

        legend([hc; he], 'Control ID', 'Dgt4 ID', 'Location', 'NorthEast');
        xlim([0 70]);
        ylim([0 1.25]);
        box on;

    end

%% Equations of the model: the derivative as a function of time & state vector

% These models are the ones in Dinarina et al. Cell 2009
% The variables are:
% t = time
% m = amount of stuff
%
% pam contains the parameters of the ODE:


    function dm = model_diff(t, m)
        global pam;
        if numel(pam) == 2
            % pam(1) = g = constant amount of polymer generated per second
            % pam(2) = 1/sigma = time-constant for microtubule self-destruction
            dm(1,1) = pam(1) - pam(2) * m(1);
            dm(2,1) = 0;
        elseif numel(pam) == 3
            % pam(1) = g = constant amount of polymer generated per second
            % pam(2) = 1/sigma = time-constant for binding of destruction enzyme
            % pam(3) = 1/tau = time-constant for unbinding of the destruction enzyme
            dm(1,1) = pam(1) - m(2);
            dm(2,1) = pam(2)*m(1) - pam(3)*m(2);
        elseif numel(pam) == 4
            % pam(1) = g = constant amount of polymer generated per second
            % pam(2) = 1/sigma = time-constant for binding of destruction enzyme
            % pam(3) = 1/tau = time-constant for unbinding of the destruction enzyme
            % pam(4) = a = auto-catalitic assembly of microtubules
            dm(1,1) = pam(1) + pam(4)*m(1) - m(2);
            dm(2,1) = pam(2)*m(1) - pam(3)*m(2);
        end
    end

%% Returns the curve (time, value) predicted by the equations (theory) for parameters p

    function [t,m] = model(p)
        global pam
        pam = p;
        [t,m] = ode45(@model_diff, [0 100], [0; 0]);
    end


%% Error function used to obtain the best fit

    function e = error0(t,m,d)
        fit = interp1(t,m,d(:,1));
        % sum of the squared-difference:
        %e = sum( ( fit - d(:,2) ).^2 );
        % sum of the absolute-difference (eg. robust fitting):
        e = sum( abs( fit - d(:,2) ) );
    end

%% The total error for one curve (given in data1)

    function e = error1(p)
        [t,sol] = model(p);
        m = sol(:,1);
        e = error0(t,m,data1);
    end

%% The total error for 3 'control' curves (given in data3.con)

    function e = error3c(p)
        [t,sol] = model(p);
        m = sol(:,1);
        e1 = error0(t,m,data3.con1);
        e2 = error0(t,m,data3.con2);
        e3 = error0(t,m,data3.con3);
        e = e1 + e2 + e3;
    end

%% The total error for 3 'experiment' curves (given in data3.exp)

    function e = error3e(p)
        [t,sol] = model(p);
        m = sol(:,1);
        e1 = error0(t,m,data3.exp1);
        e2 = error0(t,m,data3.exp2);
        e3 = error0(t,m,data3.exp3);
        e = e1 + e2 + e3;
    end

%% unbiaised (slow) search for a minimum via random sampling

    function [pt, et] = minsearch1(fhandle, R, nb_trials)
        fprintf('Global search for minimum:\n');
        et = -1;
        options = optimset('Display', 'off', 'MaxIter', 1000);
        for i = 1:nb_trials
            fprintf('.');
            % random initial start
            p = rand(size(R)).*R;
            % refine
            [ p, e ] = fminsearch(fhandle, p, options);
            if ( et < 0 || e < et )
                pt = p;
                et = e;
                fprintf('error = %f\n', e);
            end
        end
        %fprintf('best error = %f\n', et);
        fprintf('\n');
    end



    
end
