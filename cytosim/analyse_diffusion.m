function analyse_diffusion(path)
% script to verify the diffusion of fibers in Cytosim
%
% F. Nedelec, 20 Nov. 2014
%
% sim
% reportF fiber:ends


if nargin < 1
    dpath = '';
else
    dpath = [path, '/'];
end

paths = dir([dpath,'report*']);
    
res = zeros(length(paths), 3);

for ii = 1:length(paths)
    res(ii,:) = msd([dpath, paths(ii).name]);
end

%%

t_val = 1 * (1:length(res))';

for i = 1 : 3
    
    fit = polyfit(t_val, res(:,i), 1);
    % <dx^2> = 2 D dt
    coef(i) = fit(1) / 2;
    
    if ( 1 )
        fit_val = polyval(fit, t_val);
        figure;     hold on
        plot(t_val, res(:,i), 'o');
        plot(t_val, fit_val, '-');
    end
    
end

    fprintf(1, ' diffusion X is %.6f um^2/s\n', coef(1) );
    fprintf(1, ' diffusion Y is %.6f um^2/s\n', coef(2) );
    fprintf(1, ' diffusion A is %.6f rad^2/s\n', coef(3) );

    cylinder_diffusion(5);
       
end

%%
    function dev = msd(filename)
        % load the results of 'reportF fiber:ends'
        data = load(filename);
        pos1 = data(:, 5:6);
        pos2 = data(:, 10:11);
        posM = ( pos2 + pos1 ) * 0.5;
        posD = pos2 - pos1;
        
        % nrm = sqrt(posD(:,1).^2 + posD(:,2).^2);
        angle = atan2(posD(:,2), posD(:,1));
        dev = [var(posM), var(angle)];
        
    end

    function [ Dt, Dr ] = cylinder_diffusion(len)
       
        kT = 0.0042;
        viscosity = 0.02;
        diameter = 0.025;
        aspect = len/diameter;
        
        drag = 3 * pi * viscosity * len / ( log(aspect) + 0.312 );
        D1 = kT / drag;
        
        Ct =  0.312 + 0.565/aspect - 0.100/(aspect*aspect);
        Cr = -0.662 + 0.917/aspect - 0.050/(aspect*aspect);
        
        Dt = kT * ( log(aspect) + Ct ) / ( 3 * pi * viscosity * len );
        Dr = 3 * kT * ( log(aspect) + Cr ) / ( pi * viscosity * len^3 );
        
        fprintf(1, ' predicted translation %.6f\n', Dt);
        fprintf(1, ' predicted rotation    %.6f\n', Dr);
    end
