function save_config(filename, data, position)

% This is useful to generate a cytosim config file,
% where the position of the fibers is taken from a text file
% containing coordinates measured from EM tomograms
%
% The input is a structure returned by 'load_fibers',
% containing records with a '.pts' field
%
% Example
% save_config('config.cym', mt_data{11})
%
% F. Nedelec, 10.2014, 30.03.2018

data = convert_fibers(data);

%% calculate center of gravity:

if nargin < 3
    position = '0 0 0';
end

if nargin < 2 || 0 == strcmp(position, 'center')
    cnt = 0;
    pos = [ 0 0 0 ];
    for m = 1 : length(data)
        pts = 1e-3 * data(m).pts;
        pos = pos + pts(1, :);
        cnt = cnt + 1;
    end
    position = num2str( - pos / cnt );
end


%% output to file

fid = fopen(filename, 'w');
pre = preamble;
for i = 1:length(pre)
    fprintf(fid, '%s\n', pre{i});
end

for m = 1 : length(data)

    % convert to micro-meters
    pts = data(m).pts * 1e-3;
     
    if size(pts, 1) < 1
        fprintf(2, 'warning: record %i has no coordinates\n', m);
        continue;
    end
    
    fprintf(fid, '\n%% record %i with %i points:', m, size(pts, 1));
    fprintf(fid, '\nnew fiber filament');
    if isfield(data(m), 'type')
        t = data(m).type;
        if t == 1 || t == 2
            fprintf(fid, '%i', data(m).type);
        end
    end
    fprintf(fid, '\n{');
    fprintf(fid, '\n position = %s', position);
    fprintf(fid, '\n placement = anywhere');
    fprintf(fid, '\n orientation = none');

    if size(pts, 1) > 1
        fprintf(fid, '\n shape =%.6f %.6f %.6f,\\\n', pts(1, 1), pts(1, 2), pts(1, 3));
        for p = 2 : size(pts, 1)-1
            fprintf(fid, '   %.6f %.6f %.6f,\\\n', pts(p, 1), pts(p, 2), pts(p, 3));
        end
    fprintf(fid, '   %.6f %.6f %.6f;', pts(end, 1), pts(end, 2), pts(end, 3));
    end
    fprintf(fid, '\n}\n');
    
end

fclose(fid);

end



function res = preamble

res = {
    '% This file was generated by save_config.m';
    '';
    'set simul data';
    '{';
    '    time_step = 0.001';
    '    viscosity = 1';
    '    display = ( style=3; point_value=0.01; )'
    '}';
    '';
    'set fiber filament';
    '{';
    '    rigidity = 20';
    '    segmentation = 0.1';
    '    display = ( color=white; line=2.5; )';
    '}';

    'set fiber filament1';
    '{';
    '    rigidity = 20';
    '    segmentation = 0.1';
    '    display = ( color=light_blue; line=2.5; )';
    '}';
    '';
    'set fiber filament2';
    '{';
    '    rigidity = 20';
    '    segmentation = 0.1';
    '    display = ( color=green; line=2.5; )';
    '}';
    '';
    'set space cell';
    '{';
    '    geometry = ( sphere 5 )';
    '    display = ( visible=0; )';
    '}';
    '';
    'new space cell'
    };

end

