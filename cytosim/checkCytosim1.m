

%a script to read the dump files produced by the boxsolve.cc of the simulation


%-----------------------reads the names of the microtubules:

load dumpN;

%=======================size of the system:

%-----------------------microtubule coordinates:
load dumpM;
sz = size(dumpM,1);

DIM = size( dumpM, 1 ) / size( dumpN, 1 )

%-----------------------right hand side:
load dumpRHS;
%disp(['norm(rhs) = ', num2str(norm(dumpRHS))]);

%-----------------------solution found by the simulation:
load dumpS;

%-----------------------matrix B:
load dumpB;
B = sparse( 1 + dumpB(:, 1), 1 + dumpB(:, 2), dumpB(:, 3) );

%symmetrize it:
B = B + triu(B,1)';

%-----------------------matrix P (projection):
load dumpP;
P = sparse( 1 + dumpP(:, 1), 1 + dumpP(:, 2), dumpP(:, 3) );

%-----------------------matrix D ( preconditionner ):
load dumpD;
D = sparse( 1 + dumpD(:, 1), 1 + dumpD(:, 2), dumpD(:, 3) );

%-----------------------matrix R ( confinement ):
load dumpR;
if ( size( dumpR , 1 ) > 1 )
    R = sparse( 1 + dumpR(:, 1), 1 + dumpR(:, 2), dumpR(:, 3) );
else
    R = 0;
end;

%-----------------------check things:

%-----------------------matrix B2, twice as big:
B2 = sparse( 1 + DIM*dumpB(:,1), 1 + DIM*dumpB(:,2), dumpB(:,3), sz, sz );
for d = 2:DIM
    B2 = B2 + sparse( d + DIM*dumpB(:,1), d + DIM*dumpB(:,2), dumpB(:,3), sz, sz );
end
B2 = B2 + R;
B2 = B2 + triu(B2,1)';

%-----------------------full matrix of the dynamic:
M = spdiags( ones(sz,1), 0, sz, sz ) + P * B2;


T = spones(P) .* M;
dif = D * T;
disp(['error on the diagonal : ', num2str( norm( diag( dif ) -  1 ))] );

tol = 2e-5;


disp('no preconditionning :');
X = bicgstab(M, dumpRHS, tol, 500);

dV = norm( X - dumpS );
disp(['error on solution : ', num2str(dV)]);
figure(1);
clf;
plot(X, 'b.');
hold on;
plot(dumpS, 'k.');


disp('diagonal preconditionning :');
bicgstab(M, dumpRHS, tol, 100, T);
dV = norm( X - dumpS );
disp(['error on solution : ', num2str(dV)]);

return;

