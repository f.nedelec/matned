
%a script to read the dumping files produced by the boxsolve.cc of the simulation
%last modified by F. Nedelec on August 2003, 31 st

clear;
close all;
%=======================size of the system:

load dump_DIM;

DIM = dump_DIM(1,1);
nbp = dump_DIM(2,1);
dt  = dump_DIM(3,1);
tol = dump_DIM(4,1);
fprintf('nbpts = %i, DIM = %i, dt = %f, tolerance = %f', nbp, DIM, dt, tol);

%-----------------------reads the microtubule's info:

load dump_mobility;

names = unique( dump_mobility(:,1) );

for n=1:size(names,1)
    names(n, 2) = length( find( dump_mobility == names(n,1) ) );
end

%---------build the matrix with ones on the blocks:

blocks_ones = sparse( DIM * nbp, DIM * nbp );
ii=1;
for n=1:size(names, 1)
    bs = names(n, 2);
    blocks_ones( ii:ii+bs-1, ii:ii+bs-1 ) = 1;
    ii = ii + bs;
end

%-----------------------diagonal of the mobilities:

mobilities = dump_mobility( :, 3 );

%-----------------------initial positions:

pts_old = dump_mobility( :, 2 );

%-----------------------right hand side:

rhs = load('dump_rhs');
disp(['norm(rhs) = ', num2str(norm(rhs))]);

%-----------------------solution found by cytosim:

sol_sim = load('dump_sol');
disp(['norm(sol) = ', num2str(norm(sol_sim))]);

%-----------------------matrix B:
matB = load('dump_matB');
if ~isempty(matB)

    B = sparse( 1 + matB(:, 1), 1 + matB(:, 2), matB(:, 3), nbp, nbp );

    %symmetrize it:
    B = B + triu(B,1)';

    %-----------------------matrix BB, = B repeated DIM times:

    BB = sparse( 1 + DIM*matB(:,1), 1 + DIM*matB(:,2), matB(:,3), DIM*nbp, DIM*nbp );
    for d = 2:DIM
        BB = BB + sparse( d + DIM*matB(:,1), d + DIM*matB(:,2), matB(:,3), DIM*nbp, DIM*nbp );
    end
    BB = BB + triu(BB,1)';

else
    BB = 0;
end

%-----------------------matrix C ( non-isotropic contribution ):

matC = load('dump_matC');
if ~isempty(matC)
    C = sparse( 1 + matC(:, 1), 1 + matC(:, 2), matC(:, 3), DIM*nbp, DIM*nbp );
    C = C + triu(C,1)';
else
    C = 0;
end

%-----------------------matrix P (projection):

dumpP = load('dump_project');
P = sparse( 1 + dumpP(:, 1), 1 + dumpP(:, 2), dumpP(:, 3), DIM*nbp, DIM*nbp );

%-----------------------matrix P' ( projection derivative ):

matPD = load('dump_projectdiff');
if ~isempty(matPD)
    PD = sparse( 1 + matPD(:, 1), 1 + matPD(:, 2), matPD(:, 3), DIM*nbp, DIM*nbp );
else
    PD = 0;
end


%-----------------------matrix D ( preconditionner ):

dumpD = load('dump_precond');
precond = sparse( 1 + dumpD(:, 1), 1 + dumpD(:, 2), dumpD(:, 3), DIM*nbp, DIM*nbp );


%-----------------------the diagonal block of the total matrix found by cytosim:

dump = load('dump_diagblock');
blocks_sim = sparse( 1 + dump(:,1), 1 + dump(:,2), dump(:,3), DIM*nbp, DIM*nbp );

%-----------------------we also build the total matrix from the others:

id = speye( DIM*nbp );
ah = dt * spdiags( mobilities, 0, DIM*nbp, DIM*nbp );

full_matlab = id - ah * P * ( BB + C + PD );
blocks_matlab = blocks_ones .* full_matlab;



%----------------------- sparsity plot:

figure('Name', 'sparsity', 'Position',[ 30 30 nbp nbp], 'MenuBar', 'none');
spy(full_matlab, 1, 'y.');
hold on;
spy(B, 1, 'b.');
set(gca, 'Position', [0, 0, 1, 1]);
drawnow;
fprintf('nnz(B)    = %i', nnz(B) );
fprintf('nnz(C)    = %i', nnz(C) );
fprintf('nnz(P)    = %i', nnz(P) );
fprintf('nnz(full) = %i', nnz(full_matlab) );

%----------------------check the error made on the diagonal of total matrix:

difM = blocks_sim - blocks_matlab .* blocks_ones;
fprintf('error on the dynamic blocks : %e', full(max(max(abs(difM)))) );

%----------------------check the preconditioner calculated in cytosim

difB = precond * blocks_matlab - id;
fprintf('error on the preconditionner : %e', full(max(max(abs(difB)))) );

%----------------------check the solution-----------------------------

disp('BCGstab no preconditionning :');
sol = bicgstab(full_matlab, rhs, tol, DIM*nbp);

dVn = norm( sol - sol_sim );
dVm = max( abs( sol - sol_sim ));
disp(['error on solution : norm ', num2str(dVn)]);
disp(['error on solution : max  ', num2str(dVm)]);
clear sol;

%----------------------biCGstab diagonal preconditionning

dd = full_matlab .* id;
disp('BCGstab diagonal preconditionning :');
sol = bicgstab(full_matlab, rhs, tol, DIM*nbp, dd);

dVn = norm( sol - sol_sim );
dVm = max( abs( sol - sol_sim ));
disp(['error on solution : norm ', num2str(dVn)]);
disp(['error on solution : max  ', num2str(dVm)]);
clear sol;

%----------------------biCGstab block preconditionning

disp('BCGstab block preconditionning :');
sol = bicgstab(full_matlab, rhs, tol, DIM*nbp, blocks_matlab);

dVn = norm( sol - sol_sim );
dVm = max( abs( sol - sol_sim ));
disp(['error on solution : norm ', num2str(dVn)]);
disp(['error on solution : max  ', num2str(dVm)]);

%---------------------------------------------------------------------

figure(3);
clf;      plot(sol, 'b.', 'MarkerSize', 5);
hold on;  plot(sol_sim, 'y.', 'MarkerSize', 5);

figure(4);
clf;
plot( pts_old(1:DIM:DIM*nbp), pts_old(2:DIM:DIM*nbp), 'k.', 'MarkerSize', 1);
hold on;
for ii=1:nbp
    jj = DIM*ii;
    px = pts_old(jj-1);
    py = pts_old(jj);
    qx = px + sol_sim(jj-1);
    qy = py + sol_sim(jj);
    plot( [px, qx], [py, qy], 'b-');
    qx = px + sol(jj-1);
    qy = py + sol(jj);
    plot( [px, qx], [py, qy], 'k-');
end


%--------------------------------GMRES--------------------------------

disp('GMRES:');
sol = gmres(full_matlab, rhs, 100, tol);

dVn = norm( sol - sol_sim );
dVm = max( abs( sol - sol_sim ));
disp(['error on solution : norm ', num2str(dVn)]);
disp(['error on solution : max  ', num2str(dVm)]);
clear sol;

disp('GMRES block precond:');
sol = gmres(full_matlab, rhs, 100, tol, 500, blocks_matlab);

dVn = norm( sol - sol_sim );
dVm = max( abs( sol - sol_sim ));
disp(['error on solution : norm ', num2str(dVn)]);
disp(['error on solution : max  ', num2str(dVm)]);
clear sol;

return;

