function output = cell_to_struct(data)

% function output = cell_to_struct(data)
%
% Convert array of cell with fields, to struct array with fields
% Francois Nedelec, 18 Jan 2016

if iscell(data)
    
    fs = fieldnames(data{1});
    for k = 1:length(fs)
        f = fs{k};
        for i = 1:size(data, 1)
        for j = 1:size(data, 2)
            output(i,j).(f) = data{i,j}.(f);
        end
        end
    end
    
elseif istruct(data)
    
    output = data;

end

end

