function hfig = draw_fibers(data)

% Plot data in structure returned by load_fibers()
%
% F. Nedelec, Oct 2017

hfig = figure;

for i = 1 : length(data)
   
    P = data(i).pts;
    
    plot3(P(:,1),P(:,2),P(:,3));
    hold on;
    
end

axis equal;

end