function  list = diffParameters( P1, P2, silent, ignore )

% function  list = diffParameters( P1, P2 )
% function  list = diffParameters( P1, P2, silent )
% function  list = diffParameters( P1, P2, silent, ignore )
%
% find the differences of two parameter structures created by getParameters
% list contains the names of the parameters for which values differed,
%
% by default the differences are also printed on the command window
% specifying 'silent=true' turns off this output off
%
% if given, 'ignore' specifies a list or parameters to ignore in the comparison
%
% F. Nedelec, Aug. 2005
% CVS: $Id:$


if ( nargin < 3 )
    silent = 0;
end

names = unique( cat( 1, fieldnames(P1), fieldnames(P2) ));

cnt = 0;
for n = 1 : size( names, 1 )

    name = char( names( n ));

    if isfield( P1, name )
        s1 = getfield( P1, name );
    else
        s1 = [];
    end
    
    if isfield( P2, name )
        s2 = getfield( P2, name );
    else
        s2 = [];
    end

    if ( isempty(s1) || isempty(s2) )
        d = ~( isempty(s1) && isempty(s2) );
    else
        if all( size(s1) == size(s2) )
            d = sum( s1 ~= s2 );
        else
            d = 1;
        end
    end
    
    if ( d & exist('ignore', 'var') )
        d = ~ismember( names(n), ignore );
    end

    if ( d )
        
        if ( ~silent )
            v = max( size( s1, 1 ), size(s2, 1 ));
            namec = sprintf('%15s  :  ', name);

            for i = 1 : v
                if ( i <= size(s1,1) )
                    s1c = num2str( s1(i,:) );
                else
                    s1c = '';
                end
                if ( i <= size(s2,1) )
                    s2c = num2str( s2(i,:) );
                else
                    s2c = '';
                end
                h = 45 - size( s1c, 2 );
                disp( [ namec, s1c, zeros(1,h), s2c] );
            end
            
        end
        
        cnt = cnt + 1;
        list(1,cnt) = names(n);

    end
end

if ~ exist('list', 'var')
    list = [];
end
