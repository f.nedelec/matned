function d = degree_of_coiling(X, Y, Z)

% F. Nedelec, 13.03.2019

if nargin == 1 && size(X,2) == 3
    Z = X(:,3);
    Y = X(:,2);
    X = X(:,1);
end

Xm = mean(X);
Ym = mean(Y);
Zm = mean(Z);

d = sum((Z-Zm).^2) / ( sum((X-Xm).^2) + sum((Y-Ym).^2) );


end