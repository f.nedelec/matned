function PTS = bbsc(b, n)

%   Generate points according to the Baseball seam curve
%   b is the coiling parameter ( )
%   n the number of points
%
% S. Dmitrieff and F. Nedelec 24.09.2017

t = [0:n-1]' .* ( 2 * pi / n );

a = 1 - b;
c = 2 * sqrt((1-b)*b);

X = a * sin(t) + b * sin(3*t);
Y = a * cos(t) - b * cos(3*t);
Z = c * cos(2*t);

PTS = [X,Y,Z];

end

